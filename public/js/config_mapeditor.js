require.config({
    urlArgs: Math.random(),
    baseUrl: "js/halal",
    paths: {
        "loglevel": "../vendor/loglevel/dist/loglevel",
        "jquery": "../vendor/jquery/jquery",
        "jquery-ui": "../vendor/jquery-ui/ui/jquery-ui",
        "jquery-contextmenu": "../vendor/jquery.contextmenu"
        /*,"modernizr": "../vendor/modernizr/modernizr"*/
    },
    shim: {
        "jquery-ui": {
            exports: "$",
            deps: ['jquery', 'jquery-contextmenu']
        },
        "jquery-contextmenu": {
            exports: "$",
            deps: ["jquery"]
        },
        "loglevel": {
            exports: "log"
        }
    }
});

//load needed modules
//ovde osnovne stvari loadovati
require(["jquery", "loglevel"], function($, loglevel) {
    window.log = loglevel;
    log.setLevel(log.levels.DEBUG);

    $(document).ready(function() {
        require(["Halal"], function(Hal) {
            log.debug("halal loaded")
            require(["MapEditor"], function (MapEditor) {
                log.debug("mapeditor loaded");
            });
            // Hal.start();

            // require(["FullscreenButton", "AssetLoaderBar", "../scenes/Map3D"], function(FullscreenButton, AssetLoader, Map3D) {
                // Hal.scm.addScene(new Map3D({
                //     name: "Aeon7 Map3D view"
                // }));
                // $(".viewport").css("opacity", 255)
                // $(".viewport").fadeIn();
            // });
        });
    });
});

