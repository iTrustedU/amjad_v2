"use strict"
require.config
    baseUrl: "js/halal-build"
    paths:
        "loglevel": "../vendor/loglevel/dist/loglevel"
        "jquery": "../vendor/jquery/jquery"
        "jquery-ui": "../vendor/jquery-ui/ui/jquery-ui"
        "jquery-ui-soro": "../vendor/jquery-ui/ui/jquery-ui_soro"
        "hud": "../hud"
        "handlebars": "../vendor/handlebars"
    shim:
        "jquery-ui":
            exports: "$",
            deps: ['jquery']
        "loglevel":
            exports: "log"


require ["jquery", "loglevel", "jquery-ui", "jquery-ui-soro", "hud", "handlebars"], 
($, loglevel) ->
  window.log = loglevel
  log.setLevel log.levels.DEBUG
  
  $(document).ready () ->
    require ["Halal"], (Hal) ->
      require ["AssetLoaderBar"], (AssetLoaderBar) ->
        bar = new AssetLoaderBar () ->
          Hal.on "ENGINE_STARTED", () ->
            require ["../amjad/AmjadManager"], (AmjadManager) ->
              window.amjad = new AmjadManager({
                  url: "city1_30x30.json"
                  numrows: 30
                  numcols: 30
              },{
                  url: "zone1_50x50.json"
                  numrows: 50
                  numcols: 50
              })
          Hal.start()
        Hal.asm.setResourcesRelativeURL "/assets/"
        Hal.asm.loadSpritesFromFileList "assets/sprites/sprites.list"
