(function() {
  "use strict";
  require.config({
    baseUrl: "js/halal-build",
    paths: {
      "loglevel": "../vendor/loglevel/dist/loglevel",
      "jquery": "../vendor/jquery/jquery",
      "jquery-ui": "../vendor/jquery-ui/ui/jquery-ui",
      "jquery-ui-soro": "../vendor/jquery-ui/ui/jquery-ui_soro",
      "hud": "../hud",
      "handlebars": "../vendor/handlebars"
    },
    shim: {
      "jquery-ui": {
        exports: "$",
        deps: ['jquery']
      },
      "loglevel": {
        exports: "log"
      }
    }
  });

  require(["jquery", "loglevel", "jquery-ui", "jquery-ui-soro", "hud", "handlebars"], function($, loglevel) {
    window.log = loglevel;
    log.setLevel(log.levels.DEBUG);
    return $(document).ready(function() {
      return require(["Halal"], function(Hal) {
        return require(["AssetLoaderBar"], function(AssetLoaderBar) {
          var bar;
          bar = new AssetLoaderBar(function() {
            Hal.on("ENGINE_STARTED", function() {
              return require(["../amjad/AmjadManager"], function(AmjadManager) {
                return window.amjad = new AmjadManager({
                  url: "city1_30x30.json",
                  numrows: 30,
                  numcols: 30
                }, {
                  url: "zone1_50x50.json",
                  numrows: 50,
                  numcols: 50
                });
              });
            });
            return Hal.start();
          });
          Hal.asm.setResourcesRelativeURL("/assets/");
          return Hal.asm.loadSpritesFromFileList("assets/sprites/sprites.list");
        });
      });
    });
  });

}).call(this);
