(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Scene", "Renderer"], function(Scene, Renderer) {
    var Map3D;
    Map3D = (function(_super) {
      __extends(Map3D, _super);

      function Map3D(config) {
        Map3D.__super__.constructor.call(this, config.name, Hal.r.bounds, false);
      }

      Map3D.prototype.init = function() {
        var _this = this;
        this.world_dim = [0, 0, this.bounds[2], this.bounds[3]];
        this.resetQuadSpace(this.world_dim);
        this.camera.setViewFrustum([0, 0, this.bounds[2], this.bounds[3]]);
        Hal.on("ENTER_FRAME", function() {
          _this.g.ctx.setTransform(1, 0, 0, 1, 0, 0);
          return _this.g.ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
        });
        return log.debug(this.g);
      };

      Map3D.prototype.update = function(delta) {
        if (this.paused) {
          return;
        }
        this.g.ctx.fillStyle = "gray";
        this.g.ctx.fillRect(0, 0, this.bounds[2], this.bounds[3]);
        this.g.ctx.fill();
        return Map3D.__super__.update.call(this, delta);
      };

      return Map3D;

    })(Scene);
    return Map3D;
  });

}).call(this);
