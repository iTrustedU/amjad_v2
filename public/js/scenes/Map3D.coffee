"use strict"

define ["Scene", "Renderer"], 

(Scene, Renderer) ->

    class Map3D extends Scene
        constructor: (config) ->
            super(config.name, Hal.r.bounds, false)

        init: () ->
            @world_dim = [0, 0, @bounds[2], @bounds[3]]
            @resetQuadSpace(@world_dim)
            @camera.setViewFrustum([0, 0, @bounds[2], @bounds[3]])
            Hal.on "ENTER_FRAME", () =>
                @g.ctx.setTransform(1, 0, 0, 1, 0, 0)
                @g.ctx.clearRect(0, 0, @bounds[2], @bounds[3])
            log.debug @g

        update: (delta) ->
            return if @paused
            @g.ctx.fillStyle = "gray"
            @g.ctx.fillRect(0, 0, @bounds[2], @bounds[3])
            @g.ctx.fill()
            super(delta)

    return Map3D