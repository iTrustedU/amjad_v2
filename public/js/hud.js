$(document).ready(function() {
	$(function() {
		$( ".chat" ).resizable();
	});
 
	$(".tab-build").click(function() {
		$(".build-box").toggle();
		$(".tab-build").toggleClass("tab-build-click");
		$(".research-box").hide();
		$(".tab-research").removeClass("tab-research-click");
		$(".training-box").hide();
		$(".tab-training").removeClass("tab-training-click");
		$(".tab-training").removeClass("tab-training2");
	});
	$(".tab-research").click(function() {
		$(".research-box").toggle();
		$(".tab-research").toggleClass("tab-research-click");
		$(".build-box").hide();
		$(".tab-build").removeClass("tab-build-click");
		$(".training-box").hide();
		$(".tab-training").removeClass("tab-training-click");
		$(".tab-training").toggleClass("tab-training2");
	});
	$(".tab-training").click(function() {
		$(".training-box").toggle();
		$(".tab-training").toggleClass("tab-training-click");
		$(".build-box").hide();
		$(".tab-build").removeClass("tab-build-click");
		$(".research-box").hide();
		$(".tab-research").removeClass("tab-research-click");
		$(".tab-training").removeClass("tab-training2");
	});
	// $("tr").click(function() {
	// 	$('tr').not(this).removeClass('tr-click-bg');
	// 	$(this).toggleClass("tr-click-bg");
	// });
	$(".slide-down-button").click( function(event) {
	    event.preventDefault();
	    if ($(".chat").hasClass("isDown") ) {
	        $(".chat").animate({
	        	height:"27px",
	        	width:"430px"
	        }, 500);          
	        $(".chat").removeClass("isDown");
	        $(this).addClass("slide-up-button");
	        $(".chat-slider-bar").css("display","none");
	    } else {
	        $(".chat").animate({
	        	height:"230px",
	        	width:"430px"
	        }, 500);   
	        $(".chat").addClass("isDown");
	        $(this).removeClass("slide-up-button");
	        $(".chat-slider-bar").css("display","block");
	    }
	    return false;
	});

	// $(".map-box").click(function() {
	// 	if(amjad.current_view === amjad.city)
 //            amjad.enterZoneView();
 //        else 
 //            amjad.enterCityView();
	// });

	$(".close-button").click(function() {
		$(this).parent().parent().hide()
	});

	$("#build-tab").click(function() {
		Hal.trigger("LIST_AVAILABLE_CITY_BUILDINGS", {})
	})

	//var tavern_tabs = $(".tavern > .tavern-tabs").find(".chat-tab")
	// $.each(tavern_tabs, function(k, v) {
	// 	id = $(v).attr("id")
	// 	$(v).click(function() {
	// 		id = $(this).attr("id")
	// 		amjad.openTavern()
	// 	})
	// })

	                               //  <div class="tavern-tabs">
                                //     <div class="chat-tab" id="mercenary-tab">
                                //         <div class="chat-tab-l"></div>
                                //         <span class="chat-tab-middle">Commander</span>
                                //         <div class="chat-tab-r"></div>
                                //     </div>
                                //     <div class="chat-tab" id="trainer-tab">
                                //         <div class="chat-tab-l"></div>
                                //         <span class="chat-tab-middle">Agent</span>
                                //         <div class="chat-tab-r"></div>
                                //     </div>
                                //     <div class="chat-tab" id="caravan-tab">
                                //         <div class="chat-tab-l"></div>
                                //         <span class="chat-tab-middle">Caravan</span>
                                //         <div class="chat-tab-r"></div>
                                //     </div>
                                //     <div class="chat-tab" id="agent-tab">
                                //         <div class="chat-tab-l"></div>
                                //         <span class="chat-tab-middle">Trainer</span>
                                //         <div class="chat-tab-r"></div>
                                //     </div>
                                //     <div class="chat-tab" id="commander-tab">
                                //         <div class="chat-tab-l"></div>
                                //         <span class="chat-tab-middle">Mercenary</span>
                                //         <div class="chat-tab-r"></div>
                                //     </div>
                                // </div>
});

/*$( ".chat" ).click(function() {
	$(this).animate({
		height: "52px"
	}, 500, function() {
	});
});*/
