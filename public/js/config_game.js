require.config({
    baseUrl: "js/halal",
    paths: {
        "loglevel": "../vendor/loglevel/dist/loglevel",
        "jquery": "../vendor/jquery/jquery",
        "jquery-ui": "../vendor/jquery-ui/ui/jquery-ui",
        "jquery-ui-soro": "../vendor/jquery-ui/ui/jquery-ui_soro",
        "hud": "../hud",
        "handlebars": "../vendor/handlebars"
    },
    shim: {
        "jquery-ui": {
            exports: "$",
            deps: ['jquery', 'jquery-contextmenu']
        },
        "loglevel": {
            exports: "log"
        }
    }
})

//load needed modules
//ovde osnovne stvari loadovati