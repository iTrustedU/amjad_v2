(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["entities/SpriteEntity", "../amjad/TileLayer"], function(SpriteEntity, TileLayer) {
    var Tile;
    Tile = (function(_super) {
      __extends(Tile, _super);

      function Tile(config) {
        var isOdd;
        Tile.__super__.constructor.call(this, [config.x, config.y], config.sprite);
        this.row = config.row;
        this.col = config.col;
        isOdd = this.col % 2;
        this.direction = {
          north: [-1, 0],
          south: [1, 0],
          east: [0, 2],
          west: [0, -2],
          southeast: [isOdd, 1],
          northeast: [-1 + isOdd, 1],
          southwest: [isOdd, -1],
          northwest: [-1 + isOdd, -1]
        };
        this.is_partial = false;
        this.layers = [null, null, null, null];
        this.num_layers = 0;
      }

      Tile.prototype.update = function(delta) {
        var layer, _i, _len, _ref, _results;
        if (this.num_layers === 0) {
          Tile.__super__.update.call(this, delta);
        }
        _ref = this.layers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          layer = _ref[_i];
          if ((layer != null) && !layer.is_partial) {
            _results.push(layer.update());
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      Tile.prototype.isPartial = function() {
        var layer, _i, _len, _ref;
        _ref = this.layers;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          layer = _ref[_i];
          if (layer.is_partial) {
            return true;
          }
        }
      };

      Tile.prototype.isFull = function() {
        return this.num_layers > 3;
      };

      Tile.prototype.removeLayer = function(level, keeplisteners) {
        var i, span, t, _i, _ref;
        if (this.layers[level] != null) {
          if (!this.layers[level].is_partial) {
            if (!keeplisteners) {
              log.debug("removing triggers");
              this.layers[level].removeAllTriggers();
            }
            if (this.layers[level].quadspace != null) {
              this.layers[level].quadspace.remove(this.layers[level]);
            }
          } else {
            log.debug("omfg, it's partial, no way");
          }
          t = this.layers[level];
          span = this.scene.getSpanArea(this, t.meta.size);
          if (span.length > 1) {
            for (i = _i = 1, _ref = span.length - 1; 1 <= _ref ? _i <= _ref : _i >= _ref; i = 1 <= _ref ? ++_i : --_i) {
              span[i].layers[level] = null;
            }
          }
          this.layers[level] = null;
          return this.num_layers--;
        }
      };

      Tile.prototype.equals = function(other) {
        return other.row === this.row && other.col === this.col;
      };

      Tile.prototype.addLayer = function(metakey, adjpos, overwrite) {
        var i, level, lpos, meta, renderer, span, t, _i, _ref;
        meta = this.scene.tmngr.getTile(metakey);
        if (meta == null) {
          throw new Error("Couldn't load tile " + metakey);
          return null;
        }
        level = this.scene.layers[meta.level];
        renderer = this.scene.layer_renderers[level];
        if (!overwrite && (this.layers[level] != null)) {
          return;
        }
        lpos = [this.pos[0] + this.scene.camera.pos[0] / this.scene.camera.zoom, this.pos[1] + this.scene.camera.pos[1] / this.scene.camera.zoom];
        t = this.scene.tmngr.newTileFromMeta(level, meta, lpos, adjpos, renderer, this);
        if (t == null) {
          log.debug("couldn't create tile from meta " + meta.name);
          return;
        }
        span = this.scene.getSpanArea(this, meta.size);
        if (span.length > 1) {
          for (i = _i = 1, _ref = span.length - 1; 1 <= _ref ? _i <= _ref : _i >= _ref; i = 1 <= _ref ? ++_i : --_i) {
            span[i].layers[level] = {
              is_partial: true,
              is_root: false,
              parent_tile: this
            };
          }
        }
        this.layers[level] = t;
        this.num_layers++;
        return t;
      };

      Tile.prototype.save = function() {
        var layer, tile, _i, _len, _ref;
        tile = {
          r: this.row,
          c: this.col,
          l: []
        };
        if (this.num_layers === 0) {
          delete tile.l;
        } else {
          _ref = this.layers;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            layer = _ref[_i];
            if ((layer != null) && !layer.is_partial) {
              tile.l.push(layer.serialize());
            }
          }
        }
        return tile;
      };

      Tile.prototype.load = function(layers) {
        var layer, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = layers.length; _i < _len; _i++) {
          layer = layers[_i];
          if (layer == null) {
            continue;
          }
          _results.push(this.addLayer(layer.m, layer.h, true));
        }
        return _results;
      };

      return Tile;

    })(SpriteEntity);
    return Tile;
  });

}).call(this);
