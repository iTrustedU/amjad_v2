"use strict"

define ["../amjad/TileLayer", "Vec2"], 

(TileLayer, Vec2) ->

    class WalkableTile extends TileLayer
        constructor: (level, meta, basepos, adjpos, renderer, parent) ->
            super(level, meta, basepos, adjpos, renderer, parent)
            @mass = 3
            @gravity = Vec2.fromValues(0, 0)

            @velocity = Vec2.fromValues(0.0001, 0.0001)
            @max_vel = 4
            @target = @pos
            Vec2.sub(@velocity, @target, @pos)

            @steering_f = Vec2.fromValues(0,0)

            @max_steering_f = 0.3
            @max_speed = 0.4
            @moving = false

            if @meta.listeners?
                if @meta.listeners.FINISHED_WALKING?
                    @on "FINISHED_WALKING", Function(@meta.listeners.FINISHED_WALKING)

        init: () ->
            # @scene.on "LAYER_SELECTED", (layer) =>
            #     return if layer is @
            #     return if not @selected
            #     path = @scene.pathfinder.find(@parent_tile, layer.parent_tile)
            #     if not path?
            #         return
            #     out = []
            #     for p in path
            #         tile = @scene.getTile(p.row, p.col)
            #         out.unshift(tile.pos)
            #     @followPath(out)
            super()

        goTo: (destination) ->
            path = @scene.pathfinder.find(@parent_tile, destination)
            log.debug "pathfinding from"
            log.debug @parent_tile
            log.debug "pathfinding to"
            log.debug destination
            log.debug @path
            if not path?
                return
            out = []
            for p in path
                tile = @scene.getTile(p.row, p.col)
                out.unshift(tile.pos)
            @followPath(out)

        followPath: (path) ->
            if not @follow_path
                @follow_path = Hal.on "ENTER_FRAME", (delta) =>
                    @moving = true
                    if (Math.abs(@pos[0] - @target[0]) + Math.abs(@pos[1] - @target[1])) < 5
                        @target = path.shift()
                        if not @target?
                            @moving = false
                            Hal.removeTrigger "ENTER_FRAME", @follow_path
                            @follow_path = null
                            @target = @pos
                            Vec2.sub(@velocity, @target, @pos)
                            log.debug (@parent_tile)
                            @trigger "FINISHED_WALKING", @parent_tile
                            return
                        else 
                            #@parent_tile.removeLayer(@level, true)
                            span_area = @scene.getSpanArea(@parent_tile, @meta.size)
                            if span_area.length > 0
                                for i in [0..span_area.length-1]
                                    span_area[i].removeLayer(@level, true) #layers[@level] = {is_partial: true, is_root: false, parent_tile: @parent_tile}
                                    span_area[i].is_partial = false
                            @parent_tile = @scene.getTileAt([@target[0] + @scene.tilew2, @target[1] + @scene.tileh2])
                            if (not @parent_tile.layers[@level]? or not @parent_tile.layers[@level].is_partial) and @parent_tile.layers[0]?
                                @parent_tile.layers[@level] = @
                                span_area = @scene.getSpanArea(@parent_tile, @meta.size)
                                if span_area.length > 1
                                    for i in [1..span_area.length-1]
                                        span_area[i].layers[@level] = {is_partial: true, is_root: false, parent_tile: @parent_tile}
                                        span_area[i].is_partial = true
                                @parent_tile.addLayer(@meta.name, [@w, @h], false)
        update: (delta) ->
            if @follow_path
                time = delta * 0.001
                desired_vel = []
                Vec2.sub(desired_vel, @target, @pos)
                Vec2.normalize(desired_vel, desired_vel)
                Vec2.scale(desired_vel, desired_vel, @max_vel)
                Vec2.sub(@steering_f, desired_vel, @velocity)
                Vec2.scale(@steering_f, @steering_f, 1/@mass)
                Vec2.scale(@velocity, @velocity, @max_vel)
                Vec2.add(@velocity, @velocity, @steering_f)
                Vec2.add(@pos, @pos, @velocity, time)
                Vec2.normalize(@velocity, @velocity) 
                @scene.trigger "ENTITY_MOVING", @
            super()

    return WalkableTile