"use strict"

define [
    "Ajax", 
    "../amjad/AmjadMap", 
    "MapLoaderBar", 
    "../amjad/empire/Empire", 
    "../amjad/hud/ActionMenu", 
    "../amjad/hud/BottomMenu", 
    "../amjad/hud/dialogs/TavernDialog",
    "../amjad/hud/dialogs/WartentDialog",
    "../amjad/hud/dialogs/CommanderDialog",
    "../amjad/hud/dialogs/CaravanDialog",
    "../amjad/hud/dialogs/TrainingGroundsDialog",
    "../amjad/hud/dialogs/TradeStationDialog"
],

(Ajax, AmjadMap, MapLoaderBar, Empire) ->

    class AmjadManager
        constructor: (@city_meta, @zone_meta) ->
            require ["../amjad/hud/HudEpilog"], () -> log.debug "hud epilog done"

            @hud = $("#hud")
            @loaded = {"zone": false, "city": false}
            @current_view = null

            @city = new AmjadMap({
                "name": "city",
                "numrows": @city_meta.numrows
                "numcols": @city_meta.numcols
                "tiledim":
                    "width": 128
                    "height": 64
            })
            @city.camera.pos[0] = -400
            @city.camera.pos[1] = -1000
            @city.camera.enableArrowKeys()
            @empire = new Empire()

            @zone = new AmjadMap({
                "name": "zone",
                "numrows": @zone_meta.numrows
                "numcols": @zone_meta.numcols
                "tiledim":
                    "width": 128
                    "height": 64
            })
            @zone.camera.enableArrowKeys()
            @zone.camera.pos[0] = -1800
            @zone.camera.pos[1] = -200
            @city_surrounded_counter = 0
            @enterZoneView()
            @init()

        loadMap: (map, url) ->
            $.getJSON "assets/maps/" + url, (data) =>
                mapbar = null
                mapbar = new MapLoaderBar(map, 
                    () =>
                        @loaded[map.name] = true
                        Hal.scm.addScene(map)
                        Hal.scm.putOnTop(map)
                        @hud.show()

                        map.trigger "CAMERA_MOVED", map.camera.pos
                        map.trigger "ZOOM", map.camera.zoom
                        map.update()
                )
                map.load
                    map: data
                    name: url

        enterCityView: () ->
            #return if @current_view is @city
            @current_view = @city
            @zone.pause()
            if not @loaded["city"]
                @loadMap(@city, @city_meta.url)
                Hal.on "SCENE_ADDED_CITY", () =>
                    if @city.clicked_layer
                        @city.clicked_layer.trigger "DESELECTED"
                    @city.clicked_layer = null
            else
                Hal.scm.putOnTop(@city)
                if @city.clicked_layer
                    @city.clicked_layer.trigger "DESELECTED"
                @city.clicked_layer = null

        enterZoneView: () ->
            #return if @current_view is @zone
            @current_view = @zone
            @city.pause()
            if not @loaded["zone"]
                @loadMap(@zone, @zone_meta.url)
                Hal.on "SCENE_ADDED_ZONE", () =>
                    if @zone.clicked_layer?
                        @zone.clicked_layer.trigger "DESELECTED"
                    @zone.clicked_layer = null
            else 
                Hal.scm.putOnTop(@zone)
                if @zone.clicked_layer?
                    @zone.clicked_layer.trigger "DESELECTED"
                @zone.clicked_layer = null

        init: () ->
            @city.on "WARTENT_ADDED", (tent) =>
                @empire.Buildings.WarTent = tent
            @city.on "TRAINING_GROUNDS_ADDED", (trgrounds) =>
                @empire.Buildings.TrainingGrounds = trgrounds
            @city.on "TAVERN_ADDED", (tavern) =>
                @empire.Buildings.Tavern = tavern
            @city.on "TRADE_STATION_ADDED", (trstation) =>
                @empire.Buildings.TradeStation = trstation

            # Hal.on "CONSTRUCT_BUILDING", (building) =>
            #     log.debug building
            #     @city.trigger "TILELAYER_SELECTED", building
            #     @city.showTileLayer()

            Handlebars.registerHelper "concat_sprite", (src) ->
                return "assets/sprites/#{src}.png"

        listCityBuildings: () ->
            log.debug "uhuohoh"
            blds = @city.tmngr.getBuildingsByType("city_#{@empire.fraction}")
            log.debug blds
            $bldslist = $("#buildings-list")
            $bldslist.empty()
            source = $("#city-buildings-list").html();
            template = Handlebars.compile(source);
            for k, building of blds
                log.debug k
                log.debug building
                html = $(template(building))
                html.data("building", building)
                html.unbind("click")
                html.click () ->
                    Hal.trigger "CONSTRUCT_BUILDING", $(@).data("building")
                log.debug html
                $bldslist.append(html)

    return AmjadManager