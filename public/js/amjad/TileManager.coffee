"use strict"


define [
    "Ajax", 
    "../amjad/TileLayer", 
    "../amjad/WalkableTile",
    "../amjad/buildings/BedouinCity",
    "../amjad/buildings/CityTavern",
    "../amjad/buildings/WarTent",
    "../amjad/heroes/Commander",
    "../amjad/heroes/CaravanLeader",
    "../amjad/buildings/TrainingGrounds",
    "../amjad/buildings/TradeStation"
], 

(Ajax, TileLayer, WalkableTile, BedouinCity, CityTavern, WarTent, Commander, CaravanLeader, TrainingGrounds, TradeStation) ->

    class TileManager
        constructor: () ->
            @Tiles = 
                buildings: {}
                character: {}
                terrain: {}
                decor: {}
                poi: {}

            @TilesByName = {}

            Ajax.get "assets/amjad/TilesList.json", (tiles) =>
                log.debug "TileManager loaded tiles."
                tiles = JSON.parse(tiles)
                for k, t of tiles
                    @addTile(t)

            #responds when a new tile is created within mapeditor
            Hal.on "TILE_ADDED", (tile) =>
                @addTile(tile)

        addTile: (tile) ->
            @Tiles[tile.level][tile.name] = tile
            @TilesByName[tile.name] = tile

        getTile: (key) ->
            return @TilesByName[key]

        newTileFromMeta: (level, meta, lpos, adjpos, renderer, parent) ->
            if meta.level is "character"
                if meta.attr.type is "commander"
                    return new Commander(level, meta, lpos, adjpos, renderer, parent)
                if meta.attr.type is "caravan"
                    return new CaravanLeader(level, meta, lpos, adjpos, renderer, parent)

            else if meta.level is "buildings"
                if meta.attr.type is "empire_building_bedouin"
                    return new BedouinCity(level, meta, lpos, adjpos, renderer, parent)
                else if meta.name is "buildings_tavern"
                    return new CityTavern(level, meta, lpos, adjpos, renderer, parent)
                else if meta.name is "buildings_training_grounds"
                    return new TrainingGrounds(level, meta, lpos, adjpos, renderer, parent)
                else if meta.name is "buildings_trade_station"
                    return new TradeStation(level, meta, lpos, adjpos, renderer, parent)
                else if meta.name is "buildings_wartent"
                    return new WarTent(level, meta, lpos, adjpos, renderer, parent)
                else
                    return new TileLayer(level, meta, lpos, adjpos, renderer, parent)
            else
                return new TileLayer(level, meta, lpos, adjpos, renderer, parent)
        

        getTilesByType: (cls, type) ->
            return if not cls in Object.keys(@Tiles)
            out = {}
            for k, v of @Tiles[cls]
                if v.attr.type is type
                    out[k] = v
            return out

        # getAllHeroes: () ->
        #     out = {}
        #     for ty in @valid_heroes
        #         out[ty] = @getHeroesByType(ty)
        #     return out

        # getBuildingsByType: (type) ->
        #     out = {}
        #     if type?
        #         for k, v of @Tiles.buildings
        #             if v.attr.type is type
        #                 out[k] = v
        #     #else
        #     #    return @getAllBuildings()
        #     return out

        # getAllCityBuildings: () ->
        #     out = {}
        #     for k, v of @Tiles.buildings
        #         out[k] = @getBuildingsByType("city")
        #     return out

    return new TileManager()