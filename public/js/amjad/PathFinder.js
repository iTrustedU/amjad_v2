(function() {
  "use strict";
  define(["../amjad/Tile", "MinHeap"], function(Tile, MinHeap) {
    var PathFinder, compareFs;
    compareFs = function(a, b) {
      return a.f < b.f;
    };
    return PathFinder = (function() {
      function PathFinder(map) {
        this.map = map;
        this.diagonal_cost = 1.4;
        this.straight_cost = 1.0;
      }

      PathFinder.prototype.tracePath = function(dest) {
        var out;
        out = [];
        while (dest.parent != null) {
          out.push(dest);
          dest = dest.parent;
        }
        return out;
      };

      PathFinder.prototype.find = function(from, to) {
        var closed, cost, cur, g, h, neighs, open, open_map, t, tt, _i, _len;
        open = new MinHeap(null, compareFs);
        closed = {};
        open_map = {};
        if ((from == null) || (to == null)) {
          console.log("no start or end node given");
          return;
        }
        open.push({
          row: from.row,
          col: from.col,
          id: from.id,
          f: 0,
          g: 0,
          h: 0,
          parent: null
        });
        open_map[from.id] = from;
        while (open.size() > 0) {
          cur = open.pop();
          open_map[cur.id] = null;
          if (cur.row === to.row && cur.col === to.col) {
            return this.tracePath(cur);
          }
          closed[cur.id] = cur;
          neighs = this.map.getReachableNeighbours(this.map.getTile(cur.row, cur.col));
          for (_i = 0, _len = neighs.length; _i < _len; _i++) {
            t = neighs[_i];
            if ((t.row === cur.row) || (t.col === cur.col)) {
              g = this.straight_cost;
            } else {
              g = this.diagonal_cost;
            }
            if (closed[t.id] != null) {
              continue;
            }
            if (open_map[t.id] == null) {
              /*
                  ovo kao dobro balansira putanju
              */

              /*
                  ovo ok radi
              */

              /*
                  ovo definitivno uvek trazi najkracu al' je skupo dabogsacuvaj
              */

              if ((t.col % 2) === 1) {
                cost = this.straight_cost;
              } else {
                cost = this.diagonal_cost;
              }
              /* eksperimentalno se pokazalo da ovo najbolje sljaka*/

              h = (Math.abs(to.row - t.row) + Math.abs(t.col - to.col)) * cost;
              open.push({
                h: h,
                row: t.row,
                id: t.id,
                col: t.col,
                g: cur.g + g,
                f: h + g,
                parent: cur
              });
              open_map[t.id] = t;
            } else {
              tt = open_map[t.id];
              if (cur.g + g < tt.g) {
                tt.g = cur.g + g;
                tt.f = tt.h + tt.g;
                tt.parent = cur;
              }
            }
          }
        }
      };

      return PathFinder;

    })();
  });

}).call(this);
