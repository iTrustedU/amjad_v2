(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../amjad/TileLayer", "Vec2"], function(TileLayer, Vec2) {
    var WalkableTile;
    WalkableTile = (function(_super) {
      __extends(WalkableTile, _super);

      function WalkableTile(level, meta, basepos, adjpos, renderer, parent) {
        WalkableTile.__super__.constructor.call(this, level, meta, basepos, adjpos, renderer, parent);
        this.mass = 3;
        this.gravity = Vec2.fromValues(0, 0);
        this.velocity = Vec2.fromValues(0.0001, 0.0001);
        this.max_vel = 4;
        this.target = this.pos;
        Vec2.sub(this.velocity, this.target, this.pos);
        this.steering_f = Vec2.fromValues(0, 0);
        this.max_steering_f = 0.3;
        this.max_speed = 0.4;
        this.moving = false;
        if (this.meta.listeners != null) {
          if (this.meta.listeners.FINISHED_WALKING != null) {
            this.on("FINISHED_WALKING", Function(this.meta.listeners.FINISHED_WALKING));
          }
        }
      }

      WalkableTile.prototype.init = function() {
        return WalkableTile.__super__.init.call(this);
      };

      WalkableTile.prototype.goTo = function(destination) {
        var out, p, path, tile, _i, _len;
        path = this.scene.pathfinder.find(this.parent_tile, destination);
        log.debug("pathfinding from");
        log.debug(this.parent_tile);
        log.debug("pathfinding to");
        log.debug(destination);
        log.debug(this.path);
        if (path == null) {
          return;
        }
        out = [];
        for (_i = 0, _len = path.length; _i < _len; _i++) {
          p = path[_i];
          tile = this.scene.getTile(p.row, p.col);
          out.unshift(tile.pos);
        }
        return this.followPath(out);
      };

      WalkableTile.prototype.followPath = function(path) {
        var _this = this;
        if (!this.follow_path) {
          return this.follow_path = Hal.on("ENTER_FRAME", function(delta) {
            var i, span_area, _i, _j, _ref, _ref1;
            _this.moving = true;
            if ((Math.abs(_this.pos[0] - _this.target[0]) + Math.abs(_this.pos[1] - _this.target[1])) < 5) {
              _this.target = path.shift();
              if (_this.target == null) {
                _this.moving = false;
                Hal.removeTrigger("ENTER_FRAME", _this.follow_path);
                _this.follow_path = null;
                _this.target = _this.pos;
                Vec2.sub(_this.velocity, _this.target, _this.pos);
                log.debug(_this.parent_tile);
                _this.trigger("FINISHED_WALKING", _this.parent_tile);
              } else {
                span_area = _this.scene.getSpanArea(_this.parent_tile, _this.meta.size);
                if (span_area.length > 0) {
                  for (i = _i = 0, _ref = span_area.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    span_area[i].removeLayer(_this.level, true);
                    span_area[i].is_partial = false;
                  }
                }
                _this.parent_tile = _this.scene.getTileAt([_this.target[0] + _this.scene.tilew2, _this.target[1] + _this.scene.tileh2]);
                if (((_this.parent_tile.layers[_this.level] == null) || !_this.parent_tile.layers[_this.level].is_partial) && (_this.parent_tile.layers[0] != null)) {
                  _this.parent_tile.layers[_this.level] = _this;
                  span_area = _this.scene.getSpanArea(_this.parent_tile, _this.meta.size);
                  if (span_area.length > 1) {
                    for (i = _j = 1, _ref1 = span_area.length - 1; 1 <= _ref1 ? _j <= _ref1 : _j >= _ref1; i = 1 <= _ref1 ? ++_j : --_j) {
                      span_area[i].layers[_this.level] = {
                        is_partial: true,
                        is_root: false,
                        parent_tile: _this.parent_tile
                      };
                      span_area[i].is_partial = true;
                    }
                  }
                  return _this.parent_tile.addLayer(_this.meta.name, [_this.w, _this.h], false);
                }
              }
            }
          });
        }
      };

      WalkableTile.prototype.update = function(delta) {
        var desired_vel, time;
        if (this.follow_path) {
          time = delta * 0.001;
          desired_vel = [];
          Vec2.sub(desired_vel, this.target, this.pos);
          Vec2.normalize(desired_vel, desired_vel);
          Vec2.scale(desired_vel, desired_vel, this.max_vel);
          Vec2.sub(this.steering_f, desired_vel, this.velocity);
          Vec2.scale(this.steering_f, this.steering_f, 1 / this.mass);
          Vec2.scale(this.velocity, this.velocity, this.max_vel);
          Vec2.add(this.velocity, this.velocity, this.steering_f);
          Vec2.add(this.pos, this.pos, this.velocity, time);
          Vec2.normalize(this.velocity, this.velocity);
          this.scene.trigger("ENTITY_MOVING", this);
        }
        return WalkableTile.__super__.update.call(this);
      };

      return WalkableTile;

    })(TileLayer);
    return WalkableTile;
  });

}).call(this);
