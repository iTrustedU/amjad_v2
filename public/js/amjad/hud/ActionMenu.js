(function() {
  "use strict";
  var html;

  html = "<div class=\"action-box\">\n    <div class=\"action-button action-button-1\" id=\"attack\">\n        <div class=\"action-button-left\"></div>\n        <span>Atack</span>\n        <div class=\"action-button-right\"></div>\n    </div>\n    <img src=\"img/dialog/training/hero-icon-12.png\" class=\"ation-box-img\">\n</div>";

  define([], function() {
    var ActionMenu;
    ActionMenu = $(html);
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append(ActionMenu);
    });
    ActionMenu.hide();
    return ActionMenu;
  });

}).call(this);
