"use strict"

html =
"""
<div class="commander draggable">
    <div class="dialog-name">
        <span>Commander</span>
        <div class="close-button"></div>
    </div>
    <div class="commander-info">
        <div class="commander-statistics">  
            <span class="commander-expirience-name">Expirience:</span>
            <span class="commander-current-expirience">27 000</span>
            <span class="commander-max-expirience">/ 30 000</span><br />
            <span class="commander-level-name">Readines:</span>
            <span class="commander-level-value">43</span><br />
            <span class="commander-level-name">Speed:</span>
            <span class="commander-level-value">72</span><br />
            <span class="commander-level-name">Leadership:</span>
            <span class="commander-level-value">0.5</span>
        </div>
    </div>
    <div class="commander-name">
    </div>
    <div class="commander-inventory">
    </div>
    <div class="commander-equipment">
        <img src="img/dialog/commander/equipslika.png" class="equip-img" />
        <img src="img/dialog/commander/equipment-holder.png" class="equipment-holder" />
        <div class="equipment-box-b head"></div>
        <div class="equipment-box-b back"></div>
        <div class="equipment-box-b torso"></div>
        <div class="equipment-box-b r-hand"></div>
        <div class="equipment-box-b l-hand"></div>
        <div class="equipment-box-b legs"></div>
        <div class="equipment-box-b boots"></div>
        <div class="equipment-box-s neck"></div>
        <div class="equipment-box-s l-ring"></div>
        <div class="equipment-box-s r-ring"></div>
    </div>
    <div class="commander-exp-bar-box">
        <div class="commander-bar">
            <div class="commander-bar-full"></div>
        </div>
    </div>
    <div class="commander-troops">
    </div>
    <div class="attach-agent">
        
    </div>
</div>
"""

$commander_name = 
"""
<span>{{name}}</span>
"""

$commander_stat =
"""
<div class="commander-statistics">  
    <span class="commander-expirience-name">Experience:</span>
    <span class="commander-current-expirience">{{experience}}</span>
    <span class="commander-max-expirience">/ {{next_level_in}}</span><br>
    <span class="commander-level-name">Readines:</span>
    <span class="commander-level-value">{{readiness}}</span><br>
    <span class="commander-level-name">Speed:</span>
    <span class="commander-level-value">{{speed}}</span><br>
    <span class="commander-level-name">Leadership:</span>
    <span class="commander-level-value">{{leadership}}</span>
</div>
"""
$commander_inventory = 
"""
<img src="{{concat_sprite sprite}}" id="{{name}}" type="{{type}}">
"""

$commander_units = 
"""
<img src="{{concat_sprite sprite}}">
"""


define [],
() ->
    $CommanderDialog = $(html)

    Hal.trigger "DOM_ADD", (domlayer) ->
        $("#hud").append($CommanderDialog)

    Hal.on "OPEN_COMMANDER_DIALOG", (commander) ->
        $CommanderDialog.data("commander", commander)

        #popunjavanje imena
        $name = $CommanderDialog.find(".commander-name")
        $name.empty()
        template = Handlebars.compile($commander_name);
        html = template(commander)
        $name.append(html)

        #popunjavanje stata
        $stat = $CommanderDialog.find(".commander-statistics")
        $stat.empty()

        template = Handlebars.compile($commander_stat);

        html = template(commander)
        $stat.append(html)

        #popunjavanje inventara
        $inventory = $CommanderDialog.find(".commander-inventory")
        $inventory.empty()

        template = Handlebars.compile($commander_inventory)

        for k,v of amjad.empire.AvailableEquipment
            log.debug v
            log.debug "tralalalalala"
            $html = $(template({name: k, sprite: v.sprite, type: v.type}))
            $html.draggable {revert: "invalid"}
            $inventory.append($html)


        #popunjavanje commanderovih trupa
        $commander_troops = $CommanderDialog.find(".commander-troops")
        $commander_troops.empty()
        template = Handlebars.compile($commander_units)

        for k, v of commander.units
            log.debug "unit: " + k
            html = template(v)
            $commander_troops.append(html)

        $CommanderDialog.find(".l-hand").droppable {
            activeClass: "l-hand-active"
            accept: (draggable) ->
                return draggable.attr("type") is "l-hand"

            drop: (ev, ui) ->
                log.debug "drop na levu ruku"
                name = ui.draggable.attr("id")
                ui.draggable.attr("style", "")
                $(@).append(ui.draggable)
                commander = $CommanderDialog.data("commander")
                commander.addInventory(name, amjad.empire.AvailableEquipment[name])
                amjad.empire.AvailableEquipment[name] = null
                delete amjad.empire.AvailableEquipment[name]
        }

        $CommanderDialog.find(".torso").droppable {
            activeClass: "torso-active"
            accept: (draggable) ->
                return draggable.attr("type") is "torso"

            drop: (ev, ui) ->
                log.debug "drop na torso"
                name = ui.draggable.attr("id")
                ui.draggable.attr("style", "")
                $(@).append(ui.draggable)
                commander = $CommanderDialog.data("commander")
                commander.addInventory(name, amjad.empire.AvailableEquipment[name])
                amjad.empire.AvailableEquipment[name] = null
                delete amjad.empire.AvailableEquipment[name]
        }

        $CommanderDialog.find(".r-hand").droppable {
            activeClass: "r-hand-active"
            accept: (draggable) ->
                return draggable.attr("type") is "r-hand"

            drop: (ev, ui) ->
                log.debug "drop na desnu ruku"
                name = ui.draggable.attr("id")
                ui.draggable.attr("style", "")
                $(@).append(ui.draggable)
                commander = $CommanderDialog.data("commander")
                commander.addInventory(name, amjad.empire.AvailableEquipment[name])
                amjad.empire.AvailableEquipment[name] = null
                delete amjad.empire.AvailableEquipment[name]
        }

        $CommanderDialog.fadeIn(200)