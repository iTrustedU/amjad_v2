html = 
"""
<div class="training draggable">
    <div class="dialog-name">
        <span>Training Grounds</span>
        <div class="close-button"></div>
    </div>
    <div class="trainer-statistic">

    </div>
    <div class="other-trainers">
    </div>
    <div class="training-troop">
        <div class="selected-troop">

        </div>
        <div class="select-troops">
            <div id="troops-list">
                
            </div>
            <div id="troop-stat">

            </div>
        </div>
        <div class="slider-amount">
            <span class="amount-name">Amount:</span>
            <input type="text" class="troop-amount" />
            <span class="min-max">Min</span>
            <div class="troop-slider">
                <div class="slider-bar-l"></div>
                <div class="slider-bar-r"></div>
                <div class="slider-bar-middle">
                    <div class="slider"></div>
                </div>
            </div>
            <span class="min-max">Max</span>
        </div>
    </div>
    <div class="train-button-box">Train</div>
    <div class="training-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img-2" />
        <div class="upgrade-building">Upgrade</div>
        <div class="upgrade-building move-button">Move</div>
    </div>  
</div>
"""

$trainer_stat =
"""
<img src="img/dialog/training/hero-icon-12.png" class="trainer-avatar" />
<div class="trainers-name-level">
    <span class="traniers-name">{{name}}</span>
    <span class="traniers-level">level:</span>
    <span class="traniers-level-value">{{level}}</span>
    <div class="tranier-stat">  
        <div class="tranier-stat-name"> 
            <span>Brawn:</span>
            <span>Reflexes:</span>
            <span>Endurance:</span>
            <span>Experience:</span>
        </div>
        <div class="tranier-stat-value">    
            <span>{{brawn}}</span>
            <span>{{reflexes}}</span>
            <span>{{endurance}}</span>
            <span>{{experience}}</span>
        </div>
    </div>
</div>
"""

$selected_troop = 
"""
<img height="196px" src="{{concat_sprite bigsprite}}" />
<div class="troop-price">
    <span>Gold: </span>
    <span id="gold" class="value">{{gold}} </span>
    <span>Wood: </span>
    <span id="wood" class="value">{{wood}} </span>
    <span>Iron: </span>
    <span id="iron" class="value">{{iron}} </span>
</div>
"""

$troops_list =
"""
    <img src="{{concat_sprite sprite}}" />
"""

$other_trainers =
"""
<img src="img/dialog/training/hero-icon-1-copy.png" class="training-reserve" />
"""

$troops_stat = 
"""
<div class="available-unit-info">
    <div class="ver-box">
        <span>Reg</span>
        <span>Exp</span>
        <span>Vet</span>
    </div>
    <div class="unit-info-name">
        <span>Health:</span>
        <span>Attack:</span>
        <span>Defense:</span>
        <span>Upkeep:</span>
    </div>
    <div class="unit-info-value">
        <span>{{health}}</span>
        <span>{{attack}}</span>
        <span>{{defense}}</span>
        <span>{{upkeep}}</span>
    </div>
    <div class="unit-info-name">
        <span class="br-span">Pillage:</span>
        <span>Raze:</span>
        <span>Steal:</span>
    </div>
    <div class="unit-info-value">
        <span class="br-span">1</span>
        <span>{{raze}}</span>
        <span>{{steal}}</span>
    </div>
    <div class="unit-info-name">
        <span class="price-name">Price:</span>
        <span>Gold:</span>
        <span>Wood:</span>
        <span>Iron:</span>
    </div>
    <div class="unit-info-value">
        <span class="br-span">{{gold}}</span>
        <span>{{wood}}</span>
        <span>{{iron}}</span>
    </div>
</div>
"""

define [],
() ->
    $TrainingGroundsDialog = $(html)

    $TrainButton = $TrainingGroundsDialog.find(".train-button-box")
    $TroopAmount = $TrainingGroundsDialog.find(".troop-amount")

    Hal.trigger "DOM_ADD", (domlayer) ->
        $("#hud").append($TrainingGroundsDialog)

    Hal.on "OPEN_TRAINING_GROUNDS_DIALOG", (trgrounds) ->
        $TrainingGroundsDialog.data("trgrounds", trgrounds)
        $SelectedTroop = null

        #popunjavanje trainer stata
        $stat = $TrainingGroundsDialog.find(".trainer-statistic")
        $stat.empty()
        $othertrs = $TrainingGroundsDialog.find(".other-trainers")
        $othertrs.empty()
        len = Object.keys(trgrounds.trainers).length

        template_stat = Handlebars.compile($trainer_stat)
        template_others = Handlebars.compile($other_trainers)
        if len > 0
            for name, trainer of trgrounds.trainers
                html = $(template_others(trainer))
                html.data("trainer", trainer)
                html.click () ->
                    $stat.empty()
                    stat = template_stat($(@).data("trainer"))
                    $stat.append(stat)
                $othertrs.append(html)
        else
            log.debug "no trainers in training grounds"

        template = Handlebars.compile($troops_list)
        $trlist = $("#troops-list")
        $trlist.empty()
        $trstat = $("#troop-stat")
        $trstat.empty()
        $selectedwindow = $(".selected-troop")
        $selectedwindow.empty()
        template_trstat = Handlebars.compile($troops_stat)
        template_selected = Handlebars.compile($selected_troop)

        for name, troop of amjad.empire.AvailableTroops
            html = $(template(troop))
            html.data("troop", troop)
            html.click () ->
                $trstat.empty()
                stat = template_trstat($(@).data("troop"))
                $trstat.append(stat)
                selstat = template_selected($(@).data("troop"))
                $selectedwindow.empty()
                $selectedwindow.append(selstat)
                $SelectedTroop = $(@).data("troop")
                $TroopAmount.val(1)

            $trlist.append(html)

        $Gold = $TrainingGroundsDialog.find("#gold")

        #popunjavanje unita za proizvodnju
        $TroopAmount.unbind "keyup"
        $TroopAmount.on "keyup", (ev) ->
            amount = $(@).val()
            log.debug "amount: #{amount}"
            troop = $SelectedTroop
            $Gold = $TrainingGroundsDialog.find("#gold")
            $Iron = $TrainingGroundsDialog.find("#iron")
            $Wood = $TrainingGroundsDialog.find("#wood")
            $Gold.text(troop.gold * amount)
            $Wood.text(troop.wood * amount)
            $Iron.text(troop.iron * amount)

        $TrainButton.unbind "click"
        $TrainButton.click () ->
            amount = $TroopAmount.val()
            for i in [0...amount]
                amjad.empire.Buildings.TrainingGrounds.addTroop($SelectedTroop)

        $TrainingGroundsDialog.fadeIn(200)

    changePrices = () ->
