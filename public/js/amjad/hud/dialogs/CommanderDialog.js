(function() {
  "use strict";
  var $commander_inventory, $commander_name, $commander_stat, $commander_units, html;

  html = "<div class=\"commander draggable\">\n    <div class=\"dialog-name\">\n        <span>Commander</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"commander-info\">\n        <div class=\"commander-statistics\">  \n            <span class=\"commander-expirience-name\">Expirience:</span>\n            <span class=\"commander-current-expirience\">27 000</span>\n            <span class=\"commander-max-expirience\">/ 30 000</span><br />\n            <span class=\"commander-level-name\">Readines:</span>\n            <span class=\"commander-level-value\">43</span><br />\n            <span class=\"commander-level-name\">Speed:</span>\n            <span class=\"commander-level-value\">72</span><br />\n            <span class=\"commander-level-name\">Leadership:</span>\n            <span class=\"commander-level-value\">0.5</span>\n        </div>\n    </div>\n    <div class=\"commander-name\">\n    </div>\n    <div class=\"commander-inventory\">\n    </div>\n    <div class=\"commander-equipment\">\n        <img src=\"img/dialog/commander/equipslika.png\" class=\"equip-img\" />\n        <img src=\"img/dialog/commander/equipment-holder.png\" class=\"equipment-holder\" />\n        <div class=\"equipment-box-b head\"></div>\n        <div class=\"equipment-box-b back\"></div>\n        <div class=\"equipment-box-b torso\"></div>\n        <div class=\"equipment-box-b r-hand\"></div>\n        <div class=\"equipment-box-b l-hand\"></div>\n        <div class=\"equipment-box-b legs\"></div>\n        <div class=\"equipment-box-b boots\"></div>\n        <div class=\"equipment-box-s neck\"></div>\n        <div class=\"equipment-box-s l-ring\"></div>\n        <div class=\"equipment-box-s r-ring\"></div>\n    </div>\n    <div class=\"commander-exp-bar-box\">\n        <div class=\"commander-bar\">\n            <div class=\"commander-bar-full\"></div>\n        </div>\n    </div>\n    <div class=\"commander-troops\">\n    </div>\n    <div class=\"attach-agent\">\n        \n    </div>\n</div>";

  $commander_name = "<span>{{name}}</span>";

  $commander_stat = "<div class=\"commander-statistics\">  \n    <span class=\"commander-expirience-name\">Experience:</span>\n    <span class=\"commander-current-expirience\">{{experience}}</span>\n    <span class=\"commander-max-expirience\">/ {{next_level_in}}</span><br>\n    <span class=\"commander-level-name\">Readines:</span>\n    <span class=\"commander-level-value\">{{readiness}}</span><br>\n    <span class=\"commander-level-name\">Speed:</span>\n    <span class=\"commander-level-value\">{{speed}}</span><br>\n    <span class=\"commander-level-name\">Leadership:</span>\n    <span class=\"commander-level-value\">{{leadership}}</span>\n</div>";

  $commander_inventory = "<img src=\"{{concat_sprite sprite}}\" id=\"{{name}}\" type=\"{{type}}\">";

  $commander_units = "<img src=\"{{concat_sprite sprite}}\">";

  define([], function() {
    var $CommanderDialog;
    $CommanderDialog = $(html);
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append($CommanderDialog);
    });
    return Hal.on("OPEN_COMMANDER_DIALOG", function(commander) {
      var $commander_troops, $html, $inventory, $name, $stat, k, template, v, _ref, _ref1;
      $CommanderDialog.data("commander", commander);
      $name = $CommanderDialog.find(".commander-name");
      $name.empty();
      template = Handlebars.compile($commander_name);
      html = template(commander);
      $name.append(html);
      $stat = $CommanderDialog.find(".commander-statistics");
      $stat.empty();
      template = Handlebars.compile($commander_stat);
      html = template(commander);
      $stat.append(html);
      $inventory = $CommanderDialog.find(".commander-inventory");
      $inventory.empty();
      template = Handlebars.compile($commander_inventory);
      _ref = amjad.empire.AvailableEquipment;
      for (k in _ref) {
        v = _ref[k];
        log.debug(v);
        log.debug("tralalalalala");
        $html = $(template({
          name: k,
          sprite: v.sprite,
          type: v.type
        }));
        $html.draggable({
          revert: "invalid"
        });
        $inventory.append($html);
      }
      $commander_troops = $CommanderDialog.find(".commander-troops");
      $commander_troops.empty();
      template = Handlebars.compile($commander_units);
      _ref1 = commander.units;
      for (k in _ref1) {
        v = _ref1[k];
        log.debug("unit: " + k);
        html = template(v);
        $commander_troops.append(html);
      }
      $CommanderDialog.find(".l-hand").droppable({
        activeClass: "l-hand-active",
        accept: function(draggable) {
          return draggable.attr("type") === "l-hand";
        },
        drop: function(ev, ui) {
          var name;
          log.debug("drop na levu ruku");
          name = ui.draggable.attr("id");
          ui.draggable.attr("style", "");
          $(this).append(ui.draggable);
          commander = $CommanderDialog.data("commander");
          commander.addInventory(name, amjad.empire.AvailableEquipment[name]);
          amjad.empire.AvailableEquipment[name] = null;
          return delete amjad.empire.AvailableEquipment[name];
        }
      });
      $CommanderDialog.find(".torso").droppable({
        activeClass: "torso-active",
        accept: function(draggable) {
          return draggable.attr("type") === "torso";
        },
        drop: function(ev, ui) {
          var name;
          log.debug("drop na torso");
          name = ui.draggable.attr("id");
          ui.draggable.attr("style", "");
          $(this).append(ui.draggable);
          commander = $CommanderDialog.data("commander");
          commander.addInventory(name, amjad.empire.AvailableEquipment[name]);
          amjad.empire.AvailableEquipment[name] = null;
          return delete amjad.empire.AvailableEquipment[name];
        }
      });
      $CommanderDialog.find(".r-hand").droppable({
        activeClass: "r-hand-active",
        accept: function(draggable) {
          return draggable.attr("type") === "r-hand";
        },
        drop: function(ev, ui) {
          var name;
          log.debug("drop na desnu ruku");
          name = ui.draggable.attr("id");
          ui.draggable.attr("style", "");
          $(this).append(ui.draggable);
          commander = $CommanderDialog.data("commander");
          commander.addInventory(name, amjad.empire.AvailableEquipment[name]);
          amjad.empire.AvailableEquipment[name] = null;
          return delete amjad.empire.AvailableEquipment[name];
        }
      });
      return $CommanderDialog.fadeIn(200);
    });
  });

}).call(this);
