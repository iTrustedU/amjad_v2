html =
"""
<div class="trade-point draggable ui-draggable" style="left: -490.625px; top: -60.625px;">
    <div class="dialog-name">
        <span>Trade Point</span>
        <div class="close-button"></div>
    </div>
    <div class="load-money">
        <span>Money:</span>
        <span>15547</span>
        <span>Load:</span>
        <span>1542</span>
    </div>
    <div class="trade-point-avatar">
        <img src="img/hero-window/hero-icon-1.png">
    </div>
    <div class="merchant">
        <div class="sale-trade-good">
            <img src="img/hero-window/hero-icon-1.png">
        </div>
        <div class="trade-point-inventory">
            <img src="img/dialog/commander/sword.png">
            <img src="img/dialog/commander/shield.png">
        </div>
        <input type="text">
        <div class="trade-point-slider-bar">
            <div class="slider-bar-l"></div>
            <div class="slider-bar-r"></div>
            <div class="slider-bar-middle">
                <div class="slider"></div>
            </div>
        </div>
        <div class="sell-all sell-button">Sell All</div>
        <div class="sell sell-button">Sell</div>
    </div>
    <div class="trade-p">
        <div class="sale-trade-good good2">
            <img src="img/hero-window/hero-icon-1.png">
        </div>
        <div class="trade-point-inventory">
            <img src="img/dialog/commander/sword.png">
            <img src="img/dialog/commander/shield.png">
        </div>
        <input type="text">
        <div class="trade-point-slider-bar">
            <div class="slider-bar-l"></div>
            <div class="slider-bar-r"></div>
            <div class="slider-bar-middle">
                <div class="slider"></div>
            </div>
        </div>
        <div class="buy sell-button">Buy</div>
    </div>
</div>
"""