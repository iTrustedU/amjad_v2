"use strict"

html = 
"""
<div class="war-commander draggable">
    <div class="dialog-name">
        <span>War Tent</span>
        <div class="close-button"></div>
    </div>
    <div class="tavern-tabs">
        <div class="chat-tab" id="commander-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Commander</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Agent</span>
            <div class="chat-tab-r"></div>
        </div>
        <div class="chat-tab">
            <div class="chat-tab-l"></div>
            <span class="chat-tab-middle">Research</span>
            <div class="chat-tab-r"></div>
        </div>
    </div>
    <div class="war-commander-info">
        <div class="war-bars"></div>
        <table>
            <!-- commander-troops -->
        </table>
        <div class="trained-units"> 

            <div class="war-troops-trained-units">
            <!-- wartent-troops -->
            </div>
        </div>
    </div>
    <div class="war-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
        <div class="upgrade-building">Upgrade</div>
        <div class="upgrade-building move-button">Move</div>
    </div>  
</div>
"""

$commander_row = 
"""
<tr>
    <td>
        <div class="war-troops">
            {{#each units}}
                <div class="unit-box">
                        <img src="{{concat_sprite sprite}}" id={{name}}>
                </div>
            {{/each}}
        </div>

        <div class="war-troop-info">
            <span class="available-unit-name">{{name}}</span>
            <span class="war-level-name">Level:</span>
            <span class="war-level-value">{{exp_level}}</span>
            <img src="img/dialog/training/hero-icon-12.png">
            <div class="war-unit-info-name">
                <span>Readiness:</span>
                <span>Speed:</span>
                <span>Leadership:</span>
            </div>
            <div class="war-unit-info-value">
                <span>{{meta.attr.readiness}}</span>
                <span>{{meta.attr.speed}}</span>
                <span>{{meta.attr.leadership}}</span>
            </div>
        </div>
    </td>
</tr>
</div>
"""

$wartent_trained_units = 
"""
<div class="unit-box" id="trained-unit" name={{name}}>
    <img src="{{concat_sprite sprite}}">
</div>
"""

$wartent_unit_stat = 
"""
<div id="stat">
<span class="war-available-unit-name">{{name}}</span>
<div class="unit-info-name war-name">
    <span>Health:</span>
    <span>Attack:</span>
    <span>Defense:</span>
    <span>Upkeep:</span>
</div>
<div class="unit-info-value">
    <span>{{health}}</span>
    <span>{{attack}}</span>
    <span>{{defense}}</span>
    <span>{{upkeep}}</span>
</div>
<div class="unit-info-name">
    <span>Pillage:</span>
    <span>Raze:</span>
    <span>Steal:</span>
</div>
<div class="unit-info-value">
    <span>{{pillage}}</span>
    <span>{{raze}}</span>
    <span>{{steal}}</span>
</div>
</div>
"""


define [], 
() ->

    $WartentDialog = $(html)
    $Table = $WartentDialog.find("table")
    $CommanderTab = $WartentDialog.find("#commander-tab")

    $WartentDialog.find(".close-button").click () ->
        $WartentDialog.data("wartent").trigger "DESELECTED"

    $CommanderTab.click () ->
        #popunjavanje komanderovih trupa
        #$WartentDialog.find(".trained-units").empty()
        $Table.empty()
        wartent = $WartentDialog.data("wartent")
        template = Handlebars.compile($commander_row)
        for k, data of wartent.commanders
            html = $(template(data))
            html.data("hero", data)
            $Table.append(html)

        #popunjavanje wartent troopa
        $trained_units_list = $WartentDialog.find(".war-troops-trained-units")
        $trained_units_list.empty()
        template = Handlebars.compile($wartent_trained_units);
        for k, unit of amjad.empire.Buildings.TrainingGrounds.troops
            html = $(template({name: k, sprite: unit.sprite}))
            html.draggable {revert: "invalid"}
            html.data("unit", unit)
            html.click () ->
                $WartentDialog.find("#stat").remove()
                template = Handlebars.compile($wartent_unit_stat)
                html = template($(@).data("unit"))
                $WartentDialog.find(".trained-units").append(html)

            $trained_units_list.append(html)

        $Table.find("tr").each (k, v) ->
            $(v).droppable({
                accept: "#trained-unit"
                drop: (ev, ui) ->
                    ui.draggable.attr("style", "")
                    $(@).find(".war-troops").append(ui.draggable)
                    name = ui.draggable.attr("name")
                    hero = $(@).data("hero")
                    tr = amjad.empire.Buildings.TrainingGrounds.troops[name]
                    hero.addUnit(name, tr)
                    amjad.empire.Buildings.TrainingGrounds.employTroop(tr)
                    $CommanderTab.click()
            })

    Hal.trigger "DOM_ADD", (domlayer) ->
        $("#hud").append($WartentDialog)

    Hal.on "OPEN_WARTENT_DIALOG", (wartent) ->
        return if amjad.current_view is not amjad.city
        log.debug wartent
        $WartentDialog.data("wartent", wartent)
        $WartentDialog.fadeIn(200)
        $CommanderTab.click()








    # return if @current_view is not @city
    # $Table.empty()
    # template = Handlebars.compile($wartent_commander_row);

    # for k, data of wartent.heroes["commander"]
    #     html = $(template(data))
    #     html.data("hero", data)
    #     $table.append(html)

    # $table.find("tr").each (k, v) ->
    #     log.debug v
    #     $(v).droppable({
    #         accept: "#trained-unit"
    #         drop: (ev, ui) ->
    #             log.debug "omigod"
    #             ui.draggable.attr("style", "")
    #             $(@).find(".war-troops").append(ui.draggable)
    #             name = ui.draggable.attr("name")
    #             hero = $(@).data("hero")
    #             hero.addUnit(name, amjad.empire.AvailableTroops[name])
    #             amjad.empire.AvailableTroops[name] = null
    #             delete amjad.empire.AvailableTroops[name]
    #     })

    # $trained_units_list = $tent.find(".war-troops-trained-units")

    # $trained_units_list.empty()

    # source = $("#wartent-trained-units").html();
    # template = Handlebars.compile(source);
    # for k, unit of @empire.AvailableTroops
    #     html = $(template({name: k, sprite: unit.sprite}))
    #     html.draggable {}
    #     $trained_units_list.append(html)

    # $tent.show()
