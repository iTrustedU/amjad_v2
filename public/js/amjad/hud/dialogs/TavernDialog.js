(function() {
  "use strict";
  var $hero_bottom_top, $tavern_caravan_row, $tavern_hero_row, $tavern_trainer_row, html;

  html = "<div class=\"tavern draggable\">\n    <div class=\"dialog-name\">\n        <span>Tavern</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"tavern-tabs\">\n        <div class=\"chat-tab\" id=\"commander\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Commander</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"agent\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Agent</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"caravan\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Caravan</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"trainer\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Trainer</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\" id=\"mercenary\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Mercenary</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n    </div>\n    <div class=\"tavern-info\">\n        <div class=\"bars\"></div>\n        <table>\n            <tr>\n                <th>Price</th>\n                <th>Statistic</th>\n                <th>Comamnder</th>\n            </tr>\n        </table>\n    </div>\n    <div class=\"tavern-building-info\">\n        <img src=\"img/bottom-middle/buildings/tavern.png\" class=\"dialog-building-img\" />\n        <div class=\"upgrade-building\">Upgrade</div>\n        <div class=\"upgrade-building move-button\">Move</div>\n    </div>  \n    <div class=\"employ-button\">Employ</div>\n</div>";

  $tavern_hero_row = "<tr class=\"\">\n    <td>{{attr.price}}</td>\n    <td>\n        <span class=\"statistics\">Readiness</span><span>{{attr.readiness}}</span><br>\n        <span class=\"statistics\">Speed</span><span>{{attr.speed}}</span><br>\n        <span class=\"statistics\">Leadership</span><span>{{attr.leadership}}</span><br>\n    </td>\n    <td>\n        <div class=\"tavern-hero-img\"></div><br>\n        <span class=\"tavern-hero-table-name\">{{name}}</span>\n    </td>\n</tr>";

  $tavern_trainer_row = "<tr class=\"\">\n    <td>{{price}}</td>\n    <td>\n        <span class=\"statistics\">Reflexes</span><span>{{reflexes}}</span><br>\n        <span class=\"statistics\">Brawn</span><span>{{brawn}}</span><br>\n        <span class=\"statistics\">Endurance</span><span>{{endurance}}</span><br>\n    </td>\n    <td>\n        <div class=\"tavern-hero-img\"></div><br>\n        <span class=\"tavern-hero-table-name\">{{name}}</span>\n    </td>\n</tr>";

  $tavern_caravan_row = "<tr class=\"\">\n    <td>{{attr.price}}</td>\n    <td>\n        <span class=\"statistics\">Speed</span><span>{{attr.speed}}</span><br>\n        <span class=\"statistics\">Capacity</span><span>{{attr.capacity}}</span><br>\n        <span class=\"statistics\">Tongue</span><span>{{attr.tongue}}</span><br>\n    </td>\n    <td>\n        <div class=\"tavern-hero-img\"></div><br>\n        <span class=\"tavern-hero-table-name\">{{name}}</span>\n    </td>\n</tr>";

  $hero_bottom_top = "<div class=\"hero-frame\">\n    <div class=\"hero-icon\"></div>\n        <div class=\"hero-name-title\">\n            <span class=\"hero-name\">{{meta.attr.name}}</span>\n            <span class=\"hero-title\">{{meta.attr.type}}</span>\n        </div>\n        <div class=\"expirience\">\n            <span>Level {{exp_level}}</span>\n            <div class=\"exp-bar\">\n                <div class=\"exp-inner-bar\">\n                    <div></div>\n                </div>  \n            </div>\n        </div>  \n    </div>\n</div>";

  define([], function() {
    var $CaravanTab, $CommanderTab, $EmployButton, $Table, $TavernDialog, $TrainerTab, addHeroToBottomTopMenu;
    $TavernDialog = $(html);
    $CommanderTab = $TavernDialog.find("#commander");
    $CaravanTab = $TavernDialog.find("#caravan");
    $TrainerTab = $TavernDialog.find("#trainer");
    $EmployButton = $TavernDialog.find(".employ-button");
    $Table = $TavernDialog.find("table");
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append($TavernDialog);
    });
    $TavernDialog.find(".close-button").click(function() {
      return $TavernDialog.data("tavern").trigger("DESELECTED");
    });
    $CommanderTab.click(function() {
      var hero, heroname, tavern, template, _ref;
      $Table.find("tr:gt(0)").remove();
      template = Handlebars.compile($tavern_hero_row);
      tavern = $TavernDialog.data("tavern");
      _ref = tavern.getHeroes("commander");
      for (heroname in _ref) {
        hero = _ref[heroname];
        html = $(template(hero));
        html.data("hero", hero);
        $Table.append(html);
      }
      return $Table.find("tr:gt(0)").each(function(k, v) {
        return $(v).click(function() {
          $('tr').not(this).removeClass('tr-click-bg');
          $(this).toggleClass("tr-click-bg");
          return log.debug($(this).data("hero"));
        });
      });
    });
    $CaravanTab.click(function() {
      var hero, heroname, tavern, template, _ref;
      $Table.find("tr:gt(0)").remove();
      template = Handlebars.compile($tavern_caravan_row);
      tavern = $TavernDialog.data("tavern");
      _ref = tavern.getHeroes("caravan");
      for (heroname in _ref) {
        hero = _ref[heroname];
        html = $(template(hero));
        html.data("hero", hero);
        $Table.append(html);
      }
      return $Table.find("tr:gt(0)").each(function(k, v) {
        return $(v).click(function() {
          $('tr').not(this).removeClass('tr-click-bg');
          $(this).toggleClass("tr-click-bg");
          return log.debug($(this).data("hero"));
        });
      });
    });
    $TrainerTab.click(function() {
      var tavern, template, trainer, trainername, _ref;
      $Table.find("tr:gt(0)").remove();
      template = Handlebars.compile($tavern_trainer_row);
      tavern = $TavernDialog.data("tavern");
      _ref = tavern.getHeroes("trainer");
      for (trainername in _ref) {
        trainer = _ref[trainername];
        html = $(template(trainer));
        html.data("hero", trainer);
        $Table.append(html);
      }
      return $Table.find("tr:gt(0)").each(function(k, v) {
        return $(v).click(function() {
          $('tr').not(this).removeClass('tr-click-bg');
          $(this).toggleClass("tr-click-bg");
          return log.debug($(this).data("hero"));
        });
      });
    });
    $EmployButton.click(function() {
      var hero, herot, selected_hero, tavern;
      selected_hero = $TavernDialog.find(".tr-click-bg");
      hero = selected_hero.data("hero");
      tavern = $TavernDialog.data("tavern");
      herot = amjad.empire.employHero(hero);
      if (herot != null) {
        selected_hero.remove();
        tavern.removeHero(hero);
        if (hero.type !== "trainer") {
          return addHeroToBottomTopMenu(herot);
        }
      }
    });
    Hal.on("OPEN_TAVERN_DIALOG", function(tavern) {
      if (amjad.current_view === !amjad.city) {
        return;
      }
      $TavernDialog.data("tavern", tavern);
      $TavernDialog.fadeIn(200);
      return $CommanderTab.click();
    });
    return addHeroToBottomTopMenu = function(hero) {
      var herolist, template;
      template = Handlebars.compile($hero_bottom_top);
      html = $(template(hero));
      html.data("hero", hero);
      html.css("cursor", "pointer");
      html.click(function() {
        hero = $(this).data("hero");
        if (hero.meta.attr.type === "caravan") {
          return Hal.trigger("OPEN_CARAVAN_DIALOG", hero);
        } else if (hero.meta.attr.type === "commander") {
          return Hal.trigger("OPEN_COMMANDER_DIALOG", hero);
        }
      });
      html.hide();
      herolist = $(".bottom-top > .heroes");
      herolist.append(html);
      return html.fadeIn();
    };
  });

}).call(this);
