(function() {
  var $caravan_inventor_info, $caravan_inventory, $caravan_name, $caravan_stat, $caravan_wm, html;

  html = "<div class=\"trader-screen draggable ui-draggable\">\n    <div class=\"dialog-name\">\n        <!-- caravan name -->\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"commander-info\">\n        <!-- caravan stat -->\n    </div>\n    <div class=\"trader-img\">\n        <img src=\"img/hero-window/hero-icon-1.png\">\n    </div>\n    <div class=\"trader-inventory\">\n        <!-- caravan inventory -->\n    </div>\n    <div class=\"trader-bar-box\">\n        <div class=\"trader-bar\">\n            <div class=\"trader-bar-full\"></div>\n        </div>\n    </div>\n    <div class=\"weight-money\">\n        <!-- weight money -->\n    </div>\n    <div class=\"trader-inventory-info\">\n        \n    </div>\n</div>";

  $caravan_wm = "<span>Weight:</span>\n<span>{{weight}}</span>\n<span> / 100</span><br>\n<span>Money:</span>\n<span>{{money}}</span>";

  $caravan_name = "<span>{{meta.attr.name}}</span>";

  $caravan_inventor_info = "<span>\n        {{description}}\n</span>";

  $caravan_inventory = "<img src=\"{{concat_sprite sprite}}\">";

  $caravan_stat = "<div class=\"commander-statistics\">  \n    <span class=\"commander-expirience-name\">Expirience:</span>\n    <span class=\"commander-current-expirience\">{{meta.attr.experience}}</span>\n    <span class=\"commander-max-expirience\">/ {{meta.attr.next_level_in}}</span><br>\n    <span class=\"commander-level-name\">Capacity:</span>\n    <span class=\"commander-level-value\">{{meta.attr.capacity}}</span><br>\n    <span class=\"commander-level-name\">Speed:</span>\n    <span class=\"commander-level-value\">{{meta.attr.speed}}</span><br>\n    <span class=\"commander-level-name\">Tongue:</span>\n    <span class=\"commander-level-value\">{{meta.attr.tongue}}</span>\n</div>";

  define([], function() {
    var $CaravanDialog;
    $CaravanDialog = $(html);
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append($CaravanDialog);
    });
    return Hal.on("OPEN_CARAVAN_DIALOG", function(caravan) {
      var $info, $name, $wm, template;
      $CaravanDialog.data("caravan", caravan);
      $name = $CaravanDialog.find(".dialog-name");
      $name.find("span").remove();
      template = Handlebars.compile($caravan_name);
      html = template(caravan);
      $name.prepend(html);
      $info = $CaravanDialog.find(".commander-info");
      $info.empty();
      template = Handlebars.compile($caravan_stat);
      html = template(caravan);
      $info.append(html);
      $wm = $CaravanDialog.find(".weight-money");
      $wm.empty();
      template = Handlebars.compile($caravan_wm);
      html = template(caravan);
      $wm.append(html);
      return $CaravanDialog.fadeIn(200);
    });
  });

}).call(this);
