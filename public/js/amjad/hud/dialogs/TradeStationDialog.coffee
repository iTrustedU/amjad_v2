html =
"""
<div class="trade-station draggable">
    <div class="dialog-name">
        <span>Trade Station</span>
        <div class="close-button"></div>
    </div>
    <div class="trader-info">
        <div class="trade-station-name">
            <span>Trade station name</span>
        </div>
        <div class="trader">
            <img src="img/dialog/trade-station/trader-select-saheeb-medium.png" />
            <span class="trader-name">Saheeb</span><br /><br />
            <div class="slider-bar-l"></div>
            <div class="slider-bar-r"></div>
            <div class="slider-bar-middle">
                <div class="slider"></div>
            </div>
        </div>
        <div class="trade-goods-own">
            
        </div>
        <div class="load-unload">
            <div class="unload"></div>
            <div class="load"></div>
        </div>
        <div class="gold-name">
            <span>Gold:</span>
        </div>
        <div class="gold-amount">
            <span>4560 GP</span>
        </div>
        <div class="trade-goods-inventory">
            <img src="img/dialog/commander/sword.png" />
        </div>
        <div class="load-text">
            <span>Load</span>
            <span>0</span>
            <span>/ 50</span>
        </div>
    </div>      
    <div class="available-traders">
        <span class="available-traders-name">Available Traders</span>
        <div class="available-traders-box">
            <img src="img/dialog/trade-station/trader-musta.png" />
            <div class="available-trader-info">
                <span class="trader-name-b">Mustafa</span><br />
                <span class="level-name-b">level:</span>
                <span class="level-value-b">4</span>
            </div>
        </div>
    </div>
    <div class="trade-building-info">
        <img src="img/bottom-middle/buildings/trade.png" class="dialog-building-img" />
        <div class="upgrade-building">Upgrade</div>
        <div class="upgrade-building move-button">Move</div>
    </div>  
</div>
"""

$own_goods =
"""
<img src="{{concat_sprite sprite}}" />
"""

define [],
() ->
    $TradeStationDialog = $(html)

    Hal.trigger "DOM_ADD", (domlayer) ->
        $("#hud").append($TradeStationDialog)

    Hal.on "OPEN_TRADE_STATION_DIALOG", (trstation) ->
        $TradeStationDialog.data("caravan", trstation)
        $TradeStationDialog.fadeIn(200)

        #prikaz sopstvenih goodova
        $tgoodsown = $TradeStationDialog.find(".trade-goods-own")
        $tgoodsown.empty()
        #template_stat = Handlebars.compile($trainer_stat)
        #template_others = Handlebars.compile($other_trainers)



        #prikaz tradestation goodova