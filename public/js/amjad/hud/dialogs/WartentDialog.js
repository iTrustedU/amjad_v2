(function() {
  "use strict";
  var $commander_row, $wartent_trained_units, $wartent_unit_stat, html;

  html = "<div class=\"war-commander draggable\">\n    <div class=\"dialog-name\">\n        <span>War Tent</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"tavern-tabs\">\n        <div class=\"chat-tab\" id=\"commander-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Commander</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Agent</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n        <div class=\"chat-tab\">\n            <div class=\"chat-tab-l\"></div>\n            <span class=\"chat-tab-middle\">Research</span>\n            <div class=\"chat-tab-r\"></div>\n        </div>\n    </div>\n    <div class=\"war-commander-info\">\n        <div class=\"war-bars\"></div>\n        <table>\n            <!-- commander-troops -->\n        </table>\n        <div class=\"trained-units\"> \n\n            <div class=\"war-troops-trained-units\">\n            <!-- wartent-troops -->\n            </div>\n        </div>\n    </div>\n    <div class=\"war-building-info\">\n        <img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img\" />\n        <div class=\"upgrade-building\">Upgrade</div>\n        <div class=\"upgrade-building move-button\">Move</div>\n    </div>  \n</div>";

  $commander_row = "<tr>\n    <td>\n        <div class=\"war-troops\">\n            {{#each units}}\n                <div class=\"unit-box\">\n                        <img src=\"{{concat_sprite sprite}}\" id={{name}}>\n                </div>\n            {{/each}}\n        </div>\n\n        <div class=\"war-troop-info\">\n            <span class=\"available-unit-name\">{{name}}</span>\n            <span class=\"war-level-name\">Level:</span>\n            <span class=\"war-level-value\">{{exp_level}}</span>\n            <img src=\"img/dialog/training/hero-icon-12.png\">\n            <div class=\"war-unit-info-name\">\n                <span>Readiness:</span>\n                <span>Speed:</span>\n                <span>Leadership:</span>\n            </div>\n            <div class=\"war-unit-info-value\">\n                <span>{{meta.attr.readiness}}</span>\n                <span>{{meta.attr.speed}}</span>\n                <span>{{meta.attr.leadership}}</span>\n            </div>\n        </div>\n    </td>\n</tr>\n</div>";

  $wartent_trained_units = "<div class=\"unit-box\" id=\"trained-unit\" name={{name}}>\n    <img src=\"{{concat_sprite sprite}}\">\n</div>";

  $wartent_unit_stat = "<div id=\"stat\">\n<span class=\"war-available-unit-name\">{{name}}</span>\n<div class=\"unit-info-name war-name\">\n    <span>Health:</span>\n    <span>Attack:</span>\n    <span>Defense:</span>\n    <span>Upkeep:</span>\n</div>\n<div class=\"unit-info-value\">\n    <span>{{health}}</span>\n    <span>{{attack}}</span>\n    <span>{{defense}}</span>\n    <span>{{upkeep}}</span>\n</div>\n<div class=\"unit-info-name\">\n    <span>Pillage:</span>\n    <span>Raze:</span>\n    <span>Steal:</span>\n</div>\n<div class=\"unit-info-value\">\n    <span>{{pillage}}</span>\n    <span>{{raze}}</span>\n    <span>{{steal}}</span>\n</div>\n</div>";

  define([], function() {
    var $CommanderTab, $Table, $WartentDialog;
    $WartentDialog = $(html);
    $Table = $WartentDialog.find("table");
    $CommanderTab = $WartentDialog.find("#commander-tab");
    $WartentDialog.find(".close-button").click(function() {
      return $WartentDialog.data("wartent").trigger("DESELECTED");
    });
    $CommanderTab.click(function() {
      var $trained_units_list, data, k, template, unit, wartent, _ref, _ref1;
      $Table.empty();
      wartent = $WartentDialog.data("wartent");
      template = Handlebars.compile($commander_row);
      _ref = wartent.commanders;
      for (k in _ref) {
        data = _ref[k];
        html = $(template(data));
        html.data("hero", data);
        $Table.append(html);
      }
      $trained_units_list = $WartentDialog.find(".war-troops-trained-units");
      $trained_units_list.empty();
      template = Handlebars.compile($wartent_trained_units);
      _ref1 = amjad.empire.Buildings.TrainingGrounds.troops;
      for (k in _ref1) {
        unit = _ref1[k];
        html = $(template({
          name: k,
          sprite: unit.sprite
        }));
        html.draggable({
          revert: "invalid"
        });
        html.data("unit", unit);
        html.click(function() {
          $WartentDialog.find("#stat").remove();
          template = Handlebars.compile($wartent_unit_stat);
          html = template($(this).data("unit"));
          return $WartentDialog.find(".trained-units").append(html);
        });
        $trained_units_list.append(html);
      }
      return $Table.find("tr").each(function(k, v) {
        return $(v).droppable({
          accept: "#trained-unit",
          drop: function(ev, ui) {
            var hero, name, tr;
            ui.draggable.attr("style", "");
            $(this).find(".war-troops").append(ui.draggable);
            name = ui.draggable.attr("name");
            hero = $(this).data("hero");
            tr = amjad.empire.Buildings.TrainingGrounds.troops[name];
            hero.addUnit(name, tr);
            amjad.empire.Buildings.TrainingGrounds.employTroop(tr);
            return $CommanderTab.click();
          }
        });
      });
    });
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append($WartentDialog);
    });
    return Hal.on("OPEN_WARTENT_DIALOG", function(wartent) {
      if (amjad.current_view === !amjad.city) {
        return;
      }
      log.debug(wartent);
      $WartentDialog.data("wartent", wartent);
      $WartentDialog.fadeIn(200);
      return $CommanderTab.click();
    });
  });

}).call(this);
