(function() {
  var $own_goods, html;

  html = "<div class=\"trade-station draggable\">\n    <div class=\"dialog-name\">\n        <span>Trade Station</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"trader-info\">\n        <div class=\"trade-station-name\">\n            <span>Trade station name</span>\n        </div>\n        <div class=\"trader\">\n            <img src=\"img/dialog/trade-station/trader-select-saheeb-medium.png\" />\n            <span class=\"trader-name\">Saheeb</span><br /><br />\n            <div class=\"slider-bar-l\"></div>\n            <div class=\"slider-bar-r\"></div>\n            <div class=\"slider-bar-middle\">\n                <div class=\"slider\"></div>\n            </div>\n        </div>\n        <div class=\"trade-goods-own\">\n            \n        </div>\n        <div class=\"load-unload\">\n            <div class=\"unload\"></div>\n            <div class=\"load\"></div>\n        </div>\n        <div class=\"gold-name\">\n            <span>Gold:</span>\n        </div>\n        <div class=\"gold-amount\">\n            <span>4560 GP</span>\n        </div>\n        <div class=\"trade-goods-inventory\">\n            <img src=\"img/dialog/commander/sword.png\" />\n        </div>\n        <div class=\"load-text\">\n            <span>Load</span>\n            <span>0</span>\n            <span>/ 50</span>\n        </div>\n    </div>      \n    <div class=\"available-traders\">\n        <span class=\"available-traders-name\">Available Traders</span>\n        <div class=\"available-traders-box\">\n            <img src=\"img/dialog/trade-station/trader-musta.png\" />\n            <div class=\"available-trader-info\">\n                <span class=\"trader-name-b\">Mustafa</span><br />\n                <span class=\"level-name-b\">level:</span>\n                <span class=\"level-value-b\">4</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"trade-building-info\">\n        <img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img\" />\n        <div class=\"upgrade-building\">Upgrade</div>\n        <div class=\"upgrade-building move-button\">Move</div>\n    </div>  \n</div>";

  $own_goods = "<img src=\"{{concat_sprite sprite}}\" />";

  define([], function() {
    var $TradeStationDialog;
    $TradeStationDialog = $(html);
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append($TradeStationDialog);
    });
    return Hal.on("OPEN_TRADE_STATION_DIALOG", function(trstation) {
      var $tgoodsown;
      $TradeStationDialog.data("caravan", trstation);
      $TradeStationDialog.fadeIn(200);
      $tgoodsown = $TradeStationDialog.find(".trade-goods-own");
      return $tgoodsown.empty();
    });
  });

}).call(this);
