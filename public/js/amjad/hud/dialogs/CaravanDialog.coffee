html = 
"""
<div class="trader-screen draggable ui-draggable">
    <div class="dialog-name">
        <!-- caravan name -->
        <div class="close-button"></div>
    </div>
    <div class="commander-info">
        <!-- caravan stat -->
    </div>
    <div class="trader-img">
        <img src="img/hero-window/hero-icon-1.png">
    </div>
    <div class="trader-inventory">
        <!-- caravan inventory -->
    </div>
    <div class="trader-bar-box">
        <div class="trader-bar">
            <div class="trader-bar-full"></div>
        </div>
    </div>
    <div class="weight-money">
        <!-- weight money -->
    </div>
    <div class="trader-inventory-info">
        
    </div>
</div>
"""

$caravan_wm = 
"""
<span>Weight:</span>
<span>{{weight}}</span>
<span> / 100</span><br>
<span>Money:</span>
<span>{{money}}</span>
"""

$caravan_name = 
"""
<span>{{meta.attr.name}}</span>
"""

$caravan_inventor_info = 
"""
<span>
        {{description}}
</span>
"""

$caravan_inventory = 
"""
    <img src="{{concat_sprite sprite}}">
"""

$caravan_stat =
"""
<div class="commander-statistics">  
    <span class="commander-expirience-name">Expirience:</span>
    <span class="commander-current-expirience">{{meta.attr.experience}}</span>
    <span class="commander-max-expirience">/ {{meta.attr.next_level_in}}</span><br>
    <span class="commander-level-name">Capacity:</span>
    <span class="commander-level-value">{{meta.attr.capacity}}</span><br>
    <span class="commander-level-name">Speed:</span>
    <span class="commander-level-value">{{meta.attr.speed}}</span><br>
    <span class="commander-level-name">Tongue:</span>
    <span class="commander-level-value">{{meta.attr.tongue}}</span>
</div>
"""

define [],
() ->
    $CaravanDialog = $(html)

    Hal.trigger "DOM_ADD", (domlayer) ->
        $("#hud").append($CaravanDialog)

    Hal.on "OPEN_CARAVAN_DIALOG", (caravan) ->
        $CaravanDialog.data("caravan", caravan)

        #popunjavanje imena
        $name = $CaravanDialog.find(".dialog-name")
        $name.find("span").remove()
        template = Handlebars.compile($caravan_name);
        html = template(caravan)
        $name.prepend(html)

        #popunjavanje stata
        $info = $CaravanDialog.find(".commander-info")
        $info.empty()
        template = Handlebars.compile($caravan_stat);
        html = template(caravan)
        $info.append(html)

        #popunjavanje weight money
        $wm = $CaravanDialog.find(".weight-money")
        $wm.empty()
        template = Handlebars.compile($caravan_wm)
        html = template(caravan)
        $wm.append(html)

        #popunjavanje goodova koje nosi

        $CaravanDialog.fadeIn(200)