(function() {
  var $other_trainers, $selected_troop, $trainer_stat, $troops_list, $troops_stat, html;

  html = "<div class=\"training draggable\">\n    <div class=\"dialog-name\">\n        <span>Training Grounds</span>\n        <div class=\"close-button\"></div>\n    </div>\n    <div class=\"trainer-statistic\">\n\n    </div>\n    <div class=\"other-trainers\">\n    </div>\n    <div class=\"training-troop\">\n        <div class=\"selected-troop\">\n\n        </div>\n        <div class=\"select-troops\">\n            <div id=\"troops-list\">\n                \n            </div>\n            <div id=\"troop-stat\">\n\n            </div>\n        </div>\n        <div class=\"slider-amount\">\n            <span class=\"amount-name\">Amount:</span>\n            <input type=\"text\" class=\"troop-amount\" />\n            <span class=\"min-max\">Min</span>\n            <div class=\"troop-slider\">\n                <div class=\"slider-bar-l\"></div>\n                <div class=\"slider-bar-r\"></div>\n                <div class=\"slider-bar-middle\">\n                    <div class=\"slider\"></div>\n                </div>\n            </div>\n            <span class=\"min-max\">Max</span>\n        </div>\n    </div>\n    <div class=\"train-button-box\">Train</div>\n    <div class=\"training-building-info\">\n        <img src=\"img/bottom-middle/buildings/trade.png\" class=\"dialog-building-img-2\" />\n        <div class=\"upgrade-building\">Upgrade</div>\n        <div class=\"upgrade-building move-button\">Move</div>\n    </div>  \n</div>";

  $trainer_stat = "<img src=\"img/dialog/training/hero-icon-12.png\" class=\"trainer-avatar\" />\n<div class=\"trainers-name-level\">\n    <span class=\"traniers-name\">{{name}}</span>\n    <span class=\"traniers-level\">level:</span>\n    <span class=\"traniers-level-value\">{{level}}</span>\n    <div class=\"tranier-stat\">  \n        <div class=\"tranier-stat-name\"> \n            <span>Brawn:</span>\n            <span>Reflexes:</span>\n            <span>Endurance:</span>\n            <span>Experience:</span>\n        </div>\n        <div class=\"tranier-stat-value\">    \n            <span>{{brawn}}</span>\n            <span>{{reflexes}}</span>\n            <span>{{endurance}}</span>\n            <span>{{experience}}</span>\n        </div>\n    </div>\n</div>";

  $selected_troop = "<img height=\"196px\" src=\"{{concat_sprite bigsprite}}\" />\n<div class=\"troop-price\">\n    <span>Gold: </span>\n    <span id=\"gold\" class=\"value\">{{gold}} </span>\n    <span>Wood: </span>\n    <span id=\"wood\" class=\"value\">{{wood}} </span>\n    <span>Iron: </span>\n    <span id=\"iron\" class=\"value\">{{iron}} </span>\n</div>";

  $troops_list = "<img src=\"{{concat_sprite sprite}}\" />";

  $other_trainers = "<img src=\"img/dialog/training/hero-icon-1-copy.png\" class=\"training-reserve\" />";

  $troops_stat = "<div class=\"available-unit-info\">\n    <div class=\"ver-box\">\n        <span>Reg</span>\n        <span>Exp</span>\n        <span>Vet</span>\n    </div>\n    <div class=\"unit-info-name\">\n        <span>Health:</span>\n        <span>Attack:</span>\n        <span>Defense:</span>\n        <span>Upkeep:</span>\n    </div>\n    <div class=\"unit-info-value\">\n        <span>{{health}}</span>\n        <span>{{attack}}</span>\n        <span>{{defense}}</span>\n        <span>{{upkeep}}</span>\n    </div>\n    <div class=\"unit-info-name\">\n        <span class=\"br-span\">Pillage:</span>\n        <span>Raze:</span>\n        <span>Steal:</span>\n    </div>\n    <div class=\"unit-info-value\">\n        <span class=\"br-span\">1</span>\n        <span>{{raze}}</span>\n        <span>{{steal}}</span>\n    </div>\n    <div class=\"unit-info-name\">\n        <span class=\"price-name\">Price:</span>\n        <span>Gold:</span>\n        <span>Wood:</span>\n        <span>Iron:</span>\n    </div>\n    <div class=\"unit-info-value\">\n        <span class=\"br-span\">{{gold}}</span>\n        <span>{{wood}}</span>\n        <span>{{iron}}</span>\n    </div>\n</div>";

  define([], function() {
    var $TrainButton, $TrainingGroundsDialog, $TroopAmount, changePrices;
    $TrainingGroundsDialog = $(html);
    $TrainButton = $TrainingGroundsDialog.find(".train-button-box");
    $TroopAmount = $TrainingGroundsDialog.find(".troop-amount");
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud").append($TrainingGroundsDialog);
    });
    Hal.on("OPEN_TRAINING_GROUNDS_DIALOG", function(trgrounds) {
      var $Gold, $SelectedTroop, $othertrs, $selectedwindow, $stat, $trlist, $trstat, len, name, template, template_others, template_selected, template_stat, template_trstat, trainer, troop, _ref, _ref1;
      $TrainingGroundsDialog.data("trgrounds", trgrounds);
      $SelectedTroop = null;
      $stat = $TrainingGroundsDialog.find(".trainer-statistic");
      $stat.empty();
      $othertrs = $TrainingGroundsDialog.find(".other-trainers");
      $othertrs.empty();
      len = Object.keys(trgrounds.trainers).length;
      template_stat = Handlebars.compile($trainer_stat);
      template_others = Handlebars.compile($other_trainers);
      if (len > 0) {
        _ref = trgrounds.trainers;
        for (name in _ref) {
          trainer = _ref[name];
          html = $(template_others(trainer));
          html.data("trainer", trainer);
          html.click(function() {
            var stat;
            $stat.empty();
            stat = template_stat($(this).data("trainer"));
            return $stat.append(stat);
          });
          $othertrs.append(html);
        }
      } else {
        log.debug("no trainers in training grounds");
      }
      template = Handlebars.compile($troops_list);
      $trlist = $("#troops-list");
      $trlist.empty();
      $trstat = $("#troop-stat");
      $trstat.empty();
      $selectedwindow = $(".selected-troop");
      $selectedwindow.empty();
      template_trstat = Handlebars.compile($troops_stat);
      template_selected = Handlebars.compile($selected_troop);
      _ref1 = amjad.empire.AvailableTroops;
      for (name in _ref1) {
        troop = _ref1[name];
        html = $(template(troop));
        html.data("troop", troop);
        html.click(function() {
          var selstat, stat;
          $trstat.empty();
          stat = template_trstat($(this).data("troop"));
          $trstat.append(stat);
          selstat = template_selected($(this).data("troop"));
          $selectedwindow.empty();
          $selectedwindow.append(selstat);
          $SelectedTroop = $(this).data("troop");
          return $TroopAmount.val(1);
        });
        $trlist.append(html);
      }
      $Gold = $TrainingGroundsDialog.find("#gold");
      $TroopAmount.unbind("keyup");
      $TroopAmount.on("keyup", function(ev) {
        var $Iron, $Wood, amount;
        amount = $(this).val();
        log.debug("amount: " + amount);
        troop = $SelectedTroop;
        $Gold = $TrainingGroundsDialog.find("#gold");
        $Iron = $TrainingGroundsDialog.find("#iron");
        $Wood = $TrainingGroundsDialog.find("#wood");
        $Gold.text(troop.gold * amount);
        $Wood.text(troop.wood * amount);
        return $Iron.text(troop.iron * amount);
      });
      $TrainButton.unbind("click");
      $TrainButton.click(function() {
        var amount, i, _i, _results;
        amount = $TroopAmount.val();
        _results = [];
        for (i = _i = 0; 0 <= amount ? _i < amount : _i > amount; i = 0 <= amount ? ++_i : --_i) {
          _results.push(amjad.empire.Buildings.TrainingGrounds.addTroop($SelectedTroop));
        }
        return _results;
      });
      return $TrainingGroundsDialog.fadeIn(200);
    });
    return changePrices = function() {};
  });

}).call(this);
