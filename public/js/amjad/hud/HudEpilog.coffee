"use strict"

define () ->
    $(".close-button").click () ->
        $(@).parent().parent().fadeOut(100)

    $(".draggable").draggable({
        handle: ".dialog-name"
    })

    $(".full-screen-button").click () ->
        if not Hal.supports("FULLSCREEN")
            log.warn "fullscreen not supported on this device"
        else
            Hal.trigger "REQUEST_FULLSCREEN"

    $(document).on "keydown", (ev) ->
        if ev.keyCode is 27
            $(".draggable").fadeOut(100)