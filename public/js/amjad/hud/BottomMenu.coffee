"use strict"
html = 
"""
<div class="left-section">
    <div class="map-box"></div>
</div>
<div class="right-section">
    <div class="ofdsaij"></div>
    <img src="img/by-gold/premium-dinar.png" />
    <div class="ofdsaij of2"></div>
    <div class="by-gold-text"></div>
</div>
<div class="middle-section">
    <div class="bot-bar">   
        <div class="bot-bar-deco-l"></div>
        <div class="bot-bar-deco-r"></div>
        <div class="bot-bar-deco-middle">
            <div class="bot-bar-deco-name">
                <div class="friends-tab" id="friends-tab"></div>
                <div class="alliance-tab" id="aliance-tab"></div>
                <div class="bot-bar-deco-name-left"></div>
                <span class="bot-bar-deco-name-middle">Friends</span>
                <div class="bot-bar-deco-name-right"></div>
                <div class="inventory-tab"></div>
                <div class="build-tab" id="build-tab"></div>
            </div>
        </div>
    </div>
    <div class="friends frame-bg">
        <img src="img/bottom-middle/friends-frame.png" class="img-frame" />
    </div>
    <div class="buildings frame-bg" id="buildings-list">
    </div>
    <div class="slider-bar">
        <div class="slider-bar-l"></div>
        <div class="slider-bar-r"></div>
        <div class="slider-bar-middle">
            <div class="slider"></div>
        </div>
    </div>
</div>
"""


$bldslist = 
"""
<img src="{{concat_sprite sprite}}" class="img-frame">
"""

define [], 
() ->
    $BottomMenu = $(html)
    $BuildTab = $BottomMenu.find("#build-tab")
    $FriendsTab = $BottomMenu.find("#friends-tab")
    $ZoneViewButton = $BottomMenu.find(".map-box")
    $BuildingsList = $BottomMenu.find("#buildings-list")

    $BuildTab.click () ->
        $(".friends").hide()
        $(".buildings").show()
        Hal.trigger "LIST_CITY_BUILDINGS"

    $FriendsTab.click () ->
        $(".buildings").hide()
        $(".friends").show()
        Hal.trigger "LIST_EMPIRE_FRIENDS"

    $ZoneViewButton.click () ->
        if amjad.current_view is amjad.city
            amjad.enterZoneView()
        else
            amjad.enterCityView()

    Hal.trigger "DOM_ADD", (domlayer) ->
        $("#hud > .bottom").append($BottomMenu)

    Hal.on "LIST_CITY_BUILDINGS", () ->
        blds = amjad.city.tmngr.getTilesByType("buildings", "city_building_#{amjad.empire.fraction}")
        $BuildingsList.empty()
        template = Handlebars.compile($bldslist);
        for k, building of blds
            html = $(template(building))
            html.data("building", building)
            html.unbind("click")
            html.click () ->
                bld = $(@).data("building")
                Hal.trigger "CONSTRUCT_BUILDINGS", bld 
            $BuildingsList.append(html)

    Hal.on "CONSTRUCT_BUILDINGS", (building) ->
        amjad.city.trigger "TILELAYER_SELECTED", building
        amjad.city.showTileLayer()

    Hal.on "TAVERN_ADDED", () ->
