(function() {
  "use strict";
  var $bldslist, html;

  html = "<div class=\"left-section\">\n    <div class=\"map-box\"></div>\n</div>\n<div class=\"right-section\">\n    <div class=\"ofdsaij\"></div>\n    <img src=\"img/by-gold/premium-dinar.png\" />\n    <div class=\"ofdsaij of2\"></div>\n    <div class=\"by-gold-text\"></div>\n</div>\n<div class=\"middle-section\">\n    <div class=\"bot-bar\">   \n        <div class=\"bot-bar-deco-l\"></div>\n        <div class=\"bot-bar-deco-r\"></div>\n        <div class=\"bot-bar-deco-middle\">\n            <div class=\"bot-bar-deco-name\">\n                <div class=\"friends-tab\" id=\"friends-tab\"></div>\n                <div class=\"alliance-tab\" id=\"aliance-tab\"></div>\n                <div class=\"bot-bar-deco-name-left\"></div>\n                <span class=\"bot-bar-deco-name-middle\">Friends</span>\n                <div class=\"bot-bar-deco-name-right\"></div>\n                <div class=\"inventory-tab\"></div>\n                <div class=\"build-tab\" id=\"build-tab\"></div>\n            </div>\n        </div>\n    </div>\n    <div class=\"friends frame-bg\">\n        <img src=\"img/bottom-middle/friends-frame.png\" class=\"img-frame\" />\n    </div>\n    <div class=\"buildings frame-bg\" id=\"buildings-list\">\n    </div>\n    <div class=\"slider-bar\">\n        <div class=\"slider-bar-l\"></div>\n        <div class=\"slider-bar-r\"></div>\n        <div class=\"slider-bar-middle\">\n            <div class=\"slider\"></div>\n        </div>\n    </div>\n</div>";

  $bldslist = "<img src=\"{{concat_sprite sprite}}\" class=\"img-frame\">";

  define([], function() {
    var $BottomMenu, $BuildTab, $BuildingsList, $FriendsTab, $ZoneViewButton;
    $BottomMenu = $(html);
    $BuildTab = $BottomMenu.find("#build-tab");
    $FriendsTab = $BottomMenu.find("#friends-tab");
    $ZoneViewButton = $BottomMenu.find(".map-box");
    $BuildingsList = $BottomMenu.find("#buildings-list");
    $BuildTab.click(function() {
      $(".friends").hide();
      $(".buildings").show();
      return Hal.trigger("LIST_CITY_BUILDINGS");
    });
    $FriendsTab.click(function() {
      $(".buildings").hide();
      $(".friends").show();
      return Hal.trigger("LIST_EMPIRE_FRIENDS");
    });
    $ZoneViewButton.click(function() {
      if (amjad.current_view === amjad.city) {
        return amjad.enterZoneView();
      } else {
        return amjad.enterCityView();
      }
    });
    Hal.trigger("DOM_ADD", function(domlayer) {
      return $("#hud > .bottom").append($BottomMenu);
    });
    Hal.on("LIST_CITY_BUILDINGS", function() {
      var blds, building, k, template, _results;
      blds = amjad.city.tmngr.getTilesByType("buildings", "city_building_" + amjad.empire.fraction);
      $BuildingsList.empty();
      template = Handlebars.compile($bldslist);
      _results = [];
      for (k in blds) {
        building = blds[k];
        html = $(template(building));
        html.data("building", building);
        html.unbind("click");
        html.click(function() {
          var bld;
          bld = $(this).data("building");
          return Hal.trigger("CONSTRUCT_BUILDINGS", bld);
        });
        _results.push($BuildingsList.append(html));
      }
      return _results;
    });
    Hal.on("CONSTRUCT_BUILDINGS", function(building) {
      amjad.city.trigger("TILELAYER_SELECTED", building);
      return amjad.city.showTileLayer();
    });
    return Hal.on("TAVERN_ADDED", function() {});
  });

}).call(this);
