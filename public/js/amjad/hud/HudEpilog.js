(function() {
  "use strict";
  define(function() {
    $(".close-button").click(function() {
      return $(this).parent().parent().fadeOut(100);
    });
    $(".draggable").draggable({
      handle: ".dialog-name"
    });
    $(".full-screen-button").click(function() {
      if (!Hal.supports("FULLSCREEN")) {
        return log.warn("fullscreen not supported on this device");
      } else {
        return Hal.trigger("REQUEST_FULLSCREEN");
      }
    });
    return $(document).on("keydown", function(ev) {
      if (ev.keyCode === 27) {
        return $(".draggable").fadeOut(100);
      }
    });
  });

}).call(this);
