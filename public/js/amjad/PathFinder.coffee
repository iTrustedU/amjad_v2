"use strict"

define ["../amjad/Tile", "MinHeap"], 

(Tile, MinHeap) ->

    compareFs = (a, b) ->
        return a.f < b.f

    class PathFinder
        constructor: (@map) ->
            @diagonal_cost = 1.4
            @straight_cost = 1.0

        tracePath: (dest) ->
            out = []
            while dest.parent?
                out.push(dest)
                dest = dest.parent

            return out

        find: (from, to) ->
            open = new MinHeap(null, compareFs)
            closed = {}
            open_map = {}

            if not from? or not to?
                console.log("no start or end node given")
                return

            open.push({
                row: from.row
                col: from.col
                id: from.id
                f: 0
                g: 0
                h: 0
                parent: null
            })

            open_map[from.id] = from
            while open.size() > 0
                cur = open.pop()
                open_map[cur.id] = null

                if cur.row == to.row and cur.col == to.col
                    return @tracePath(cur)
                closed[cur.id] = cur

                neighs = @map.getReachableNeighbours(@map.getTile(cur.row, cur.col))

                for t in neighs
                    if (t.row == cur.row) or (t.col == cur.col)
                        g = @straight_cost
                    else 
                        g = @diagonal_cost

                    if closed[t.id]?
                        continue

                    if not open_map[t.id]?
                        ###
                            ovo kao dobro balansira putanju
                        ###
                        #h = (Math.abs(to.row - t.row) + Math.abs((to.col - t.col)/2-(t.col%2))) * @straight_cost
                        
                        ###
                            ovo ok radi
                        ###
                        ##h = (Math.abs(to.row - t.row) + Math.abs((to.col - t.col)/2-(t.col%2))) * @diagonal_cost

                        ###
                            ovo definitivno uvek trazi najkracu al' je skupo dabogsacuvaj
                        ###

                        #xDiff = Math.abs(to.pos[0] + t.pos[0])
                        #yDiff = Math.abs(to.pos[1] + t.pos[1])
                        
                        if (t.col % 2)
                            cost = @straight_cost
                        else 
                            cost = @diagonal_cost

                        #h = Math.sqrt(yDiff*xDiff - xDiff*yDiff) * cost

                        #h = (Math.abs(t.row - to.row) + Math.abs(t.col - to.col)) * @straight_cost

                        ### eksperimentalno se pokazalo da ovo najbolje sljaka ###
                        h = (Math.abs(to.row - t.row) + Math.abs(t.col - to.col)) * cost
                        # h = (Math.abs(to.row - t.row) + Math.abs((to.col - t.col)/2)) * (@straight_cost-(t.col%2))
                        open.push({
                            h: h
                            row: t.row
                            id: t.id
                            col: t.col
                            g: cur.g + g
                            f: h + g
                            parent: cur
                        })
                        open_map[t.id] = t
                    else 
                        tt = open_map[t.id]
                        if cur.g + g < tt.g
                            tt.g = cur.g + g
                            tt.f = tt.h + tt.g
                            tt.parent = cur