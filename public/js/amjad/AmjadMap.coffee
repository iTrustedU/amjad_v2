"use strict"

define ["../amjad/Tile", "Scene", "Renderer", "../amjad/TileManager", "../amjad/PathFinder"], 

(Tile, Scene, Renderer, TileManager, PathFinder) ->

    class AmjadMap extends Scene
        constructor: (config) ->
            super(config.name, Hal.r.bounds, false)

            @tilew = config.tiledim.width
            @tileh = config.tiledim.height
            @nrows = +config.numrows
            @ncols = +config.numcols

            @tilew2prop     = 2 / @tilew
            @tileh2prop     = 2 / @tileh
            @tilew2         = @tilew / 2
            @tileh2         = @tileh / 2

            @tiles = []

            @mask = Hal.asm.getSprite("amjad/tilemask_128x64")

            @over = {
                 "green": Hal.asm.getSprite("amjad/grid_unit_over_green_128x64")
                 "red": Hal.asm.getSprite("amjad/grid_unit_over_red_128x64")
            }

            hittest        = Hal.dom.createCanvas(@tilew, @tileh).getContext("2d")
            hittest.drawImage(@mask.img, 0, 0)
            @mask_data     = hittest.getImageData(0, 0, @tilew, @tileh).data

            for i,j in @mask_data
                @mask_data[j] = i < 120

            @initMap()

            @search_range = [@bounds[2], @bounds[3]]

            @camera.disableDrag()

            @tile_under_mouse = undefined

            @display = {
                startr: 0
                endr: 0
                startc: 0
                endc: 0
            }

            @info = {
                row: "row: "
                col: "col: "
                tilename: "tile: "
                start_row: "starting row: "
                start_col: "staring col: "
                end_row: "end row: "
                end_col: "end_col: "
                tile_x: "tile_x: "
                tile_y: "tile_y: "
                num_rendering: "no. rendereded entities: "
            }

            @calcDrawingArea()
            @total_rendered = 0

            terrain_layer = Hal.dom.createCanvasLayer(1)
            Hal.dom.addCanvas(terrain_layer,0,0,true)
            decor_layer = Hal.dom.createCanvasLayer(2)
            Hal.dom.addCanvas(decor_layer,0,0,true)
            buildings_layer = Hal.dom.createCanvasLayer(3)
            Hal.dom.addCanvas(buildings_layer,0,0,true)
            poi_layer = Hal.dom.createCanvasLayer(4)
            Hal.dom.addCanvas(poi_layer,0,0,true)

            @layer_renderers = [
                new Renderer(@bounds, terrain_layer, @camera), 
                new Renderer(@bounds, decor_layer, @camera),
                new Renderer(@bounds, buildings_layer, @camera),
                new Renderer(@bounds, poi_layer, @camera)
            ]

            @layers = {
                terrain: 0
                decor: 1
                buildings: 2
                poi: 3
                character: 3
            }

            @working_layer = null
            @working_layer_sprite = null
            @can_place_layer = false
            @cur_area  = []
            @working_layer_adjpos = [0, 0]
            @showtile = null

            @clicked_layer = null

    AmjadMap::drawInfo = () ->
        if @tile_under_mouse?
            Hal.glass.ctx.fillText(@info.row + @tile_under_mouse.row, 0, 60)
            Hal.glass.ctx.fillText(@info.col + @tile_under_mouse.col, 0, 70)
            Hal.glass.ctx.fillText(@info.tile_x + @tile_under_mouse.pos[0], 0, 80)
            Hal.glass.ctx.fillText(@info.tile_y + @tile_under_mouse.pos[1], 0, 90)

        #Hal.glass.ctx.fillText(@info.tilename + @current_tile_meta.name, 0, 30)
        Hal.glass.ctx.fillText(@info.start_row + @display.startr, 0, 100)
        Hal.glass.ctx.fillText(@info.start_col + @display.startc, 0, 110)
        Hal.glass.ctx.fillText(@info.end_row + @display.endr, 0, 120)
        Hal.glass.ctx.fillText(@info.end_col + @display.endc, 0, 130)
        Hal.glass.ctx.fillText(@info.num_rendering + @total_rendered, 0, 140)

    AmjadMap::calcDrawingArea = () ->
        @display = {
            startc: Math.max(0, Math.round(((-@camera.pos[0] / @tilew * 2) / @camera.zoom) - 3))
            endr: Math.min(@nrows-1, Math.ceil((@bounds[3] / @tileh) / @camera.zoom) + 3)
            startr: Math.max(0, Math.round(((-@camera.pos[1] / @tileh) / @camera.zoom) - 3))
            endc: Math.min(@ncols-1, Math.floor((@bounds[2] / @tilew2) / @camera.zoom) + 3)
        }

    AmjadMap::addWorkingLayer = () ->
        @tile_under_mouse.addLayer(
            @working_layer.name,
            @working_layer_adjpos,
            false
        )

    AmjadMap::init = () ->
        @on "SHOWING_TILELAYER_STARTED", () =>
            @camera.disableZoom()

        @on "SHOWING_TILELAYER_FINISHED", () =>
            @camera.enableZoom()

        @on "TILELAYER_SELECTED", (layer) =>
            log.debug layer
            @working_layer = layer
            @working_layer_sprite = Hal.asm.getSprite(@working_layer.sprite)
            @calcCenterAdjPos(@working_layer_adjpos, @working_layer_sprite)

        Hal.on "RIGHT_CLICK", (pos) =>
            return if @paused
            @camera.lerpTo(pos)

        Hal.on "MOUSE_DBL_CLICK", (pos) =>
            @processMouseClick(pos, "DBL_CLICK")

        Hal.on "LEFT_CLICK", (pos) =>
            @processMouseClick(pos, "CLICK")

        @on "ENTER_FULLSCREEN", (sc) =>
            for r in @layer_renderers
                r.resize(Hal.r.canvas.width, Hal.r.canvas.height)
            # @search_range = [@bounds[2], @bounds[3]]

        @on "EXIT_FULLSCREEN", (sc) =>
            for r in @layer_renderers
                r.resize(Hal.r.canvas.width, Hal.r.canvas.height)
            # @search_range = [@bounds[2], @bounds[3]]

        Hal.on "ENTER_FRAME", () =>
            Hal.glass.ctx.clearRect(0, 0, @bounds[2], @bounds[3])
            @g.ctx.setTransform(1, 0, 0, 1, 0, 0)
            @g.ctx.clearRect(0, 0, @bounds[2], @bounds[3])
            @layer_renderers[0].ctx.setTransform(1, 0, 0, 1, 0, 0)
            @layer_renderers[0].ctx.clearRect(0, 0, @bounds[2], @bounds[3])
            @layer_renderers[1].ctx.setTransform(1, 0, 0, 1, 0, 0)
            @layer_renderers[1].ctx.clearRect(0, 0, @bounds[2], @bounds[3])
            @layer_renderers[2].ctx.setTransform(1, 0, 0, 1, 0, 0)
            @layer_renderers[2].ctx.clearRect(0, 0, @bounds[2], @bounds[3])
            @layer_renderers[3].ctx.setTransform(1, 0, 0, 1, 0, 0)
            @layer_renderers[3].ctx.clearRect(0, 0, @bounds[2], @bounds[3])

        Hal.on "MOUSE_MOVE", () =>
            @tile_under_mouse = @getTileAt(@mpos)

        @on "CAMERA_MOVED", () ->
            @calcDrawingArea()

        @on "ZOOM", () ->
            @calcDrawingArea()

    AmjadMap::processMouseClick = (pos, type) ->
            return if @paused

            if @clicked_layer?
                @clicked_layer.trigger "DESELECTED"
                @clicked_layer = null

            if @showtile and @can_place_layer
                @addWorkingLayer()
                @hideTileLayer()
                return

            t = performance.now()
            ents = @quadspace.searchInRange(
                [@mpos[0] - @search_range[0], @mpos[1] - @search_range[1], 2*@search_range[0], 2*@search_range[1]]
            )
            t1 = performance.now() - t

            log.info t1
            log.info ents.length

            for tile in ents
                if not tile.isTransparent(@mpos.slice())
                    continue
                log.debug tile
                if not @clicked_layer?
                    @clicked_layer = tile
                else
                    if (tile.parent_tile.col == @clicked_layer.parent_tile.col) and (tile.parent_tile.row == @clicked_layer.parent_tile.row)
                        log.debug tile.level
                        log.debug @clicked_layer.level
                        if tile.level > @clicked_layer.level
                            @clicked_layer = tile
                    else if (tile.parent_tile.row == @clicked_layer.parent_tile.row)
                        if (tile.h + tile.pos[1] > @clicked_layer.h + @clicked_layer.pos[1])
                            @clicked_layer = tile
                    else if (tile.parent_tile.col == @clicked_layer.parent_tile.col)
                        if (tile.h + tile.pos[1] > @clicked_layer.h + @clicked_layer.pos[1])
                            @clicked_layer = tile
                    else if (tile.parent_tile.col != @clicked_layer.parent_tile.col) and (tile.parent_tile.row != @clicked_layer.parent_tile.row)
                        if (tile.h + tile.pos[1] > @clicked_layer.h + @clicked_layer.pos[1])
                            @clicked_layer = tile

            if @clicked_layer?
                log.debug "clicked layer"
                log.debug @clicked_layer
                @trigger "LAYER_SELECTED", @clicked_layer
                @clicked_layer.trigger "SELECTED_#{type}"

    AmjadMap::drawSpan = (span, color, level) ->
        for s in span
            @layer_renderers[level].ctx.drawImage(@over[color].img, s.pos[0], s.pos[1])

    AmjadMap::canBePlacedOn = (span, layer) ->
        for k in span
            if k.layers[layer]?
                if k.layers[layer].is_partial or k.layers[layer].is_root
                    return false
        return true

    AmjadMap::findInDirectionOf = (tile, dirstr, len) ->
        if not tile?
            return []
        out = []
        out.push(tile)
        fromr = tile.row
        fromc = tile.col
        dir = tile.direction[dirstr]
        while len > 0
            t = @getTile(fromr, fromc, dir)
            if t?
                out.push(t)
                fromr = t.row
                fromc = t.col
                dir = t.direction[dirstr]
            else
                break
            len--
        return out

    AmjadMap::getSpanArea = (tile, size) ->
        #log.debug "span bits: #{size}"
        root    = ~~Math.sqrt(size.length) #~~(Math.log(size.length)/Math.LN2)
        #log.debug "root: #{root}"
        #size    = size.slice(1)
        sizelen = size.length - 1
        nwests  = @findInDirectionOf(tile, "northwest", root - 1)
        out     = []
        for w in nwests
            neasts = @findInDirectionOf(w, "northeast", root - 1)
            out = out.concat(neasts)
        out = out.filter (_, i) ->
            return +size[sizelen-((i%root)*root)-~~(i/root)]
        return out

    AmjadMap::getTileAt = (pos) ->
        coord = @toOrtho(pos)
        if (coord[0] < 0.0 || coord[1] < 0.0 || coord[1] >= @nrows || coord[0] >= @ncols)
            return null
        return @tiles[Math.floor(coord[0]) + Math.floor(coord[1]) * @ncols]

    AmjadMap::initMap = () ->
        @clicked_layer = null        
        log.debug "columns: #{@ncols}"
        log.debug "rows: #{@nrows}"
        log.debug "tilew2: #{@tilew2}"
        log.debug "tileh: #{@tileh}"
        @world_dim = [0, 0, (@ncols + 1) * @tilew2, (@nrows + 1) * @tileh]
        log.debug @world_dim
        @resetQuadSpace(@world_dim)
        for i in [0..@nrows-1]
            for j in [0..@ncols-1]
                x = (j / 2) * @tilew + @camera.pos[0]
                y = (i + ((j % 2) / 2)) * @tileh + @camera.pos[1]

                t = new Tile({
                    "x": x
                    "y": y
                    "row": i
                    "col": j
                    "sprite": "amjad/grid_unit_128x64"
                })

                @tiles.push(@addEntity(t))
        @pathfinder = new PathFinder(@)
        @tmngr = TileManager
        @camera.setViewFrustum([@tilew2, @tileh2, @ncols * @tilew2, @nrows * @tileh])
        #@camera.pos[0] = -256
        #@camera.pos[1] = -256
        @calcDrawingArea()

    AmjadMap::removeTile = (row, col, dir=[0,0]) ->
        ind = (col+dir[1]) + (row+dir[0]) * @ncols
        @removeEntity(@tiles[ind])
        @tiles[ind] = null

    AmjadMap::toOrtho = (pos) ->
        coldiv  = (pos[0] * @tilew2prop)
        rowdiv  = (pos[1] * @tileh2prop)
        off_x   = ~~(pos[0] - ~~(coldiv * 0.5) * @tilew)
        off_y   = ~~(pos[1] - ~~(rowdiv * 0.5) * @tileh)
        transp  = @mask_data[(off_x + @tilew * off_y) * 4 + 3]
        return [
            coldiv - (transp ^ !(coldiv & 1)),
            (rowdiv - (transp ^ !(rowdiv & 1))) / 2
        ]

    AmjadMap::getTile = (row, col, dir=[0,0]) ->
        return @tiles[(col+dir[1]) + (row+dir[0]) * @ncols]
    
    AmjadMap::update = (delta) ->
        return if @paused
        super(delta)
        @layer_renderers[0].ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])
        @layer_renderers[1].ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])
        @layer_renderers[2].ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])
        @layer_renderers[3].ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])

        @total_rendered = 0
        for i in [@display.startr..@display.startr + @display.endr]
            for j in [@display.startc..@display.endc + @display.startc]
                tile = @tiles[j + i*@ncols]
                if not tile? 
                    continue
                if not @camera.isVisible(tile) and not tile.layers[3]?
                    continue
                tile.update()
                @total_rendered++

        if @tile_under_mouse
            @layer_renderers[1].ctx.drawImage(@over.green.img, @tile_under_mouse.pos[0], @tile_under_mouse.pos[1])
            if @debug
                @drawInfo()

    AmjadMap::showTileLayer = () ->
        @calcCenterAdjPos(@working_layer_adjpos, @working_layer_sprite)
        skeep = 0
        log.debug "showtile: " + @showtile?
        if not @showtile?
            @trigger "SHOWING_TILELAYER_STARTED", @working_layer
            @showtile = 
                Hal.on "ENTER_FRAME", (delta) =>
                    if @tile_under_mouse and @working_layer
                        @layer_renderers[3].ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])

                        if skeep % 2
                            @cur_area = @getSpanArea(@tile_under_mouse, @working_layer.size)
                        if @canBePlacedOn(@cur_area, @layers[@working_layer.level])
                            @drawSpan(@cur_area, "green", 3)
                            @can_place_layer = true
                        else
                            @drawSpan(@cur_area, "red", 3)
                            @can_place_layer = false

                        @layer_renderers[3].ctx.drawImage(
                            @working_layer_sprite.img,
                            (@tile_under_mouse.pos[0] - @working_layer_adjpos[0]),
                            (@tile_under_mouse.pos[1] - @working_layer_adjpos[1])
                        )                         
                        skeep = not skeep % 2
                    else
                        @hideTileLayer()

    AmjadMap::hideTileLayer = () ->
        Hal.removeTrigger "ENTER_FRAME", @showtile
        @showtile = null
        @trigger "SHOWING_TILELAYER_FINISHED", @working_layer

    AmjadMap::calcCenterAdjPos = (layerpos, layersprite) ->
        layerpos[1] = (layersprite.h - @tileh)
        layerpos[0] = (layersprite.w / 2) - @tilew2

    AmjadMap::drawSpanBorders = (borders, color, layer) ->
        @drawSpan(borders, color, layer)

    AmjadMap::getSpanBorders = (tile, size) ->
        root    = ~~Math.sqrt(size.length)
        tile = @findInDirectionOf(tile, "south", 1)[1]
        nwests  = @findInDirectionOf(tile, "northwest", root + 1)
        neasts = @findInDirectionOf(tile, "northeast", root + 1)
        lastw = nwests[nwests.length-1]
        laste = neasts[neasts.length-1]
        lnwests = @findInDirectionOf(lastw, "northeast", root + 1)
        lneasts = @findInDirectionOf(laste, "northwest", root + 1)
        out = nwests.concat(neasts).concat(lnwests).concat(lneasts)
        return out

    AmjadMap::load = (data) ->
        @trigger "MAP_LOADING_STARTED", data.map.length
        @pause()
        @prev_camera_data = {
            pos: @camera.pos.slice()
            zoom: @camera.zoom
        }
        @camera.pos  = [0, 0]
        @camera.zoom = 1
        size         = data.name.match(/(\d+x\d+)/g)[0].split("x")
        nrows        = +size[0]
        ncols        = +size[1]

        log.debug "nrows: #{nrows}"
        log.debug "ncols: #{ncols}"

        del = (nrows != @nrows and ncols != @ncols)

        for g in @tiles
            for l in g.layers
                if l?
                    g.removeLayer(l.level)
            if del
                @removeTile(g.row, g.col)

        @tiles = []
        @nrows = nrows
        @ncols = ncols
        @initMap()

        setTimeout(
            () => 
                @loadTile(0, data)
        , 100)

    AmjadMap::loadRandomMap = (i, data) ->
        for i in [0..@ncols-1]
            for j in [0..@nrows-1]
                t = @getTile(i, j)
                k = 5
                while k > 0 and not t.isFull()
                    @addRandomLayer(t)
                        #log.debug k
                    --k

    AmjadMap::addRandomLayer = (t) ->
        tskeys = Object.keys(@tmngr.Tiles)
        #log.debug tskeys
        randts = ~~(Math.random() * tskeys.length)
        #log.debug randts
        index = tskeys[randts]
        tiles = amj.tmngr.Tiles[index]
        #log.debug tiles
        tkeys = Object.keys(tiles)
        randt = ~~(Math.random() * tkeys.length)
        index = tkeys[randt]
        tileLayer = tiles[index]
        #log.debug tileLayer
        #log.debug tileLayer
        hw = [0,0]
        spr = Hal.asm.getSprite(tileLayer.sprite)
        #log.debug spr
        @calcCenterAdjPos(hw, spr)
        #log.debug hw
        area = @getSpanArea(t, tileLayer.size)
        if @canBePlacedOn(area, @layers[tileLayer.level])
            return t.addLayer(tileLayer.name, hw, true)

    AmjadMap::loadTile = (i, data) ->
        if i >= data.map.length
            @camera.pos = @prev_camera_data.pos
            @camera.zoom = @prev_camera_data.zoom
            @trigger "MAP_LOADED", data.name
            @resume()
        else 
            g = data.map[i]
            #log.debug "dodajem tile"
            #log.debug g
            t = @tiles[g.c + g.r * @ncols]

            if (g.layers != null)
                t.load(g.l)

            #setTimeout(
            #    () =>
            #onframe = 
            #Hal.on "ENTER_FRAME" , () =>
            #@trigger "TILE_LOADED"
            if (i % @nrows) is 0
                #log.debug "tick: #{i}"
                setTimeout(
                    () => 
                        @loadTile(i + 1, data)
                , 10)
            else
                @loadTile(i+1, data)
            #    Hal.removeTrigger "ENTER_FRAME", onframe
            #, 10)

    AmjadMap::getNeighbours = (tile) ->
        out = []
        for dir in Object.keys(tile.direction)
            n = @getTile(tile.row, tile.col, tile.direction[dir])
            if n?
                out.push(n)
        return out

    AmjadMap::getReachableNeighbours = (tile) ->
        out = []
        for dir in Object.keys(tile.direction)
            n = @getTile(tile.row, tile.col, tile.direction[dir])
            if n? and n.layers[0] and n.num_layers == 1 and not n.is_partial and not n.layers[3]
                out.push(n)
        return out

    return AmjadMap
