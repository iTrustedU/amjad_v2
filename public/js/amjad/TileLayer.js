(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["entities/RectBoundedEntity", "Renderer"], function(RectBoundedEntity, Renderer) {
    var TileLayer;
    TileLayer = (function(_super) {
      __extends(TileLayer, _super);

      function TileLayer(level, meta, basepos, adjpos, renderer, parent) {
        this.level = level;
        this.meta = meta;
        TileLayer.__super__.constructor.call(this, basepos, meta.sprite);
        this.name = meta.name;
        this.scene = parent.scene;
        this.pos[0] -= this.scene.camera.pos[0] / this.scene.camera.zoom;
        this.pos[1] -= this.scene.camera.pos[1] / this.scene.camera.zoom;
        this.scene.quadspace.insert(this);
        this.parent_tile = parent;
        this.g = renderer;
        this.h = adjpos[1] || (this.sprite.h - this.scene.tileh);
        this.w = adjpos[0] || (this.sprite.w - this.scene.tilew);
        this.is_partial = false;
        this.is_root = true;
        this.selected = false;
        this.tinted_red = Hal.asm.tint(this.sprite, "red");
        this.init();
        if (this.meta.listeners != null) {
          if (this.meta.listeners.SELECTED_CLICK != null) {
            this.on("SELECTED_CLICK", Function(this.meta.listeners.SELECTED_CLICK));
          }
          if (this.meta.listeners.SELECTED_DBL_CLICK != null) {
            this.on("SELECTED_DBL_CLICK", Function(this.meta.listeners.SELECTED_DBL_CLICK));
          }
          if (this.meta.listeners.DESELECTED) {
            this.on("DESELECTED", Function(this.meta.listeners.DESELECTED));
          }
        }
        this.visible = true;
      }

      TileLayer.prototype.drawSpan = function() {
        var borders, span;
        span = this.scene.getSpanArea(this.parent_tile, this.meta.size);
        this.scene.drawSpan(span, "green", 1);
        borders = this.scene.getSpanBorders(this.parent_tile, this.meta.size);
        return this.scene.drawSpanBorders(borders, "red", 1);
      };

      TileLayer.prototype.hide = function() {
        return this.visible = false;
      };

      TileLayer.prototype.show = function() {
        return this.visible = true;
      };

      TileLayer.prototype.init = function() {
        this.on("SELECTED_CLICK", function() {
          this.selected = true;
          return log.debug("tile selected " + (this.getInfo()));
        });
        return this.on("DESELECTED", function() {
          this.selected = false;
          return log.debug("tile deselected " + (this.getInfo()));
        });
      };

      TileLayer.prototype.getInfo = function() {
        return "#" + this.parent_tile.id + "-" + this.parent_tile.row + ":" + this.parent_tile.col + "_" + this.level;
      };

      TileLayer.prototype.update = function(delta) {
        if (!this.visible) {
          return;
        }
        if (this.selected) {
          this.g.ctx.drawImage(this.tinted_red, this.pos[0] - this.w, this.pos[1] - this.h);
        } else {
          this.g.ctx.drawImage(this.sprite.img, 0, 0, this.sprite.w - 2, this.sprite.h - 2, (this.pos[0] - this.w) - 1, (this.pos[1] - this.h) - 1, this.sprite.w + 2, this.sprite.h + 2);
        }
        if (this.scene.show_span && this.level !== 0) {
          return this.drawSpan();
        }
      };

      TileLayer.prototype.isTransparent = function(pos) {
        pos[0] = (pos[0] - this.pos[0]) + this.w;
        pos[1] = (pos[1] - this.pos[1]) + this.h;
        if (Hal.m.isPointInRect(pos, this.bounds)) {
          if (!Hal.im.isTransparent(this.sprite.img, pos[0], pos[1])) {
            return true;
          }
        }
        return false;
      };

      TileLayer.prototype.equals = function(b) {
        if (b == null) {
          return false;
        }
        return (this.level === b.level) && this.parent_tile.equals(b.parent_tile);
      };

      TileLayer.prototype.serialize = function() {
        var l;
        l = {
          m: this.meta.name,
          h: [this.w, this.h]
        };
        return l;
      };

      return TileLayer;

    })(RectBoundedEntity);
    return TileLayer;
  });

}).call(this);
