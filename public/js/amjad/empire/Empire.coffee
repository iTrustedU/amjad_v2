"use strict"

define [],

() ->

    class Empire
        constructor: () ->
            @id = 15
            @fraction = "bedouin"
            @Army = 
                Swordsman: {}
                Haggon: {}

            @Resources = 
                Money: 20000
                Wood: 100
                Iron: 100
                Gold: 100

            @Heroes = 
                Commanders: {}
                Trainers: {}
                Agents: {}

            @Buildings = 
                WarTent: null
                TradeStation: null
                Tavern: null
                TrainingGrounds: null

            @AvailableBuildings = {}
            @AvailableHeroes = 
                Commanders: {}
                Trainers: {}
                Agents: {}

            @AvailableGoods =
                "Dates":
                    sprite: "trade-goods/dates"
                    price: 50
                    weight: 10
                    description: "Dates"
                
                "Silk":
                    sprite: "trade-goods/silk"
                    price: 150
                    weight: 5
                    description: "Silk"

            @AvailableTroops =
                "Swordsman":
                    name: "Brad"
                    sprite: "troop-icons/swordsman"
                    bigsprite: "troops/bedouin_swordsman"
                    pillage: 1
                    raze: 5
                    steal: 4
                    health: 20
                    defense: 10
                    attack: 17
                    upkeep: 13
                    gold: 1
                    wood: 1
                    iron: 1
                    price: 100   

                "HeavySwordsman":
                    name: "Matt"
                    sprite: "troop-icons/heavyswordsman"
                    bigsprite: "troops/heavyswordsman"
                    pillage: 3
                    raze: 2
                    steal: 7
                    health: 40
                    defense: 23
                    attack: 7
                    upkeep: 8
                    gold: 2
                    wood: 1
                    iron: 1
                    price: 200

                "Skirmisher":
                    name: "John"
                    sprite: "troop-icons/skirmisher"
                    bigsprite: "troops/haggan"
                    pillage: 8
                    raze: 9
                    steal: 4
                    health: 30
                    defense: 10
                    attack: 27
                    upkeep: 8
                    gold: 3
                    wood: 1
                    iron: 1
                    price: 300
                    
            @AvailableTrainers =
                "Jahar":
                    name: "Jahar"
                    brawn: 2
                    reflexes: 5
                    endurance: 4
                    experience: 7
                    level: 1

                "Jabar":
                    name: "Jabar"
                    brawn: 2
                    reflexes: 5
                    endurance: 4
                    experience: 7
                    level: 1

            @AvailableEquipment =
                "Saber":
                    sprite: "equipment/saber"
                    readiness: 3
                    type: "r-hand"
                
                "Armor":
                    sprite: "equipment/armor"
                    leadership: 3
                    type: "torso"
                
                "Shield":
                    sprite: "equipment/shield"
                    readiness: 2
                    leadership: 1
                    type: "l-hand"

            @money_div = $("#empire-money")
            @City = null
            @init()

        takeMoney: (amount) ->
            log.debug "taking money: #{amount}"
            @Resources.Money -= amount
            @money_div.text(@getMoney())

        addMoney: (amount) ->
            log.debug "adding money: #{amount}"
            @Resources.Money += amount
            @money_div.text(@getMoney())

        takeWood: (amount) ->
            log.debug "taking money: #{amount}"
            @Resources.Money -= amount
            @money_div.text(@getMoney())

        addWood: (amount) ->
            log.debug "adding money: #{amount}"
            @Resources.Money += amount
            @money_div.text(@getMoney())

        takeIron: (amount) ->
            log.debug "taking money: #{amount}"
            @Resources.Money -= amount
            @money_div.text(@getMoney())

        addIron: (amount) ->
            log.debug "adding money: #{amount}"
            @Resources.Money += amount
            @money_div.text(@getMoney())

        takeGold: (amount) ->
            log.debug "taking money: #{amount}"
            @Resources.Money -= amount
            @money_div.text(@getMoney())

        addGold: (amount) ->
            log.debug "adding money: #{amount}"
            @Resources.Money += amount
            @money_div.text(@getMoney())

        setCity: (city) ->
            @City = city

        init: () ->
            @money_div.text(@getMoney())
            return "hah"

        getMoney: () ->
            return @Resources.Money

        employHero: (hero) ->
            if @Resources.Money < hero.price
                log.debug "not enough money"
                return

            log.debug "employ hero-----------------"
            log.debug hero
            if hero.type? and hero.type is "trainer"
                if not @Buildings.TrainingGrounds?
                    log.debug "no training grounds in city"
                    return null
                else
                    @takeMoney(+hero.price)
                    @Buildings.TrainingGrounds.putTrainer(hero)

            else if hero.attr.type is "commander"
                if not @Buildings.WarTent?
                    log.debug "no wartent in your city"
                    return null
                else 
                    @takeMoney(+hero.attr.price)
                    return @Buildings.WarTent.putCommander(hero)
            else if hero.attr.type is "caravan"
                if not @Buildings.WarTent?
                    log.debug "no wartent in your city"
                    return null
                else 
                    @takeMoney(+hero.attr.price)
                    return @Buildings.WarTent.putCaravan(hero)
    return Empire