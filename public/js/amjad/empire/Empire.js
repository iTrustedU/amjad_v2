(function() {
  "use strict";
  define([], function() {
    var Empire;
    Empire = (function() {
      function Empire() {
        this.id = 15;
        this.fraction = "bedouin";
        this.Army = {
          Swordsman: {},
          Haggon: {}
        };
        this.Resources = {
          Money: 20000,
          Wood: 100,
          Iron: 100,
          Gold: 100
        };
        this.Heroes = {
          Commanders: {},
          Trainers: {},
          Agents: {}
        };
        this.Buildings = {
          WarTent: null,
          TradeStation: null,
          Tavern: null,
          TrainingGrounds: null
        };
        this.AvailableBuildings = {};
        this.AvailableHeroes = {
          Commanders: {},
          Trainers: {},
          Agents: {}
        };
        this.AvailableGoods = {
          "Dates": {
            sprite: "trade-goods/dates",
            price: 50,
            weight: 10,
            description: "Dates"
          },
          "Silk": {
            sprite: "trade-goods/silk",
            price: 150,
            weight: 5,
            description: "Silk"
          }
        };
        this.AvailableTroops = {
          "Swordsman": {
            name: "Brad",
            sprite: "troop-icons/swordsman",
            bigsprite: "troops/bedouin_swordsman",
            pillage: 1,
            raze: 5,
            steal: 4,
            health: 20,
            defense: 10,
            attack: 17,
            upkeep: 13,
            gold: 1,
            wood: 1,
            iron: 1,
            price: 100
          },
          "HeavySwordsman": {
            name: "Matt",
            sprite: "troop-icons/heavyswordsman",
            bigsprite: "troops/heavyswordsman",
            pillage: 3,
            raze: 2,
            steal: 7,
            health: 40,
            defense: 23,
            attack: 7,
            upkeep: 8,
            gold: 2,
            wood: 1,
            iron: 1,
            price: 200
          },
          "Skirmisher": {
            name: "John",
            sprite: "troop-icons/skirmisher",
            bigsprite: "troops/haggan",
            pillage: 8,
            raze: 9,
            steal: 4,
            health: 30,
            defense: 10,
            attack: 27,
            upkeep: 8,
            gold: 3,
            wood: 1,
            iron: 1,
            price: 300
          }
        };
        this.AvailableTrainers = {
          "Jahar": {
            name: "Jahar",
            brawn: 2,
            reflexes: 5,
            endurance: 4,
            experience: 7,
            level: 1
          },
          "Jabar": {
            name: "Jabar",
            brawn: 2,
            reflexes: 5,
            endurance: 4,
            experience: 7,
            level: 1
          }
        };
        this.AvailableEquipment = {
          "Saber": {
            sprite: "equipment/saber",
            readiness: 3,
            type: "r-hand"
          },
          "Armor": {
            sprite: "equipment/armor",
            leadership: 3,
            type: "torso"
          },
          "Shield": {
            sprite: "equipment/shield",
            readiness: 2,
            leadership: 1,
            type: "l-hand"
          }
        };
        this.money_div = $("#empire-money");
        this.City = null;
        this.init();
      }

      Empire.prototype.takeMoney = function(amount) {
        log.debug("taking money: " + amount);
        this.Resources.Money -= amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.addMoney = function(amount) {
        log.debug("adding money: " + amount);
        this.Resources.Money += amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.takeWood = function(amount) {
        log.debug("taking money: " + amount);
        this.Resources.Money -= amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.addWood = function(amount) {
        log.debug("adding money: " + amount);
        this.Resources.Money += amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.takeIron = function(amount) {
        log.debug("taking money: " + amount);
        this.Resources.Money -= amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.addIron = function(amount) {
        log.debug("adding money: " + amount);
        this.Resources.Money += amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.takeGold = function(amount) {
        log.debug("taking money: " + amount);
        this.Resources.Money -= amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.addGold = function(amount) {
        log.debug("adding money: " + amount);
        this.Resources.Money += amount;
        return this.money_div.text(this.getMoney());
      };

      Empire.prototype.setCity = function(city) {
        return this.City = city;
      };

      Empire.prototype.init = function() {
        this.money_div.text(this.getMoney());
        return "hah";
      };

      Empire.prototype.getMoney = function() {
        return this.Resources.Money;
      };

      Empire.prototype.employHero = function(hero) {
        if (this.Resources.Money < hero.price) {
          log.debug("not enough money");
          return;
        }
        log.debug("employ hero-----------------");
        log.debug(hero);
        if ((hero.type != null) && hero.type === "trainer") {
          if (this.Buildings.TrainingGrounds == null) {
            log.debug("no training grounds in city");
            return null;
          } else {
            this.takeMoney(+hero.price);
            return this.Buildings.TrainingGrounds.putTrainer(hero);
          }
        } else if (hero.attr.type === "commander") {
          if (this.Buildings.WarTent == null) {
            log.debug("no wartent in your city");
            return null;
          } else {
            this.takeMoney(+hero.attr.price);
            return this.Buildings.WarTent.putCommander(hero);
          }
        } else if (hero.attr.type === "caravan") {
          if (this.Buildings.WarTent == null) {
            log.debug("no wartent in your city");
            return null;
          } else {
            this.takeMoney(+hero.attr.price);
            return this.Buildings.WarTent.putCaravan(hero);
          }
        }
      };

      return Empire;

    })();
    return Empire;
  });

}).call(this);
