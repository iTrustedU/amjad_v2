"use strict"

define ["entities/SpriteEntity", "../amjad/TileLayer"], 

(SpriteEntity, TileLayer) ->

    class Tile extends SpriteEntity
        constructor: (config) ->
            super([config.x, config.y], config.sprite)
            @row = config.row
            @col = config.col
            isOdd = @col % 2
            @direction = 
                north: [-1, 0],
                south: [1, 0],
                east: [0, 2],
                west: [0, -2],
                southeast: [isOdd, 1],
                northeast: [-1+isOdd,1],
                southwest: [isOdd, -1],
                northwest: [-1+isOdd, -1]

            @is_partial = false
            @layers = [null, null, null, null]
            @num_layers = 0

        update: (delta) ->
            if @num_layers == 0
                super(delta)
            for layer in @layers
                if layer? and not layer.is_partial
                    layer.update()

        isPartial: () ->
            for layer in @layers
                if layer.is_partial
                    return true
        isFull: () ->
            return @num_layers > 3

        removeLayer: (level, keeplisteners) ->
            if @layers[level]?
                #log.debug @layers[level]
                if not @layers[level].is_partial
                    if not keeplisteners
                        log.debug "removing triggers"
                        @layers[level].removeAllTriggers()
                    if @layers[level].quadspace?
                        @layers[level].quadspace.remove(@layers[level])
                else
                    log.debug "omfg, it's partial, no way"

                t = @layers[level]
                # if t.span_area?
                #     for i in [1..t.span_area.length-1]
                #         if t.span_area[i]?
                #             t.span_area[i].is_partial = false
                #             if t.span_area[i].layers[level]?
                #                 t.span_area[i].layers[level].parent_tile = null 
                #                 t.span_area[i].layers[level] = null
                span = @scene.getSpanArea(@, t.meta.size)
                if span.length > 1
                    for i in [1..span.length-1]
                        span[i].layers[level] = null
                @layers[level] = null
                @num_layers--
                #ispart = false
                #for v in @layers
                #    if v? and v.is_partial
                #        ispart = true
                #        break
                #@is_partial = ispart

        equals: (other) ->
            return other.row == @row and other.col == @col

        addLayer: (metakey, adjpos, overwrite) ->
            #load meta information from tilemanager
            meta = @scene.tmngr.getTile(metakey)
            if not meta?
                throw new Error("Couldn't load tile #{metakey}")
                return null

            #get the level index
            level = @scene.layers[meta.level]
            #get the renderer for that level
            renderer = @scene.layer_renderers[level]

            #if tile is occupied and you don't want to overwrite
            if not overwrite and @layers[level]?
                return

            #@removeLayer(level)
            #t = null
            lpos = [@pos[0] + @scene.camera.pos[0] / @scene.camera.zoom, @pos[1] + @scene.camera.pos[1] / @scene.camera.zoom]
            t = @scene.tmngr.newTileFromMeta(level, meta, lpos, adjpos, renderer, @)
            if not t?
                log.debug "couldn't create tile from meta #{meta.name}"
                return

            span = @scene.getSpanArea(@, meta.size)
            if span.length > 1
                for i in [1..span.length-1]
                    span[i].layers[level] = {is_partial: true, is_root: false, parent_tile: @}
                    #@span_area[i].is_partial = true

            @layers[level] = t
            @num_layers++

            return t
            
        save: () ->
            tile = {
                r: @row,
                c: @col,
                l: []
            }
            if @num_layers == 0
                delete tile.l # tile.l = null
            else 
                for layer in @layers
                    if layer? and not layer.is_partial
                        tile.l.push(layer.serialize())
            return tile

        load: (layers) ->
            for layer in layers
                continue if not layer?
                @addLayer(layer.m, layer.h, true)

        ##deprecated###
        # load: (layers) ->
        #     for layer in layers
        #         continue if not layer?
        #         log.debug layer.meta.sprite
        #         @addLayer(layer.layer, layer.meta, [layer.w, layer.h], @scene.layer_renderers[layer.layer], true)

    return Tile
