"use strict"

define ["entities/RectBoundedEntity", "Renderer"],

(RectBoundedEntity, Renderer) ->

    class TileLayer extends RectBoundedEntity
        constructor: (@level, @meta, basepos, adjpos, renderer, parent) ->
            super(basepos, meta.sprite)

            @name         = meta.name
            @scene        = parent.scene

            @pos[0] -= @scene.camera.pos[0] / @scene.camera.zoom
            @pos[1] -= @scene.camera.pos[1] / @scene.camera.zoom

            @scene.quadspace.insert(@)
            
            #log.debug @scene.quadspace

            #@span_area   = null
            @parent_tile = parent

            #@span_area    = @scene.getSpanArea(@parent_tile, meta.size)

            @g = renderer

            #log.debug adjpos
            @h = adjpos[1] || (@sprite.h - @scene.tileh)
            @w = adjpos[0] || (@sprite.w - @scene.tilew)
        
            # if @span_area.length > 1
            #     for i in [1..@span_area.length-1]
            #         @span_area[i].layers[@level] = {is_partial: true, is_root: false, parent_tile: @}
            #         @span_area[i].is_partial = true
            
            @is_partial = false
            @is_root = true

            @selected = false

            @tinted_red = Hal.asm.tint(@sprite, "red")

            @init()


            if @meta.listeners?
                if @meta.listeners.SELECTED_CLICK?
                    @on "SELECTED_CLICK", Function(@meta.listeners.SELECTED_CLICK)

                if @meta.listeners.SELECTED_DBL_CLICK?
                    @on "SELECTED_DBL_CLICK", Function(@meta.listeners.SELECTED_DBL_CLICK)

                if @meta.listeners.DESELECTED
                    @on "DESELECTED", Function(@meta.listeners.DESELECTED)

            @visible = true

        drawSpan: () ->
            span = @scene.getSpanArea(@parent_tile, @meta.size)
            @scene.drawSpan(span, "green", 1)
            borders = @scene.getSpanBorders(@parent_tile, @meta.size)
            @scene.drawSpanBorders(borders, "red", 1)

        hide: () ->
            @visible = false
        show: () ->
            @visible = true

        init: () ->
            @on "SELECTED_CLICK", () ->
                @selected = true
                log.debug "tile selected #{@getInfo()}"
                #Hal.trigger "LOAD_TILE_INMAP", {meta: @meta, spr: @sprite, tile: @}

            @on "DESELECTED", () ->
                @selected = false
                log.debug "tile deselected #{@getInfo()}"
                
            # @on "UPDATE_META", (meta) ->
            #     @parseMeta(meta)

        getInfo: () ->
            return "#"+@parent_tile.id+"-"+@parent_tile.row+":"+@parent_tile.col+"_"+@level

        update: (delta) ->
            return if not @visible
            if @selected
                @g.ctx.drawImage(@tinted_red, @pos[0] - @w, @pos[1] - @h)
            else
                #sprite.img, sprite.x, sprite.y, sprite.w-2, sprite.h-2, ~~(x+@bounds[0]-dx || 0), ~~(y+@bounds[1]-dy+@camera[1] || 0), sprite.w+dx, sprite.h+dy
                @g.ctx.drawImage(@sprite.img, 0,0, @sprite.w-2, @sprite.h-2, (@pos[0] - @w) - 1, (@pos[1] - @h) - 1, @sprite.w+2, @sprite.h+2)

            if @scene.show_span and @level isnt 0
                @drawSpan()

        isTransparent: (pos) ->
            pos[0] = (pos[0] - @pos[0]) + @w
            pos[1] = (pos[1] - @pos[1]) + @h
            if Hal.m.isPointInRect(pos, @bounds)
                if not Hal.im.isTransparent(@sprite.img, pos[0], pos[1])
                    return true
            return false

        equals: (b) ->
            return false if not b?
            return (@level == b.level) and @parent_tile.equals(b.parent_tile)

        serialize: () ->
            l = {
                m: @meta.name
                h: [@w, @h]
            }
            return l

        #parseMeta: () ->
                #if @meta.listeners.DESELECTED?
                #    @on "SELECTED", Function(@meta.listeners.SELECTED)

    return TileLayer 