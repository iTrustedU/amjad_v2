(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../amjad/AmjadMap"], function(AmjadMap) {
    var AmjadMapEditor;
    AmjadMapEditor = (function(_super) {
      __extends(AmjadMapEditor, _super);

      function AmjadMapEditor(settings) {
        AmjadMapEditor.__super__.constructor.call(this, settings);
        this.drag_started = false;
        this.delete_mode = false;
        this.delete_buffer = [];
        this.key_pressed = {
          "shift": false,
          "d": false
        };
        this.show_span = false;
        this.pattern = {};
        this.is_pivot = false;
        this.pattern_pivot = null;
        this.working_pattern = null;
        this.working_pivot = null;
        this.arab_map = new Image();
        this.arab_map.src = "assets/sprites/amjad/arab_map.svg";
      }

      AmjadMapEditor.prototype.setPatternPivot = function(tile) {
        if (tile == null) {
          return;
        }
        this.pattern_pivot = tile;
        this.pattern["pivot"] = tile;
        return this.drawCurrentPattern();
      };

      AmjadMapEditor.prototype.addToPattern = function(tile) {
        var even, odd, off_c, off_r;
        if (tile == null) {
          return;
        }
        even = !(this.tile_under_mouse.col % 2);
        odd = 0;
        if (even && (this.tile_under_mouse.col % 2)) {
          odd = 1;
        }
        off_r = tile.parent_tile.row - this.pattern_pivot.parent_tile.row + odd;
        off_c = tile.parent_tile.col - this.pattern_pivot.parent_tile.col;
        return this.pattern["" + off_r + "_" + off_c] = tile;
      };

      AmjadMapEditor.prototype.drawCurrentPattern = function() {
        var _this = this;
        if (this.draw_curr_pattern == null) {
          return this.draw_curr_pattern = Hal.on("ENTER_POST_FRAME", function(delta) {
            var buf, k, v, _ref, _results;
            if (!_this.key_pressed['f']) {
              Hal.removeTrigger("ENTER_POST_FRAME", _this.draw_curr_pattern);
              log.debug("Prikazi dijalog za cuvanje patterna");
              Hal.trigger("NEW_PATTERN_CREATED", _this.pattern);
              _this.draw_curr_pattern = null;
              _this.pattern = {};
              _this.pattern_pivot = null;
              return;
            }
            _ref = _this.pattern;
            _results = [];
            for (k in _ref) {
              v = _ref[k];
              buf = Hal.im.tintImage(v.sprite.img, "cyan", "0.3");
              _results.push(_this.layer_renderers[3].ctx.drawImage(buf, v.pos[0] - v.w, v.pos[1] - v.h));
            }
            return _results;
          });
        }
      };

      AmjadMapEditor.prototype.drawPattern = function() {
        var _this = this;
        if (this.draw_pattern == null) {
          return this.draw_pattern = Hal.on("ENTER_POST_FRAME", function(delta) {
            var even, img, odd, p, pmeta, t, _i, _len, _ref;
            if (_this.tile_under_mouse && _this.working_pattern && _this.key_pressed["shift"]) {
              pmeta = _this.tmngr.getTile(_this.working_pivot.m);
              img = Hal.asm.getSprite(pmeta.sprite).img;
              _this.layer_renderers[_this.layers[pmeta.level]].ctx.drawImage(img, _this.tile_under_mouse.pos[0], _this.tile_under_mouse.pos[1]);
              _ref = _this.working_pattern;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                p = _ref[_i];
                even = !((p.off[1] + _this.tile_under_mouse.col) % 2);
                odd = 0;
                if (even && (_this.tile_under_mouse.col % 2)) {
                  odd = 1;
                }
                t = _this.getTile((p.off[0] + _this.tile_under_mouse.row) + odd, p.off[1] + _this.tile_under_mouse.col);
                if (t == null) {
                  return;
                }
                pmeta = _this.tmngr.getTile(p.m);
                img = Hal.asm.getSprite(pmeta.sprite).img;
                _this.layer_renderers[_this.layers[pmeta.level]].ctx.drawImage(img, t.pos[0] + t.hw[0], t.pos[1] + t.hw[1]);
              }
            } else {
              Hal.removeTrigger("ENTER_POST_FRAME", _this.draw_pattern);
              return _this.draw_pattern = null;
            }
          });
        }
      };

      AmjadMapEditor.prototype.processMouseClick = function(pos, type) {
        AmjadMapEditor.__super__.processMouseClick.call(this, pos, type);
        if (this.key_pressed['f']) {
          if (!this.pattern_pivot) {
            return this.setPatternPivot(this.clicked_layer);
          } else {
            return this.addToPattern(this.clicked_layer);
          }
        } else if (this.working_pattern && this.key_pressed["shift"]) {
          log.debug("place pattern");
          return this.placeWorkingPattern(this.tile_under_mouse);
        }
      };

      AmjadMapEditor.prototype.placeWorkingPattern = function(tile) {
        var even, odd, p, t, _i, _len, _ref, _results;
        tile.addLayer(this.working_pivot.m, this.working_pivot.h, true);
        _ref = this.working_pattern;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          p = _ref[_i];
          even = !((p.off[1] + tile.col) % 2);
          odd = 0;
          if (even && (tile.col % 2)) {
            odd = 1;
          }
          t = this.getTile(p.off[0] + tile.row + odd, p.off[1] + tile.col);
          _results.push(t.addLayer(p.m, p.h, true));
        }
        return _results;
      };

      AmjadMapEditor.prototype.init = function() {
        var _this = this;
        AmjadMapEditor.__super__.init.call(this);
        this.on("TILELAYER_SELECTED", function() {
          this.working_pattern = null;
          return this.working_pivot = null;
        });
        this.on("PATTERN_SELECTED", function(pattern) {
          log.debug("pattern selected");
          log.debug(pattern);
          this.working_layer = null;
          this.working_pivot = pattern[0];
          return this.working_pattern = pattern.slice(1);
        });
        Hal.on("KEY_DOWN", function(ev) {
          var keycode;
          keycode = ev.keyCode;
          if (keycode === Hal.Keys.SHIFT) {
            _this.key_pressed["shift"] = true;
          } else if (keycode === Hal.Keys.D) {
            _this.key_pressed["d"] = true;
          } else if (keycode === Hal.Keys.F) {
            _this.key_pressed['f'] = true;
          }
          if (_this.key_pressed["shift"]) {
            if (_this.working_pivot) {
              _this.drawPattern();
            }
            if (_this.working_layer != null) {
              return _this.showTileLayer();
            }
          } else if (_this.key_pressed["d"] && (_this.clicked_layer != null)) {
            _this.clicked_layer.parent_tile.removeLayer(_this.clicked_layer.level);
            return _this.clicked_layer = null;
          }
        });
        Hal.on("KEY_UP", function(ev) {
          var keycode;
          keycode = ev.keyCode;
          if (keycode === Hal.Keys.SHIFT) {
            _this.key_pressed["shift"] = false;
          } else if (keycode === Hal.Keys.D) {
            _this.key_pressed["d"] = false;
          } else if (keycode === Hal.Keys.F) {
            _this.key_pressed['f'] = false;
          }
          if (keycode === Hal.Keys.I && ev.ctrlKey) {
            Hal.trigger("DEBUG_MODE", !_this.debug);
          } else if (keycode === Hal.Keys.M && ev.ctrlKey) {
            _this.toggleArabMap();
          }
          log.debug("keycode: " + keycode);
          log.debug("ctrl: " + ev.ctrlKey);
          if (keycode === Hal.Keys.C && ev.ctrlKey) {
            _this.trigger("TOGGLE_SHOW_SPAN");
          }
          if (_this.showtile && !_this.key_pressed["shift"]) {
            return _this.hideTileLayer();
          }
        });
        this.on("TOGGLE_SHOW_SPAN", function() {
          _this.show_span = !_this.show_span;
          return log.debug("show span " + _this.show_span);
        });
        this.on("REQUEST_MAP_SAVE", function(name) {
          var map;
          map = _this.save();
          return Hal.trigger("MAP_SAVED", {
            map: map,
            name: name + "_" + _this.nrows + "x" + _this.ncols
          });
        });
        Hal.on("SCROLL", function(ev) {
          if (_this.showtile != null) {
            if (ev.down) {
              return _this.working_layer_adjpos[1]--;
            } else {
              return _this.working_layer_adjpos[1]++;
            }
          }
        });
        Hal.on("DRAG_STARTED", function() {
          return _this.drag_started = true;
        });
        Hal.on("DRAG_ENDED", function() {
          return _this.drag_started = false;
        });
        Hal.on("MOUSE_MOVE", function() {
          if (_this.key_pressed["shift"] && _this.showtile && _this.drag_started) {
            return _this.addWorkingLayer();
          }
        });
        return this.on("REQUEST_MAP_LOAD", function(data) {
          log.debug(data);
          return _this.load(data);
        });
      };

      return AmjadMapEditor;

    })(AmjadMap);
    AmjadMapEditor.prototype.save = function() {
      var map, tile, _i, _len, _ref;
      map = [];
      _ref = this.tiles;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        tile = _ref[_i];
        if ((tile != null) && tile.num_layers > 0) {
          map.push(tile.save());
        }
      }
      return map;
    };
    AmjadMap.prototype.toggleArabMap = function() {
      var _this = this;
      if (this.arab_map_drawable != null) {
        return this.stopDrawingArabMap();
      } else {
        return this.arab_map_drawable = Hal.on("ENTER_FRAME", function(delta) {
          _this.layer_renderers[1].ctx.setTransform(_this.camera.zoom, 0, 0, _this.camera.zoom, _this.camera.pos[0], _this.camera.pos[1]);
          return _this.layer_renderers[1].ctx.drawImage(_this.arab_map, 0, 0, _this.arab_map.width, _this.arab_map.height, 0, 0, _this.world_dim[2], _this.world_dim[3]);
        });
      }
    };
    AmjadMap.prototype.stopDrawingArabMap = function() {
      Hal.removeTrigger("ENTER_FRAME", this.arab_map_drawable);
      return this.arab_map_drawable = null;
    };
    return AmjadMapEditor;
  });

}).call(this);
