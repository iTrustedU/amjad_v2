"use strict"

define () ->
    $NOTIF = $(".inplace-notification")
    animateFx = (t, byhowmuch) ->
        t.animate
            "top": byhowmuch + "px",
            "opacity": "0"
        ,1000
        () ->
            t.css("opacity", 255)
            t.remove()
        

    showNotification = (text, x, y) ->
        t = $NOTIF.clone()
        t.text(text)
        t.prependTo($NOTIF.parent())
        t.css("top", y)
        t.css("left", x)
        byhowmuch = y - 50
        t.show()
        t.delay(500)
        t.queue(animateFx(t, byhowmuch))

    Hal.on "SHOW_NOTIFICATION", (notif) ->
        showNotification(notif.text, notif.x, notif.y)
