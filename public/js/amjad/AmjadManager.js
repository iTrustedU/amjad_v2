(function() {
  "use strict";
  define(["Ajax", "../amjad/AmjadMap", "MapLoaderBar", "../amjad/empire/Empire", "../amjad/hud/ActionMenu", "../amjad/hud/BottomMenu", "../amjad/hud/dialogs/TavernDialog", "../amjad/hud/dialogs/WartentDialog", "../amjad/hud/dialogs/CommanderDialog", "../amjad/hud/dialogs/CaravanDialog", "../amjad/hud/dialogs/TrainingGroundsDialog", "../amjad/hud/dialogs/TradeStationDialog"], function(Ajax, AmjadMap, MapLoaderBar, Empire) {
    var AmjadManager;
    AmjadManager = (function() {
      function AmjadManager(city_meta, zone_meta) {
        this.city_meta = city_meta;
        this.zone_meta = zone_meta;
        require(["../amjad/hud/HudEpilog"], function() {
          return log.debug("hud epilog done");
        });
        this.hud = $("#hud");
        this.loaded = {
          "zone": false,
          "city": false
        };
        this.current_view = null;
        this.city = new AmjadMap({
          "name": "city",
          "numrows": this.city_meta.numrows,
          "numcols": this.city_meta.numcols,
          "tiledim": {
            "width": 128,
            "height": 64
          }
        });
        this.city.camera.pos[0] = -400;
        this.city.camera.pos[1] = -1000;
        this.city.camera.enableArrowKeys();
        this.empire = new Empire();
        this.zone = new AmjadMap({
          "name": "zone",
          "numrows": this.zone_meta.numrows,
          "numcols": this.zone_meta.numcols,
          "tiledim": {
            "width": 128,
            "height": 64
          }
        });
        this.zone.camera.enableArrowKeys();
        this.zone.camera.pos[0] = -1800;
        this.zone.camera.pos[1] = -200;
        this.city_surrounded_counter = 0;
        this.enterZoneView();
        this.init();
      }

      AmjadManager.prototype.loadMap = function(map, url) {
        var _this = this;
        return $.getJSON("assets/maps/" + url, function(data) {
          var mapbar;
          mapbar = null;
          mapbar = new MapLoaderBar(map, function() {
            _this.loaded[map.name] = true;
            Hal.scm.addScene(map);
            Hal.scm.putOnTop(map);
            _this.hud.show();
            map.trigger("CAMERA_MOVED", map.camera.pos);
            map.trigger("ZOOM", map.camera.zoom);
            return map.update();
          });
          return map.load({
            map: data,
            name: url
          });
        });
      };

      AmjadManager.prototype.enterCityView = function() {
        var _this = this;
        this.current_view = this.city;
        this.zone.pause();
        if (!this.loaded["city"]) {
          this.loadMap(this.city, this.city_meta.url);
          return Hal.on("SCENE_ADDED_CITY", function() {
            if (_this.city.clicked_layer) {
              _this.city.clicked_layer.trigger("DESELECTED");
            }
            return _this.city.clicked_layer = null;
          });
        } else {
          Hal.scm.putOnTop(this.city);
          if (this.city.clicked_layer) {
            this.city.clicked_layer.trigger("DESELECTED");
          }
          return this.city.clicked_layer = null;
        }
      };

      AmjadManager.prototype.enterZoneView = function() {
        var _this = this;
        this.current_view = this.zone;
        this.city.pause();
        if (!this.loaded["zone"]) {
          this.loadMap(this.zone, this.zone_meta.url);
          return Hal.on("SCENE_ADDED_ZONE", function() {
            if (_this.zone.clicked_layer != null) {
              _this.zone.clicked_layer.trigger("DESELECTED");
            }
            return _this.zone.clicked_layer = null;
          });
        } else {
          Hal.scm.putOnTop(this.zone);
          if (this.zone.clicked_layer != null) {
            this.zone.clicked_layer.trigger("DESELECTED");
          }
          return this.zone.clicked_layer = null;
        }
      };

      AmjadManager.prototype.init = function() {
        var _this = this;
        this.city.on("WARTENT_ADDED", function(tent) {
          return _this.empire.Buildings.WarTent = tent;
        });
        this.city.on("TRAINING_GROUNDS_ADDED", function(trgrounds) {
          return _this.empire.Buildings.TrainingGrounds = trgrounds;
        });
        this.city.on("TAVERN_ADDED", function(tavern) {
          return _this.empire.Buildings.Tavern = tavern;
        });
        this.city.on("TRADE_STATION_ADDED", function(trstation) {
          return _this.empire.Buildings.TradeStation = trstation;
        });
        return Handlebars.registerHelper("concat_sprite", function(src) {
          return "assets/sprites/" + src + ".png";
        });
      };

      AmjadManager.prototype.listCityBuildings = function() {
        var $bldslist, blds, building, html, k, source, template, _results;
        log.debug("uhuohoh");
        blds = this.city.tmngr.getBuildingsByType("city_" + this.empire.fraction);
        log.debug(blds);
        $bldslist = $("#buildings-list");
        $bldslist.empty();
        source = $("#city-buildings-list").html();
        template = Handlebars.compile(source);
        _results = [];
        for (k in blds) {
          building = blds[k];
          log.debug(k);
          log.debug(building);
          html = $(template(building));
          html.data("building", building);
          html.unbind("click");
          html.click(function() {
            return Hal.trigger("CONSTRUCT_BUILDING", $(this).data("building"));
          });
          log.debug(html);
          _results.push($bldslist.append(html));
        }
        return _results;
      };

      return AmjadManager;

    })();
    return AmjadManager;
  });

}).call(this);
