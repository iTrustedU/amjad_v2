"use strict"


define ["../amjad/AmjadMap"], 

(AmjadMap) ->

    class AmjadMapEditor extends AmjadMap
        constructor: (settings) ->
            super(settings)
            @drag_started = false
            @delete_mode = false
            @delete_buffer = []
            @key_pressed = {"shift": false, "d": false}
            @show_span = false

            @pattern = {}
            @is_pivot = false
            @pattern_pivot = null

            @working_pattern = null
            @working_pivot = null

            @arab_map = new Image()
            @arab_map.src = "assets/sprites/amjad/arab_map.svg"

        setPatternPivot: (tile) ->
            return if not tile?
            @pattern_pivot = tile
            @pattern["pivot"] = tile
            @drawCurrentPattern()

        addToPattern: (tile) ->
            return if not tile?
            even = not (@tile_under_mouse.col % 2)
            odd = 0
            if even and (@tile_under_mouse.col % 2)
                odd = 1
            off_r = tile.parent_tile.row - @pattern_pivot.parent_tile.row + odd
            off_c = tile.parent_tile.col - @pattern_pivot.parent_tile.col

            @pattern["#{off_r}_#{off_c}"] = tile

            # log.warn "pattern...."
            # log.debug @pattern

        drawCurrentPattern: () ->
            if not @draw_curr_pattern?
                @draw_curr_pattern = 
                Hal.on "ENTER_POST_FRAME", (delta) =>
                    if not @key_pressed['f']
                        Hal.removeTrigger "ENTER_POST_FRAME", @draw_curr_pattern
                        log.debug "Prikazi dijalog za cuvanje patterna"
                        Hal.trigger "NEW_PATTERN_CREATED", @pattern
                        @draw_curr_pattern = null
                        @pattern = {}
                        @pattern_pivot = null
                        return
                    for k, v of @pattern
                        buf = Hal.im.tintImage(v.sprite.img, "cyan", "0.3")
                        @layer_renderers[3].ctx.drawImage(buf, v.pos[0] - v.w, v.pos[1] - v.h)


        drawPattern: () ->
            if not @draw_pattern?
                @draw_pattern = 
                Hal.on "ENTER_POST_FRAME", (delta) =>
                    if @tile_under_mouse and @working_pattern and @key_pressed["shift"]
                        pmeta = @tmngr.getTile(@working_pivot.m)
                        img = Hal.asm.getSprite(pmeta.sprite).img
                        @layer_renderers[@layers[pmeta.level]].ctx.drawImage(img, @tile_under_mouse.pos[0], @tile_under_mouse.pos[1])
                        
                        for p in @working_pattern
                            even = not ((p.off[1] + @tile_under_mouse.col) % 2)
                            odd = 0
                            if even and (@tile_under_mouse.col % 2)
                                odd = 1
                            t = @getTile((p.off[0] + @tile_under_mouse.row) + odd, (p.off[1] + @tile_under_mouse.col))
                            return if not t?
                            pmeta = @tmngr.getTile(p.m)
                            img = Hal.asm.getSprite(pmeta.sprite).img
                            @layer_renderers[@layers[pmeta.level]].ctx.drawImage(img, t.pos[0] + t.hw[0], t.pos[1] + t.hw[1])
                    else
                        Hal.removeTrigger "ENTER_POST_FRAME", @draw_pattern
                        @draw_pattern = null

        processMouseClick: (pos, type) ->
            super(pos, type)
            if @key_pressed['f']
                if not @pattern_pivot
                    @setPatternPivot(@clicked_layer)
                else
                    @addToPattern(@clicked_layer)
            else if @working_pattern and @key_pressed["shift"]
                log.debug "place pattern"
                @placeWorkingPattern(@tile_under_mouse)

        placeWorkingPattern: (tile) ->
            tile.addLayer(@working_pivot.m, @working_pivot.h, true)
            for p in @working_pattern              
                even = not ((p.off[1] + tile.col) % 2)
                odd = 0
                if even and (tile.col % 2)
                    odd = 1
                t = @getTile(p.off[0] + tile.row + odd, p.off[1] + tile.col)
                t.addLayer(p.m, p.h, true)
                #pmeta = @tmngr.getTile(p.m)
                #img = Hal.asm.getSprite(pmeta.sprite).img
                #@layer_renderers[@layers[pmeta.level]].ctx.drawImage(img, t.pos[0], t.pos[1])

        init: () ->
            super()

            @on "TILELAYER_SELECTED", () ->
                @working_pattern = null
                @working_pivot = null

            @on "PATTERN_SELECTED", (pattern) ->
                log.debug "pattern selected"
                log.debug pattern
                @working_layer = null
                @working_pivot = pattern[0]
                @working_pattern = pattern.slice(1)

            Hal.on "KEY_DOWN", (ev) =>
                keycode = ev.keyCode
                if keycode is Hal.Keys.SHIFT
                    @key_pressed["shift"] = true
                else if keycode is Hal.Keys.D
                    @key_pressed["d"] = true
                else if keycode is Hal.Keys.F
                    @key_pressed['f'] = true
                if @key_pressed["shift"]
                    if @working_pivot
                        @drawPattern()
                    if @working_layer?
                        @showTileLayer()
                else if @key_pressed["d"] and @clicked_layer?
                    @clicked_layer.parent_tile.removeLayer(@clicked_layer.level)
                    @clicked_layer = null

            Hal.on "KEY_UP", (ev) =>
                keycode = ev.keyCode
                if keycode is Hal.Keys.SHIFT
                    @key_pressed["shift"] = false
                else if keycode is Hal.Keys.D
                    @key_pressed["d"] = false
                else if keycode is Hal.Keys.F
                    @key_pressed['f'] = false

                if keycode is Hal.Keys.I and ev.ctrlKey
                    Hal.trigger "DEBUG_MODE", not @debug
                else if keycode is Hal.Keys.M and ev.ctrlKey
                    @toggleArabMap()

                log.debug "keycode: #{keycode}"
                log.debug "ctrl: #{ev.ctrlKey}"

                if keycode is Hal.Keys.C and ev.ctrlKey
                    @trigger "TOGGLE_SHOW_SPAN"

                if @showtile and not @key_pressed["shift"]
                    @hideTileLayer()

            @on "TOGGLE_SHOW_SPAN", () =>
                @show_span = not @show_span
                log.debug "show span #{@show_span}"

            @on "REQUEST_MAP_SAVE", (name) =>
                map = @save()
                Hal.trigger "MAP_SAVED", {map: map, name: name + "_" + @nrows + "x" + @ncols}

            Hal.on "SCROLL", (ev) =>
                if @showtile?
                    if ev.down
                        @working_layer_adjpos[1]--
                    else
                        @working_layer_adjpos[1]++

            Hal.on "DRAG_STARTED", () =>
                @drag_started = true

            Hal.on "DRAG_ENDED", () =>
                @drag_started = false

            Hal.on "MOUSE_MOVE", () =>
                if @key_pressed["shift"] and @showtile and @drag_started
                    @addWorkingLayer()

            @on "REQUEST_MAP_LOAD", (data) =>
                log.debug data
                @load(data)

    AmjadMapEditor::save = () ->
        map = []
        for tile in @tiles
            if tile? and tile.num_layers > 0
                map.push(tile.save())
        return map

    AmjadMap::toggleArabMap = () ->
        if @arab_map_drawable?
            @stopDrawingArabMap()
        else
            @arab_map_drawable = 
            Hal.on "ENTER_FRAME", (delta) =>
                @layer_renderers[1].ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])
                @layer_renderers[1].ctx.drawImage(@arab_map, 0, 0, @arab_map.width, @arab_map.height, 0, 0, @world_dim[2], @world_dim[3])

    AmjadMap::stopDrawingArabMap = () ->
        Hal.removeTrigger "ENTER_FRAME", @arab_map_drawable
        @arab_map_drawable = null

    return AmjadMapEditor