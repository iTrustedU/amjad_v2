(function() {
  "use strict";
  define(function() {
    var $NOTIF, animateFx, showNotification;
    $NOTIF = $(".inplace-notification");
    animateFx = function(t, byhowmuch) {
      t.animate({
        "top": byhowmuch + "px",
        "opacity": "0"
      }, 1000);
      return function() {
        t.css("opacity", 255);
        return t.remove();
      };
    };
    showNotification = function(text, x, y) {
      var byhowmuch, t;
      t = $NOTIF.clone();
      t.text(text);
      t.prependTo($NOTIF.parent());
      t.css("top", y);
      t.css("left", x);
      byhowmuch = y - 50;
      t.show();
      t.delay(500);
      return t.queue(animateFx(t, byhowmuch));
    };
    return Hal.on("SHOW_NOTIFICATION", function(notif) {
      return showNotification(notif.text, notif.x, notif.y);
    });
  });

}).call(this);
