(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../amjad/Tile", "Scene", "Renderer", "../amjad/TileManager", "../amjad/PathFinder"], function(Tile, Scene, Renderer, TileManager, PathFinder) {
    var AmjadMap;
    AmjadMap = (function(_super) {
      __extends(AmjadMap, _super);

      function AmjadMap(config) {
        var buildings_layer, decor_layer, hittest, i, j, poi_layer, terrain_layer, _i, _len, _ref;
        AmjadMap.__super__.constructor.call(this, config.name, Hal.r.bounds, false);
        this.tilew = config.tiledim.width;
        this.tileh = config.tiledim.height;
        this.nrows = +config.numrows;
        this.ncols = +config.numcols;
        this.tilew2prop = 2 / this.tilew;
        this.tileh2prop = 2 / this.tileh;
        this.tilew2 = this.tilew / 2;
        this.tileh2 = this.tileh / 2;
        this.tiles = [];
        this.mask = Hal.asm.getSprite("amjad/tilemask_128x64");
        this.over = {
          "green": Hal.asm.getSprite("amjad/grid_unit_over_green_128x64"),
          "red": Hal.asm.getSprite("amjad/grid_unit_over_red_128x64")
        };
        hittest = Hal.dom.createCanvas(this.tilew, this.tileh).getContext("2d");
        hittest.drawImage(this.mask.img, 0, 0);
        this.mask_data = hittest.getImageData(0, 0, this.tilew, this.tileh).data;
        _ref = this.mask_data;
        for (j = _i = 0, _len = _ref.length; _i < _len; j = ++_i) {
          i = _ref[j];
          this.mask_data[j] = i < 120;
        }
        this.initMap();
        this.search_range = [this.bounds[2], this.bounds[3]];
        this.camera.disableDrag();
        this.tile_under_mouse = void 0;
        this.display = {
          startr: 0,
          endr: 0,
          startc: 0,
          endc: 0
        };
        this.info = {
          row: "row: ",
          col: "col: ",
          tilename: "tile: ",
          start_row: "starting row: ",
          start_col: "staring col: ",
          end_row: "end row: ",
          end_col: "end_col: ",
          tile_x: "tile_x: ",
          tile_y: "tile_y: ",
          num_rendering: "no. rendereded entities: "
        };
        this.calcDrawingArea();
        this.total_rendered = 0;
        terrain_layer = Hal.dom.createCanvasLayer(1);
        Hal.dom.addCanvas(terrain_layer, 0, 0, true);
        decor_layer = Hal.dom.createCanvasLayer(2);
        Hal.dom.addCanvas(decor_layer, 0, 0, true);
        buildings_layer = Hal.dom.createCanvasLayer(3);
        Hal.dom.addCanvas(buildings_layer, 0, 0, true);
        poi_layer = Hal.dom.createCanvasLayer(4);
        Hal.dom.addCanvas(poi_layer, 0, 0, true);
        this.layer_renderers = [new Renderer(this.bounds, terrain_layer, this.camera), new Renderer(this.bounds, decor_layer, this.camera), new Renderer(this.bounds, buildings_layer, this.camera), new Renderer(this.bounds, poi_layer, this.camera)];
        this.layers = {
          terrain: 0,
          decor: 1,
          buildings: 2,
          poi: 3,
          character: 3
        };
        this.working_layer = null;
        this.working_layer_sprite = null;
        this.can_place_layer = false;
        this.cur_area = [];
        this.working_layer_adjpos = [0, 0];
        this.showtile = null;
        this.clicked_layer = null;
      }

      return AmjadMap;

    })(Scene);
    AmjadMap.prototype.drawInfo = function() {
      if (this.tile_under_mouse != null) {
        Hal.glass.ctx.fillText(this.info.row + this.tile_under_mouse.row, 0, 60);
        Hal.glass.ctx.fillText(this.info.col + this.tile_under_mouse.col, 0, 70);
        Hal.glass.ctx.fillText(this.info.tile_x + this.tile_under_mouse.pos[0], 0, 80);
        Hal.glass.ctx.fillText(this.info.tile_y + this.tile_under_mouse.pos[1], 0, 90);
      }
      Hal.glass.ctx.fillText(this.info.start_row + this.display.startr, 0, 100);
      Hal.glass.ctx.fillText(this.info.start_col + this.display.startc, 0, 110);
      Hal.glass.ctx.fillText(this.info.end_row + this.display.endr, 0, 120);
      Hal.glass.ctx.fillText(this.info.end_col + this.display.endc, 0, 130);
      return Hal.glass.ctx.fillText(this.info.num_rendering + this.total_rendered, 0, 140);
    };
    AmjadMap.prototype.calcDrawingArea = function() {
      return this.display = {
        startc: Math.max(0, Math.round(((-this.camera.pos[0] / this.tilew * 2) / this.camera.zoom) - 3)),
        endr: Math.min(this.nrows - 1, Math.ceil((this.bounds[3] / this.tileh) / this.camera.zoom) + 3),
        startr: Math.max(0, Math.round(((-this.camera.pos[1] / this.tileh) / this.camera.zoom) - 3)),
        endc: Math.min(this.ncols - 1, Math.floor((this.bounds[2] / this.tilew2) / this.camera.zoom) + 3)
      };
    };
    AmjadMap.prototype.addWorkingLayer = function() {
      return this.tile_under_mouse.addLayer(this.working_layer.name, this.working_layer_adjpos, false);
    };
    AmjadMap.prototype.init = function() {
      var _this = this;
      this.on("SHOWING_TILELAYER_STARTED", function() {
        return _this.camera.disableZoom();
      });
      this.on("SHOWING_TILELAYER_FINISHED", function() {
        return _this.camera.enableZoom();
      });
      this.on("TILELAYER_SELECTED", function(layer) {
        log.debug(layer);
        _this.working_layer = layer;
        _this.working_layer_sprite = Hal.asm.getSprite(_this.working_layer.sprite);
        return _this.calcCenterAdjPos(_this.working_layer_adjpos, _this.working_layer_sprite);
      });
      Hal.on("RIGHT_CLICK", function(pos) {
        if (_this.paused) {
          return;
        }
        return _this.camera.lerpTo(pos);
      });
      Hal.on("MOUSE_DBL_CLICK", function(pos) {
        return _this.processMouseClick(pos, "DBL_CLICK");
      });
      Hal.on("LEFT_CLICK", function(pos) {
        return _this.processMouseClick(pos, "CLICK");
      });
      this.on("ENTER_FULLSCREEN", function(sc) {
        var r, _i, _len, _ref, _results;
        _ref = _this.layer_renderers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          r = _ref[_i];
          _results.push(r.resize(Hal.r.canvas.width, Hal.r.canvas.height));
        }
        return _results;
      });
      this.on("EXIT_FULLSCREEN", function(sc) {
        var r, _i, _len, _ref, _results;
        _ref = _this.layer_renderers;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          r = _ref[_i];
          _results.push(r.resize(Hal.r.canvas.width, Hal.r.canvas.height));
        }
        return _results;
      });
      Hal.on("ENTER_FRAME", function() {
        Hal.glass.ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
        _this.g.ctx.setTransform(1, 0, 0, 1, 0, 0);
        _this.g.ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
        _this.layer_renderers[0].ctx.setTransform(1, 0, 0, 1, 0, 0);
        _this.layer_renderers[0].ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
        _this.layer_renderers[1].ctx.setTransform(1, 0, 0, 1, 0, 0);
        _this.layer_renderers[1].ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
        _this.layer_renderers[2].ctx.setTransform(1, 0, 0, 1, 0, 0);
        _this.layer_renderers[2].ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
        _this.layer_renderers[3].ctx.setTransform(1, 0, 0, 1, 0, 0);
        return _this.layer_renderers[3].ctx.clearRect(0, 0, _this.bounds[2], _this.bounds[3]);
      });
      Hal.on("MOUSE_MOVE", function() {
        return _this.tile_under_mouse = _this.getTileAt(_this.mpos);
      });
      this.on("CAMERA_MOVED", function() {
        return this.calcDrawingArea();
      });
      return this.on("ZOOM", function() {
        return this.calcDrawingArea();
      });
    };
    AmjadMap.prototype.processMouseClick = function(pos, type) {
      var ents, t, t1, tile, _i, _len;
      if (this.paused) {
        return;
      }
      if (this.clicked_layer != null) {
        this.clicked_layer.trigger("DESELECTED");
        this.clicked_layer = null;
      }
      if (this.showtile && this.can_place_layer) {
        this.addWorkingLayer();
        this.hideTileLayer();
        return;
      }
      t = performance.now();
      ents = this.quadspace.searchInRange([this.mpos[0] - this.search_range[0], this.mpos[1] - this.search_range[1], 2 * this.search_range[0], 2 * this.search_range[1]]);
      t1 = performance.now() - t;
      log.info(t1);
      log.info(ents.length);
      for (_i = 0, _len = ents.length; _i < _len; _i++) {
        tile = ents[_i];
        if (!tile.isTransparent(this.mpos.slice())) {
          continue;
        }
        log.debug(tile);
        if (this.clicked_layer == null) {
          this.clicked_layer = tile;
        } else {
          if ((tile.parent_tile.col === this.clicked_layer.parent_tile.col) && (tile.parent_tile.row === this.clicked_layer.parent_tile.row)) {
            log.debug(tile.level);
            log.debug(this.clicked_layer.level);
            if (tile.level > this.clicked_layer.level) {
              this.clicked_layer = tile;
            }
          } else if (tile.parent_tile.row === this.clicked_layer.parent_tile.row) {
            if (tile.h + tile.pos[1] > this.clicked_layer.h + this.clicked_layer.pos[1]) {
              this.clicked_layer = tile;
            }
          } else if (tile.parent_tile.col === this.clicked_layer.parent_tile.col) {
            if (tile.h + tile.pos[1] > this.clicked_layer.h + this.clicked_layer.pos[1]) {
              this.clicked_layer = tile;
            }
          } else if ((tile.parent_tile.col !== this.clicked_layer.parent_tile.col) && (tile.parent_tile.row !== this.clicked_layer.parent_tile.row)) {
            if (tile.h + tile.pos[1] > this.clicked_layer.h + this.clicked_layer.pos[1]) {
              this.clicked_layer = tile;
            }
          }
        }
      }
      if (this.clicked_layer != null) {
        log.debug("clicked layer");
        log.debug(this.clicked_layer);
        this.trigger("LAYER_SELECTED", this.clicked_layer);
        return this.clicked_layer.trigger("SELECTED_" + type);
      }
    };
    AmjadMap.prototype.drawSpan = function(span, color, level) {
      var s, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = span.length; _i < _len; _i++) {
        s = span[_i];
        _results.push(this.layer_renderers[level].ctx.drawImage(this.over[color].img, s.pos[0], s.pos[1]));
      }
      return _results;
    };
    AmjadMap.prototype.canBePlacedOn = function(span, layer) {
      var k, _i, _len;
      for (_i = 0, _len = span.length; _i < _len; _i++) {
        k = span[_i];
        if (k.layers[layer] != null) {
          if (k.layers[layer].is_partial || k.layers[layer].is_root) {
            return false;
          }
        }
      }
      return true;
    };
    AmjadMap.prototype.findInDirectionOf = function(tile, dirstr, len) {
      var dir, fromc, fromr, out, t;
      if (tile == null) {
        return [];
      }
      out = [];
      out.push(tile);
      fromr = tile.row;
      fromc = tile.col;
      dir = tile.direction[dirstr];
      while (len > 0) {
        t = this.getTile(fromr, fromc, dir);
        if (t != null) {
          out.push(t);
          fromr = t.row;
          fromc = t.col;
          dir = t.direction[dirstr];
        } else {
          break;
        }
        len--;
      }
      return out;
    };
    AmjadMap.prototype.getSpanArea = function(tile, size) {
      var neasts, nwests, out, root, sizelen, w, _i, _len;
      root = ~~Math.sqrt(size.length);
      sizelen = size.length - 1;
      nwests = this.findInDirectionOf(tile, "northwest", root - 1);
      out = [];
      for (_i = 0, _len = nwests.length; _i < _len; _i++) {
        w = nwests[_i];
        neasts = this.findInDirectionOf(w, "northeast", root - 1);
        out = out.concat(neasts);
      }
      out = out.filter(function(_, i) {
        return +size[sizelen - ((i % root) * root) - ~~(i / root)];
      });
      return out;
    };
    AmjadMap.prototype.getTileAt = function(pos) {
      var coord;
      coord = this.toOrtho(pos);
      if (coord[0] < 0.0 || coord[1] < 0.0 || coord[1] >= this.nrows || coord[0] >= this.ncols) {
        return null;
      }
      return this.tiles[Math.floor(coord[0]) + Math.floor(coord[1]) * this.ncols];
    };
    AmjadMap.prototype.initMap = function() {
      var i, j, t, x, y, _i, _j, _ref, _ref1;
      this.clicked_layer = null;
      log.debug("columns: " + this.ncols);
      log.debug("rows: " + this.nrows);
      log.debug("tilew2: " + this.tilew2);
      log.debug("tileh: " + this.tileh);
      this.world_dim = [0, 0, (this.ncols + 1) * this.tilew2, (this.nrows + 1) * this.tileh];
      log.debug(this.world_dim);
      this.resetQuadSpace(this.world_dim);
      for (i = _i = 0, _ref = this.nrows - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        for (j = _j = 0, _ref1 = this.ncols - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
          x = (j / 2) * this.tilew + this.camera.pos[0];
          y = (i + ((j % 2) / 2)) * this.tileh + this.camera.pos[1];
          t = new Tile({
            "x": x,
            "y": y,
            "row": i,
            "col": j,
            "sprite": "amjad/grid_unit_128x64"
          });
          this.tiles.push(this.addEntity(t));
        }
      }
      this.pathfinder = new PathFinder(this);
      this.tmngr = TileManager;
      this.camera.setViewFrustum([this.tilew2, this.tileh2, this.ncols * this.tilew2, this.nrows * this.tileh]);
      return this.calcDrawingArea();
    };
    AmjadMap.prototype.removeTile = function(row, col, dir) {
      var ind;
      if (dir == null) {
        dir = [0, 0];
      }
      ind = (col + dir[1]) + (row + dir[0]) * this.ncols;
      this.removeEntity(this.tiles[ind]);
      return this.tiles[ind] = null;
    };
    AmjadMap.prototype.toOrtho = function(pos) {
      var coldiv, off_x, off_y, rowdiv, transp;
      coldiv = pos[0] * this.tilew2prop;
      rowdiv = pos[1] * this.tileh2prop;
      off_x = ~~(pos[0] - ~~(coldiv * 0.5) * this.tilew);
      off_y = ~~(pos[1] - ~~(rowdiv * 0.5) * this.tileh);
      transp = this.mask_data[(off_x + this.tilew * off_y) * 4 + 3];
      return [coldiv - (transp ^ !(coldiv & 1)), (rowdiv - (transp ^ !(rowdiv & 1))) / 2];
    };
    AmjadMap.prototype.getTile = function(row, col, dir) {
      if (dir == null) {
        dir = [0, 0];
      }
      return this.tiles[(col + dir[1]) + (row + dir[0]) * this.ncols];
    };
    AmjadMap.prototype.update = function(delta) {
      var i, j, tile, _i, _j, _ref, _ref1, _ref2, _ref3;
      if (this.paused) {
        return;
      }
      AmjadMap.__super__.update.call(this, delta);
      this.layer_renderers[0].ctx.setTransform(this.camera.zoom, 0, 0, this.camera.zoom, this.camera.pos[0], this.camera.pos[1]);
      this.layer_renderers[1].ctx.setTransform(this.camera.zoom, 0, 0, this.camera.zoom, this.camera.pos[0], this.camera.pos[1]);
      this.layer_renderers[2].ctx.setTransform(this.camera.zoom, 0, 0, this.camera.zoom, this.camera.pos[0], this.camera.pos[1]);
      this.layer_renderers[3].ctx.setTransform(this.camera.zoom, 0, 0, this.camera.zoom, this.camera.pos[0], this.camera.pos[1]);
      this.total_rendered = 0;
      for (i = _i = _ref = this.display.startr, _ref1 = this.display.startr + this.display.endr; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; i = _ref <= _ref1 ? ++_i : --_i) {
        for (j = _j = _ref2 = this.display.startc, _ref3 = this.display.endc + this.display.startc; _ref2 <= _ref3 ? _j <= _ref3 : _j >= _ref3; j = _ref2 <= _ref3 ? ++_j : --_j) {
          tile = this.tiles[j + i * this.ncols];
          if (tile == null) {
            continue;
          }
          if (!this.camera.isVisible(tile) && (tile.layers[3] == null)) {
            continue;
          }
          tile.update();
          this.total_rendered++;
        }
      }
      if (this.tile_under_mouse) {
        this.layer_renderers[1].ctx.drawImage(this.over.green.img, this.tile_under_mouse.pos[0], this.tile_under_mouse.pos[1]);
        if (this.debug) {
          return this.drawInfo();
        }
      }
    };
    AmjadMap.prototype.showTileLayer = function() {
      var skeep,
        _this = this;
      this.calcCenterAdjPos(this.working_layer_adjpos, this.working_layer_sprite);
      skeep = 0;
      log.debug("showtile: " + (this.showtile != null));
      if (this.showtile == null) {
        this.trigger("SHOWING_TILELAYER_STARTED", this.working_layer);
        return this.showtile = Hal.on("ENTER_FRAME", function(delta) {
          if (_this.tile_under_mouse && _this.working_layer) {
            _this.layer_renderers[3].ctx.setTransform(_this.camera.zoom, 0, 0, _this.camera.zoom, _this.camera.pos[0], _this.camera.pos[1]);
            if (skeep % 2) {
              _this.cur_area = _this.getSpanArea(_this.tile_under_mouse, _this.working_layer.size);
            }
            if (_this.canBePlacedOn(_this.cur_area, _this.layers[_this.working_layer.level])) {
              _this.drawSpan(_this.cur_area, "green", 3);
              _this.can_place_layer = true;
            } else {
              _this.drawSpan(_this.cur_area, "red", 3);
              _this.can_place_layer = false;
            }
            _this.layer_renderers[3].ctx.drawImage(_this.working_layer_sprite.img, _this.tile_under_mouse.pos[0] - _this.working_layer_adjpos[0], _this.tile_under_mouse.pos[1] - _this.working_layer_adjpos[1]);
            return skeep = !skeep % 2;
          } else {
            return _this.hideTileLayer();
          }
        });
      }
    };
    AmjadMap.prototype.hideTileLayer = function() {
      Hal.removeTrigger("ENTER_FRAME", this.showtile);
      this.showtile = null;
      return this.trigger("SHOWING_TILELAYER_FINISHED", this.working_layer);
    };
    AmjadMap.prototype.calcCenterAdjPos = function(layerpos, layersprite) {
      layerpos[1] = layersprite.h - this.tileh;
      return layerpos[0] = (layersprite.w / 2) - this.tilew2;
    };
    AmjadMap.prototype.drawSpanBorders = function(borders, color, layer) {
      return this.drawSpan(borders, color, layer);
    };
    AmjadMap.prototype.getSpanBorders = function(tile, size) {
      var laste, lastw, lneasts, lnwests, neasts, nwests, out, root;
      root = ~~Math.sqrt(size.length);
      tile = this.findInDirectionOf(tile, "south", 1)[1];
      nwests = this.findInDirectionOf(tile, "northwest", root + 1);
      neasts = this.findInDirectionOf(tile, "northeast", root + 1);
      lastw = nwests[nwests.length - 1];
      laste = neasts[neasts.length - 1];
      lnwests = this.findInDirectionOf(lastw, "northeast", root + 1);
      lneasts = this.findInDirectionOf(laste, "northwest", root + 1);
      out = nwests.concat(neasts).concat(lnwests).concat(lneasts);
      return out;
    };
    AmjadMap.prototype.load = function(data) {
      var del, g, l, ncols, nrows, size, _i, _j, _len, _len1, _ref, _ref1,
        _this = this;
      this.trigger("MAP_LOADING_STARTED", data.map.length);
      this.pause();
      this.prev_camera_data = {
        pos: this.camera.pos.slice(),
        zoom: this.camera.zoom
      };
      this.camera.pos = [0, 0];
      this.camera.zoom = 1;
      size = data.name.match(/(\d+x\d+)/g)[0].split("x");
      nrows = +size[0];
      ncols = +size[1];
      log.debug("nrows: " + nrows);
      log.debug("ncols: " + ncols);
      del = nrows !== this.nrows && ncols !== this.ncols;
      _ref = this.tiles;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        g = _ref[_i];
        _ref1 = g.layers;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          l = _ref1[_j];
          if (l != null) {
            g.removeLayer(l.level);
          }
        }
        if (del) {
          this.removeTile(g.row, g.col);
        }
      }
      this.tiles = [];
      this.nrows = nrows;
      this.ncols = ncols;
      this.initMap();
      return setTimeout(function() {
        return _this.loadTile(0, data);
      }, 100);
    };
    AmjadMap.prototype.loadRandomMap = function(i, data) {
      var j, k, t, _i, _ref, _results;
      _results = [];
      for (i = _i = 0, _ref = this.ncols - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (j = _j = 0, _ref1 = this.nrows - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
            t = this.getTile(i, j);
            k = 5;
            _results1.push((function() {
              var _results2;
              _results2 = [];
              while (k > 0 && !t.isFull()) {
                this.addRandomLayer(t);
                _results2.push(--k);
              }
              return _results2;
            }).call(this));
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };
    AmjadMap.prototype.addRandomLayer = function(t) {
      var area, hw, index, randt, randts, spr, tileLayer, tiles, tkeys, tskeys;
      tskeys = Object.keys(this.tmngr.Tiles);
      randts = ~~(Math.random() * tskeys.length);
      index = tskeys[randts];
      tiles = amj.tmngr.Tiles[index];
      tkeys = Object.keys(tiles);
      randt = ~~(Math.random() * tkeys.length);
      index = tkeys[randt];
      tileLayer = tiles[index];
      hw = [0, 0];
      spr = Hal.asm.getSprite(tileLayer.sprite);
      this.calcCenterAdjPos(hw, spr);
      area = this.getSpanArea(t, tileLayer.size);
      if (this.canBePlacedOn(area, this.layers[tileLayer.level])) {
        return t.addLayer(tileLayer.name, hw, true);
      }
    };
    AmjadMap.prototype.loadTile = function(i, data) {
      var g, t,
        _this = this;
      if (i >= data.map.length) {
        this.camera.pos = this.prev_camera_data.pos;
        this.camera.zoom = this.prev_camera_data.zoom;
        this.trigger("MAP_LOADED", data.name);
        return this.resume();
      } else {
        g = data.map[i];
        t = this.tiles[g.c + g.r * this.ncols];
        if (g.layers !== null) {
          t.load(g.l);
        }
        if ((i % this.nrows) === 0) {
          return setTimeout(function() {
            return _this.loadTile(i + 1, data);
          }, 10);
        } else {
          return this.loadTile(i + 1, data);
        }
      }
    };
    AmjadMap.prototype.getNeighbours = function(tile) {
      var dir, n, out, _i, _len, _ref;
      out = [];
      _ref = Object.keys(tile.direction);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        dir = _ref[_i];
        n = this.getTile(tile.row, tile.col, tile.direction[dir]);
        if (n != null) {
          out.push(n);
        }
      }
      return out;
    };
    AmjadMap.prototype.getReachableNeighbours = function(tile) {
      var dir, n, out, _i, _len, _ref;
      out = [];
      _ref = Object.keys(tile.direction);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        dir = _ref[_i];
        n = this.getTile(tile.row, tile.col, tile.direction[dir]);
        if ((n != null) && n.layers[0] && n.num_layers === 1 && !n.is_partial && !n.layers[3]) {
          out.push(n);
        }
      }
      return out;
    };
    return AmjadMap;
  });

}).call(this);
