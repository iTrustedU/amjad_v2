(function() {
  "use strict";
  var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  define(["Ajax", "../amjad/TileLayer", "../amjad/WalkableTile", "../amjad/buildings/BedouinCity", "../amjad/buildings/CityTavern", "../amjad/buildings/WarTent", "../amjad/heroes/Commander", "../amjad/heroes/CaravanLeader", "../amjad/buildings/TrainingGrounds", "../amjad/buildings/TradeStation"], function(Ajax, TileLayer, WalkableTile, BedouinCity, CityTavern, WarTent, Commander, CaravanLeader, TrainingGrounds, TradeStation) {
    var TileManager;
    TileManager = (function() {
      function TileManager() {
        var _this = this;
        this.Tiles = {
          buildings: {},
          character: {},
          terrain: {},
          decor: {},
          poi: {}
        };
        this.TilesByName = {};
        Ajax.get("assets/amjad/TilesList.json", function(tiles) {
          var k, t, _results;
          log.debug("TileManager loaded tiles.");
          tiles = JSON.parse(tiles);
          _results = [];
          for (k in tiles) {
            t = tiles[k];
            _results.push(_this.addTile(t));
          }
          return _results;
        });
        Hal.on("TILE_ADDED", function(tile) {
          return _this.addTile(tile);
        });
      }

      TileManager.prototype.addTile = function(tile) {
        this.Tiles[tile.level][tile.name] = tile;
        return this.TilesByName[tile.name] = tile;
      };

      TileManager.prototype.getTile = function(key) {
        return this.TilesByName[key];
      };

      TileManager.prototype.newTileFromMeta = function(level, meta, lpos, adjpos, renderer, parent) {
        if (meta.level === "character") {
          if (meta.attr.type === "commander") {
            return new Commander(level, meta, lpos, adjpos, renderer, parent);
          }
          if (meta.attr.type === "caravan") {
            return new CaravanLeader(level, meta, lpos, adjpos, renderer, parent);
          }
        } else if (meta.level === "buildings") {
          if (meta.attr.type === "empire_building_bedouin") {
            return new BedouinCity(level, meta, lpos, adjpos, renderer, parent);
          } else if (meta.name === "buildings_tavern") {
            return new CityTavern(level, meta, lpos, adjpos, renderer, parent);
          } else if (meta.name === "buildings_training_grounds") {
            return new TrainingGrounds(level, meta, lpos, adjpos, renderer, parent);
          } else if (meta.name === "buildings_trade_station") {
            return new TradeStation(level, meta, lpos, adjpos, renderer, parent);
          } else if (meta.name === "buildings_wartent") {
            return new WarTent(level, meta, lpos, adjpos, renderer, parent);
          } else {
            return new TileLayer(level, meta, lpos, adjpos, renderer, parent);
          }
        } else {
          return new TileLayer(level, meta, lpos, adjpos, renderer, parent);
        }
      };

      TileManager.prototype.getTilesByType = function(cls, type) {
        var k, out, v, _ref, _ref1;
        if (_ref = !cls, __indexOf.call(Object.keys(this.Tiles), _ref) >= 0) {
          return;
        }
        out = {};
        _ref1 = this.Tiles[cls];
        for (k in _ref1) {
          v = _ref1[k];
          if (v.attr.type === type) {
            out[k] = v;
          }
        }
        return out;
      };

      return TileManager;

    })();
    return new TileManager();
  });

}).call(this);
