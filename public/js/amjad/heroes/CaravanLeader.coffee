"use strict"

define ["../../amjad/WalkableTile"], 

(WalkableTile) ->

    class CaravanLeader extends WalkableTile

        init: () ->
            super()
            @name = @meta.attr.name

            @max_weight = 100
            @weight = 0
            @money = 0
            @experience = 0

            # @capacity = @meta.attr.capacity
            # @tongue = @meta.attr.tongue
            # @speed = @meta.attr.speed

            @next_level_in = 1000
            @goods = {}
            @exp_level = 1
            @sent_to_poi = false
            @max_vel = ~~@meta.attr.velocity
            @caravan_selected = false

            @on "SELECTED_CLICK", () ->
                @caravan_selected = true

            @scene.on "LAYER_SELECTED", (layer) =>
                if @caravan_selected and not layer.equals(@)
                    destination = layer.parent_tile
                    #@goTo(destination)
                    @caravan_selected = false

                    #return if layer is @
                    #return if not @selected

                    destination = null
                    if layer.name is "buildings_trade_poi"
                        destination = @scene.getSpanBorders(layer.parent_tile, layer.meta.size)[0]
                        @sent_to_city = true
                    else
                        destination = layer.parent_tile
                        @sent_to_city = false

                    if not destination?
                        return

                    @goTo(destination)

            @on "FINISHED_WALKING", (tile) ->
                if @sent_to_city
                    @sent_to_city = false
                    @money += 100
                    @experience = (~~@experience + 100) + ""
                    amjad.empire.addMoney(100)
                    dest = @scene.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size)[0]
                    @goTo(dest)

            @on "SELECTED_DBL_CLICK", () ->
                x = @pos[0] + @scene.camera.pos[0] * @scene.camera.zoom
                y = @pos[1] + @scene.camera.pos[1] * @scene.camera.zoom - @h
                Hal.trigger "SHOW_NOTIFICATION", {text: @name, x: x, y: y}
                Hal.trigger "OPEN_CARAVAN_DIALOG", @


        getExpPercentage: () ->
            return (@experience / @next_level_in) * 100


    return CaravanLeader