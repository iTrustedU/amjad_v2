"use strict"

define ["../../amjad/WalkableTile", "../../amjad/TileNotification"], 

(WalkableTile, TileNotification) ->

    class Commander extends WalkableTile

        init: () ->
            super()
            @name           = @meta.attr.name
            @experience     = @meta.attr.experience
            @speed          = @meta.attr.speed
            @leadership     = @meta.attr.leadership
            @readiness      = @meta.attr.readiness
            @next_level_in  = 1000
            @inventory      =
                "head": null
                "neck": null
                "back": null
                "torso": null
                "r-hand": null
                "l-hand": null
                "l-ring": null
                "r-ring": null
                "legs": null
                "boots": null
                        

            @units = {}
            @exp_level = 1
            @sent_to_city = false
            @max_vel = 4
            @commander_selected = false

            @on "SELECTED_DBL_CLICK", () ->
                x = @pos[0] + @scene.camera.pos[0] * @scene.camera.zoom
                y = @pos[1] + @scene.camera.pos[1] * @scene.camera.zoom - @h
                Hal.trigger "SHOW_NOTIFICATION", {text: @name, x: x, y: y}
                Hal.trigger "OPEN_COMMANDER_DIALOG", @

            @on "SELECTED_CLICK", () ->
                @commander_selected = true

            @scene.on "LAYER_SELECTED", (layer) =>
                # log.debug layer
                # return if layer is @
                # return if not @selected

                # destination = null
                # if layer.meta.attr.type is "civil_city"
                #     destination = @scene.getReachableNeighbours(layer.parent_tile)[0]
                #     @sent_to_city = true
                # else
                #     destination = layer.parent_tile
                #     @sent_to_city = false

                # if not destination?
                #     return

                # log.debug "destination----"
                # log.debug destination
                # log.debug "---------------"
                if @commander_selected and not layer.equals(@)
                    destination = layer.parent_tile
                    
                    @commander_selected = false
                    destination = null
                    if layer.meta.attr.type is "empire_building_civil"
                        destination = @scene.getSpanBorders(layer.parent_tile, layer.meta.size)[0]
                        @sent_to_city = true
                    else
                        destination = layer.parent_tile
                        @sent_to_city = false

                    if not destination?
                        return
                    
                    log.debug "destination----"
                    log.debug destination
                    log.debug "---------------"
                    @goTo(destination)
                    
            @on "FINISHED_WALKING", (tile) ->
                if @sent_to_city
                    @sent_to_city = false
                    total_lost = 0
                    for k, v of @units
                        if Math.random() > 0.5
                            delete @units[k]
                            total_lost++
                    log.debug "you lost #{total_lost} units"
                    @experience = ((~~@experience) + 100) + ""
                    dest = @scene.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size)[0]
                    @goTo(dest)

        addUnit: (name, troop) ->
            @units[name] = troop

        addInventory: (type, inventory) ->
            @inventory[type] = inventory
            log.debug "added inventory #{type}"
            log.debug inventory

        getExpPercentage: () ->
            return (@experience / @next_level_in) * 100


    return Commander