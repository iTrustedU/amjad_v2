(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/WalkableTile", "../../amjad/TileNotification"], function(WalkableTile, TileNotification) {
    var Commander, _ref;
    Commander = (function(_super) {
      __extends(Commander, _super);

      function Commander() {
        _ref = Commander.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      Commander.prototype.init = function() {
        var _this = this;
        Commander.__super__.init.call(this);
        this.name = this.meta.attr.name;
        this.experience = this.meta.attr.experience;
        this.speed = this.meta.attr.speed;
        this.leadership = this.meta.attr.leadership;
        this.readiness = this.meta.attr.readiness;
        this.next_level_in = 1000;
        this.inventory = {
          "head": null,
          "neck": null,
          "back": null,
          "torso": null,
          "r-hand": null,
          "l-hand": null,
          "l-ring": null,
          "r-ring": null,
          "legs": null,
          "boots": null
        };
        this.units = {};
        this.exp_level = 1;
        this.sent_to_city = false;
        this.max_vel = 4;
        this.commander_selected = false;
        this.on("SELECTED_DBL_CLICK", function() {
          var x, y;
          x = this.pos[0] + this.scene.camera.pos[0] * this.scene.camera.zoom;
          y = this.pos[1] + this.scene.camera.pos[1] * this.scene.camera.zoom - this.h;
          Hal.trigger("SHOW_NOTIFICATION", {
            text: this.name,
            x: x,
            y: y
          });
          return Hal.trigger("OPEN_COMMANDER_DIALOG", this);
        });
        this.on("SELECTED_CLICK", function() {
          return this.commander_selected = true;
        });
        this.scene.on("LAYER_SELECTED", function(layer) {
          var destination;
          if (_this.commander_selected && !layer.equals(_this)) {
            destination = layer.parent_tile;
            _this.commander_selected = false;
            destination = null;
            if (layer.meta.attr.type === "empire_building_civil") {
              destination = _this.scene.getSpanBorders(layer.parent_tile, layer.meta.size)[0];
              _this.sent_to_city = true;
            } else {
              destination = layer.parent_tile;
              _this.sent_to_city = false;
            }
            if (destination == null) {
              return;
            }
            log.debug("destination----");
            log.debug(destination);
            log.debug("---------------");
            return _this.goTo(destination);
          }
        });
        return this.on("FINISHED_WALKING", function(tile) {
          var dest, k, total_lost, v, _ref1;
          if (this.sent_to_city) {
            this.sent_to_city = false;
            total_lost = 0;
            _ref1 = this.units;
            for (k in _ref1) {
              v = _ref1[k];
              if (Math.random() > 0.5) {
                delete this.units[k];
                total_lost++;
              }
            }
            log.debug("you lost " + total_lost + " units");
            this.experience = ((~~this.experience) + 100) + "";
            dest = this.scene.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size)[0];
            return this.goTo(dest);
          }
        });
      };

      Commander.prototype.addUnit = function(name, troop) {
        return this.units[name] = troop;
      };

      Commander.prototype.addInventory = function(type, inventory) {
        this.inventory[type] = inventory;
        log.debug("added inventory " + type);
        return log.debug(inventory);
      };

      Commander.prototype.getExpPercentage = function() {
        return (this.experience / this.next_level_in) * 100;
      };

      return Commander;

    })(WalkableTile);
    return Commander;
  });

}).call(this);
