(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/WalkableTile"], function(WalkableTile) {
    var CaravanLeader, _ref;
    CaravanLeader = (function(_super) {
      __extends(CaravanLeader, _super);

      function CaravanLeader() {
        _ref = CaravanLeader.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      CaravanLeader.prototype.init = function() {
        var _this = this;
        CaravanLeader.__super__.init.call(this);
        this.name = this.meta.attr.name;
        this.max_weight = 100;
        this.weight = 0;
        this.money = 0;
        this.experience = 0;
        this.next_level_in = 1000;
        this.goods = {};
        this.exp_level = 1;
        this.sent_to_poi = false;
        this.max_vel = ~~this.meta.attr.velocity;
        this.caravan_selected = false;
        this.on("SELECTED_CLICK", function() {
          return this.caravan_selected = true;
        });
        this.scene.on("LAYER_SELECTED", function(layer) {
          var destination;
          if (_this.caravan_selected && !layer.equals(_this)) {
            destination = layer.parent_tile;
            _this.caravan_selected = false;
            destination = null;
            if (layer.name === "buildings_trade_poi") {
              destination = _this.scene.getSpanBorders(layer.parent_tile, layer.meta.size)[0];
              _this.sent_to_city = true;
            } else {
              destination = layer.parent_tile;
              _this.sent_to_city = false;
            }
            if (destination == null) {
              return;
            }
            return _this.goTo(destination);
          }
        });
        this.on("FINISHED_WALKING", function(tile) {
          var dest;
          if (this.sent_to_city) {
            this.sent_to_city = false;
            this.money += 100;
            this.experience = (~~this.experience + 100) + "";
            amjad.empire.addMoney(100);
            dest = this.scene.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size)[0];
            return this.goTo(dest);
          }
        });
        return this.on("SELECTED_DBL_CLICK", function() {
          var x, y;
          x = this.pos[0] + this.scene.camera.pos[0] * this.scene.camera.zoom;
          y = this.pos[1] + this.scene.camera.pos[1] * this.scene.camera.zoom - this.h;
          Hal.trigger("SHOW_NOTIFICATION", {
            text: this.name,
            x: x,
            y: y
          });
          return Hal.trigger("OPEN_CARAVAN_DIALOG", this);
        });
      };

      CaravanLeader.prototype.getExpPercentage = function() {
        return (this.experience / this.next_level_in) * 100;
      };

      return CaravanLeader;

    })(WalkableTile);
    return CaravanLeader;
  });

}).call(this);
