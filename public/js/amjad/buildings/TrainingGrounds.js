(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/TileLayer"], function(TileLayer) {
    var TrainingGrounds, _ref;
    TrainingGrounds = (function(_super) {
      __extends(TrainingGrounds, _super);

      function TrainingGrounds() {
        _ref = TrainingGrounds.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      TrainingGrounds.prototype.init = function() {
        this.trainers = {};
        this.troops = {};
        this.amount = {};
        TrainingGrounds.__super__.init.call(this);
        this.scene.trigger("TRAINING_GROUNDS_ADDED", this);
        return this.on("SELECTED_CLICK", function() {
          return Hal.trigger("OPEN_TRAINING_GROUNDS_DIALOG", this);
        });
      };

      TrainingGrounds.prototype.putTrainer = function(trainer) {
        return this.trainers[trainer.name] = trainer;
      };

      TrainingGrounds.prototype.addTroop = function(troop) {
        this.troops[troop.name] = troop;
        if (!this.amount[troop.name]) {
          this.amount[troop.name] = 0;
        }
        this.amount[troop.name]++;
        amjad.empire.takeMoney(+troop.price);
        return log.debug("added troop " + troop.name);
      };

      TrainingGrounds.prototype.employTroop = function(troop) {
        if (this.troops[troop.name] != null) {
          if (this.amount[troop.name] > 0) {
            this.amount[troop.name]--;
          }
          if (this.amount[troop.name] === 0) {
            log.debug("nemas vise trupa " + troop.name);
            return delete this.troops[troop.name];
          }
        }
      };

      return TrainingGrounds;

    })(TileLayer);
    return TrainingGrounds;
  });

}).call(this);
