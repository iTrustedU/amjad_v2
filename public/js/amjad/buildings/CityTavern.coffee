"use strict"

define ["../../amjad/TileLayer"],

(TileLayer) ->
    
    class CityTavern extends TileLayer
        init: () ->
            @Heroes = 
                commander: amjad.city.tmngr.getTilesByType("character", "commander")
                caravan: amjad.city.tmngr.getTilesByType("character", "caravan")
                trainer:                 
                    "Jahar":
                        name: "Jahar"
                        brawn: 2
                        reflexes: 5
                        endurance: 4
                        experience: 7
                        level: 1
                        price: 2000
                        type: "trainer"
                    "Jabar":
                        name: "Jabar"
                        brawn: 2
                        reflexes: 3
                        endurance: 1
                        experience: 9
                        level: 1
                        price: 2400
                        type: "trainer"

            super()

            @scene.trigger "TAVERN_ADDED", @

            @on "SELECTED_CLICK", () ->
                Hal.trigger "OPEN_TAVERN_DIALOG", @
        
        getHeroes: (type) ->
            return @Heroes[type]

        removeHero: (herometa) ->
            log.debug "removing hero from tavern"
            log.debug herometa
            if herometa.type is "trainer"
                delete @Heroes[herometa.type][herometa.name]
            else
                delete @Heroes[herometa.attr.type][herometa.name]

    return CityTavern