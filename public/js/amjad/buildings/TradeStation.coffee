"use strict"

define ["../../amjad/TileLayer"],

(TileLayer) ->

    class TradeStation extends TileLayer
        init: () ->
            @traders =
                name: "Mustafa"
                    level: 4
                    goods: 
                        "Dates":
                            sprite: "trade-goods/dates"
                            price: 50
                            weight: 10
                            description: "Dates"

                name: "GoodTrader"
                    level: 2
                    goods:
                        "Silk":
                            sprite: "trade-goods/silk"
                            price: 150
                            weight: 5
                            description: "Silk"
                            
            super()
            
            @scene.trigger "TRADE_STATION_ADDED", @
            
            @on "SELECTED_CLICK", () ->
                Hal.trigger "OPEN_TRADE_STATION_DIALOG", @

    return TradeStation
