(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/TileLayer"], function(TileLayer) {
    var WarTent, _ref;
    WarTent = (function(_super) {
      __extends(WarTent, _super);

      function WarTent() {
        _ref = WarTent.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      WarTent.prototype.init = function() {
        this.commanders = {};
        this.caravans = {};
        this.trainers = {};
        this.ngs = amjad.zone.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size);
        this.surrounded_cnt = 0;
        WarTent.__super__.init.call(this);
        this.scene.trigger("WARTENT_ADDED", this);
        return this.on("SELECTED_CLICK", function() {
          return Hal.trigger("OPEN_WARTENT_DIALOG", this);
        });
      };

      WarTent.prototype.getCommanders = function() {
        return this.commanders;
      };

      WarTent.prototype.putCommander = function(hero) {
        var level, t;
        log.debug("putCommander called");
        level = amjad.zone.layers[hero.level];
        t = amjad.empire.City.parent_tile;
        t = this.ngs[0];
        t = t.addLayer(hero.name, [0, 0], false);
        if (t != null) {
          log.debug("added commander " + hero.name);
          this.commanders[hero.name] = t;
          this.ngs = this.ngs.filter(function(x) {
            return x.layers[3] === null;
          });
          return t;
        }
      };

      WarTent.prototype.putCaravan = function(hero) {
        var level, t;
        log.debug("putCaravan called");
        level = amjad.zone.layers[hero.level];
        t = amjad.empire.City.parent_tile;
        t = this.ngs[0];
        log.debug(t);
        t = t.addLayer(hero.name, [0, 0], false);
        if (t != null) {
          log.debug("added commander " + hero.name);
          this.caravans[hero.name] = t;
          this.ngs = this.ngs.filter(function(x) {
            return x.layers[3] === null;
          });
          return t;
        }
      };

      WarTent.prototype.putTrainer = function(trainer) {
        return log.debug("putTrainer not implemented");
      };

      return WarTent;

    })(TileLayer);
    return WarTent;
  });

}).call(this);
