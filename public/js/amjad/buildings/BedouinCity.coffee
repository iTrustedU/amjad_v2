"use strict"


define ["../../amjad/TileLayer"], 

(TileLayer) ->

    class BedouinCity extends TileLayer    
        init: () ->

            @on "SELECTED_CLICK", () =>
                amjad.enterCityView()

            @empire_owner = 15

            if amjad?
                if @empire_owner is amjad.empire.id
                    log.debug "it is my city...."
                    amjad.empire.setCity(@)

            super()

    return BedouinCity

