"use strict"

define ["../../amjad/TileLayer"],

(TileLayer) ->

    class TrainingGrounds extends TileLayer
        init: () ->
            @trainers = {}
            @troops = {}
            @amount = {}
            super()
            
            @scene.trigger "TRAINING_GROUNDS_ADDED", @
            
            @on "SELECTED_CLICK", () ->
                Hal.trigger "OPEN_TRAINING_GROUNDS_DIALOG", @

        putTrainer: (trainer) ->
            @trainers[trainer.name] = trainer

        addTroop: (troop) ->
            @troops[troop.name] = troop
            if not @amount[troop.name]
                @amount[troop.name] = 0
            @amount[troop.name]++
            amjad.empire.takeMoney(+troop.price)
            #amjad.empire.takeGold(+troop.gold)
            #amjad.empire.takeWood(+troop.wood)
            #amjad.empire.takeIron(+troop.iron)
            log.debug "added troop #{troop.name}"

        employTroop: (troop) ->
            if @troops[troop.name]?
                if @amount[troop.name] > 0
                    @amount[troop.name]--
                if @amount[troop.name] is 0
                    log.debug "nemas vise trupa #{troop.name}"
                    delete @troops[troop.name]

    return TrainingGrounds
