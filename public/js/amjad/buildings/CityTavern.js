(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/TileLayer"], function(TileLayer) {
    var CityTavern, _ref;
    CityTavern = (function(_super) {
      __extends(CityTavern, _super);

      function CityTavern() {
        _ref = CityTavern.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      CityTavern.prototype.init = function() {
        this.Heroes = {
          commander: amjad.city.tmngr.getTilesByType("character", "commander"),
          caravan: amjad.city.tmngr.getTilesByType("character", "caravan"),
          trainer: {
            "Jahar": {
              name: "Jahar",
              brawn: 2,
              reflexes: 5,
              endurance: 4,
              experience: 7,
              level: 1,
              price: 2000,
              type: "trainer"
            },
            "Jabar": {
              name: "Jabar",
              brawn: 2,
              reflexes: 3,
              endurance: 1,
              experience: 9,
              level: 1,
              price: 2400,
              type: "trainer"
            }
          }
        };
        CityTavern.__super__.init.call(this);
        this.scene.trigger("TAVERN_ADDED", this);
        return this.on("SELECTED_CLICK", function() {
          return Hal.trigger("OPEN_TAVERN_DIALOG", this);
        });
      };

      CityTavern.prototype.getHeroes = function(type) {
        return this.Heroes[type];
      };

      CityTavern.prototype.removeHero = function(herometa) {
        log.debug("removing hero from tavern");
        log.debug(herometa);
        if (herometa.type === "trainer") {
          return delete this.Heroes[herometa.type][herometa.name];
        } else {
          return delete this.Heroes[herometa.attr.type][herometa.name];
        }
      };

      return CityTavern;

    })(TileLayer);
    return CityTavern;
  });

}).call(this);
