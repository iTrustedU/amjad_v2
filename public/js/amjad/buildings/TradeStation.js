(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/TileLayer"], function(TileLayer) {
    var TradeStation, _ref;
    TradeStation = (function(_super) {
      __extends(TradeStation, _super);

      function TradeStation() {
        _ref = TradeStation.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      TradeStation.prototype.init = function() {
        this.traders = {
          name: "Mustafa"
        };
        this.ngs = amjad.zone.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size);
        TradeStation.__super__.init.call(this);
        this.scene.trigger("TRADE_STATION_ADDED", this);
        return this.on("SELECTED_CLICK", function() {
          return Hal.trigger("OPEN_TRADE_STATION_DIALOG", this);
        });
      };

      return TradeStation;

    })(TileLayer);
    return TradeStation;
  });

}).call(this);
