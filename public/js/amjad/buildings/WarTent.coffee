"use strict"

define ["../../amjad/TileLayer"],

(TileLayer) ->

    class WarTent extends TileLayer
        init: () ->
            @commanders = {}
            @caravans = {}
            @trainers = {}
            @ngs = amjad.zone.getSpanBorders(amjad.empire.City.parent_tile, amjad.empire.City.meta.size)
            @surrounded_cnt = 0
            super()
            
            @scene.trigger "WARTENT_ADDED", @
            
            @on "SELECTED_CLICK", () ->
                Hal.trigger "OPEN_WARTENT_DIALOG", @

        getCommanders: () ->
            return @commanders

        putCommander: (hero) ->
            log.debug "putCommander called"
            level = amjad.zone.layers[hero.level]
            t = amjad.empire.City.parent_tile
            t = @ngs[0]
            t = t.addLayer(hero.name, [0,0], false)

            if t? 
                log.debug "added commander #{hero.name}"
                @commanders[hero.name] = t
                @ngs = @ngs.filter (x) -> return (x.layers[3] is null)
                return t

        putCaravan: (hero) ->
            log.debug "putCaravan called"   
            level = amjad.zone.layers[hero.level]
            t = amjad.empire.City.parent_tile
            t = @ngs[0]
            log.debug t
            t = t.addLayer(hero.name, [0,0], false)
            if t? 
                log.debug "added commander #{hero.name}"
                @caravans[hero.name] = t
                @ngs = @ngs.filter (x) -> return (x.layers[3] is null)
                return t

        putTrainer: (trainer) ->
            log.debug "putTrainer not implemented"
            #@commanders[hero.name] = amjad.putTileNearOwnCity(hero)

            # ngs = amjad.zone.getReachableNeighbours(t)
            # if ngs? and ngs.length > 0
            #     t = ngs[amjacity_surrounded_counter]
            #     t = t.addLayer(hero.name, [0,0], false)
            #     log.debug "dodao heroja"
            #     @city_surrounded_counter++
            #     #t.visible = false
            #     return t
            # return null
            #return @heroes[hero.attr.type][hero.name] = amjad.putHeroOnZoneNearCity(hero)

    return WarTent
