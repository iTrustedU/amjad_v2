(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../../amjad/TileLayer"], function(TileLayer) {
    var BedouinCity, _ref;
    BedouinCity = (function(_super) {
      __extends(BedouinCity, _super);

      function BedouinCity() {
        _ref = BedouinCity.__super__.constructor.apply(this, arguments);
        return _ref;
      }

      BedouinCity.prototype.init = function() {
        var _this = this;
        this.on("SELECTED_CLICK", function() {
          return amjad.enterCityView();
        });
        this.empire_owner = 15;
        if (typeof amjad !== "undefined" && amjad !== null) {
          if (this.empire_owner === amjad.empire.id) {
            log.debug("it is my city....");
            amjad.empire.setCity(this);
          }
        }
        return BedouinCity.__super__.init.call(this);
      };

      return BedouinCity;

    })(TileLayer);
    return BedouinCity;
  });

}).call(this);
