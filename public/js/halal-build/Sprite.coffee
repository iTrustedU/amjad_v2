"use strict"

define () ->

    class Sprite
        constructor: (@img, @name, @x, @y, @w, @h) ->
            spl = @img.src.match(/\/assets\/sprites\/(.*\/)(.*)\.png/)
            @name = spl[2]
            @folder = spl[1]
    return Sprite
