"use strict"

define ["Vec2"],

(Vec2) ->

    class Camera
        constructor: (@ctx, @pos, @w, @h, @scene) ->
            @dragging = false
            @start_drag_point = [0,0]
            @camera_prev_pos = [@pos[0], @pos[1]]

            @zoom = 1
            @zoom_step = 0.1
            @camera_speed = 3.8
            @bounds = [0, 0, @w, @h]
            @center_point = [@scene.bounds[2] / 2, @scene.bounds[3] / 2]

            @lerp_to = @center_point.slice()

            @view_frustum = [0, 0, @scene.bounds[2], @scene.bounds[3]]

            @scene.on "ENTER_FULLSCREEN", (scale) =>
                @zoom = scale[0]
                @bounds = [0, 0, @w * @zoom, @h * @zoom]
                @center_point = [@scene.bounds[2] / 2, @scene.bounds[3] / 2]
                log.info @center_point

            @scene.on "EXIT_FULLSCREEN", (scale) =>
                @zoom = scale[0]
                @bounds = [0, 0, @w * @zoom, @h * @zoom]
                @center_point = [@scene.bounds[2] / 2, @scene.bounds[3] / 2]

        enableDrag: () ->
            @drag_started = 
            Hal.on "DRAG_STARTED", (pos) =>
                return if @scene.paused
                @dragging = true
                @start_drag_point = pos.slice()
                @camera_prev_pos = @pos.slice()

            @drag_ended = 
            Hal.on "DRAG_ENDED", (pos) =>
                @dragging = false

            @drag = 
            Hal.on "MOUSE_MOVE", (pos) =>
                return if @scene.paused
                if @dragging
                    @pos[0] = (@camera_prev_pos[0] + (pos[0] - @start_drag_point[0]))
                    @pos[1] = (@camera_prev_pos[1] + (pos[1] - @start_drag_point[1]))
                    @scene.trigger "CAMERA_MOVED", @pos

        isVisible: (ent) ->
            return (
                Hal.m.rectIntersectsRect([ent.pos[0] * @zoom + @pos[0], ent.pos[1] * @zoom + @pos[1], ent.bounds[2], ent.bounds[3]], @bounds)
            )

        enableZoom: () ->
            @zoom_trig = 
            Hal.on "SCROLL", (ev) =>
                return if @scene.paused

                if @scene.paused
                    return
                if ev.down
                    @zoom -= @zoom_step
                else
                    @zoom += @zoom_step

                

                @scene.trigger "CAMERA_MOVED", @pos
                #@pos[0] += @center_point[0] / @zoom
                #@pos[1] += @center_point[1] / @zoom
                @scene.trigger "ZOOM", @zoom
                #@pos[0] -= @center_point[0] / @zoom
                #@pos[1] -= @center_point[1] / @zoom
                #@lerpTo([@center_point[0] * @zoom, @center_point[1] * @zoom])

        setViewFrustum: (bnds) ->
            @view_frustum[0] = -bnds[0]
            @view_frustum[1] = -bnds[1]
            @view_frustum[2] = -bnds[2]
            @view_frustum[3] = -bnds[3]
            log.debug "view frustum setted"
            log.debug @view_frustum

        enableArrowKeys: () ->
            @arrkeys = 
            Hal.on "KEY_DOWN", (ev) =>
                if ev.keyCode == Hal.Keys.LEFT
                    @lerpTo([@center_point[0] - @camera_speed * 50, @center_point[1]])
                else if ev.keyCode == Hal.Keys.RIGHT
                    @lerpTo([@center_point[0] + @camera_speed * 50, @center_point[1]])
                else if ev.keyCode == Hal.Keys.UP
                    @lerpTo([@center_point[0], @center_point[1] - @camera_speed * 50])
                else if ev.keyCode == Hal.Keys.DOWN
                    @lerpTo([@center_point[0], @center_point[1] + @camera_speed * 50])

        disableArrowKeys: () ->
            Hal.removeTrigger "KEY_DOWN", @arrkeys

        enableLerp: () ->
            @lerpTo = (pos) ->
                return if @scene.paused
                lx = (@center_point[0] - pos[0] + @pos[0])
                ly = (@center_point[1] - pos[1] + @pos[1])
                if lx > @view_frustum[0] * @zoom
                    lx = @view_frustum[0] * @zoom
                if (lx - @bounds[2]) < @view_frustum[2] * @zoom
                    lx = @view_frustum[2] * @zoom + @bounds[2]

                if ly > @view_frustum[1] * @zoom 
                    ly = @view_frustum[1] * @zoom
                if (ly - @bounds[3] < @view_frustum[3] * @zoom)
                    ly = @view_frustum[3] * @zoom + @bounds[3] 

                @lerp_to[0] = lx
                @lerp_to[1] = ly

                if not @lerp_anim
                    @lerp_anim = Hal.on "ENTER_FRAME", (delta) =>
                        if (~~Math.abs(@pos[0] - @lerp_to[0]) + ~~Math.abs(-@pos[1] + @lerp_to[1])) < 2
                            Hal.removeTrigger "ENTER_FRAME", @lerp_anim
                            @lerp_anim = null
                        else
                            Vec2.lerp(@pos,  @pos.slice(), @lerp_to, delta / 1000 * @camera_speed)
                            @scene.trigger "CAMERA_MOVED", @pos

        lerpTo: () -> return

        disableLerp: () ->
            @lerpTo = () -> return
            
        disableZoom: () ->
            Hal.removeTrigger "SCROLL", @zoom_trig

        disableDrag: () ->
            Hal.removeTrigger "DRAG_STARTED", @drag_started
            Hal.removeTrigger "DRAG_ENDED", @drag_ended
            Hal.removeTrigger "MOUSE_MOVE", @drag

    return Camera