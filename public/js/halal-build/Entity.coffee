"use strict"

define ["EventDispatcher"], 

(EventDispatcher) ->

    class Entity extends EventDispatcher
        constructor: (@pos, @bounds) ->
            @scene      = null
            @g          = null
            @in_focus   = false
            @id         = Hal.UID()
            @quadspace  = null
            super()

        init: () ->
            return

    Entity::inBounds = (point) ->
        return Hal.m.isPointInRect(point, @bounds)

    return Entity
    