"use strict"

define ["Scene"], 

(Scene) ->

    class SceneManager
        constructor: () ->
            @scenes = []
            @max_depth = 0
            @top_scene = null
            @fscale     = [1, 1]

            Hal.on "ENGINE_PAUSED", () =>
                @pauseAll()
            
            Hal.on "ENGINE_RESUMED", () =>
                @unpauseAll()

            Hal.on "MOUSE_MOVE", (pos) =>
                pos[0] = (pos[0] / @fscale[0])
                pos[1] = (pos[1] / @fscale[1])
                
                for scene in @scenes
                    scene.in_focus[0] = false
                    if not scene.paused and scene.inBounds(pos)
                        high = scene

                if not high?
                    return

                @top_scene = high
                @top_scene.in_focus[0] = true
                @top_scene.mpos[0] = ((pos[0] - @top_scene.x) - @top_scene.camera.pos[0]) / @top_scene.camera.zoom
                @top_scene.mpos[1] = ((pos[1] - @top_scene.y) - @top_scene.camera.pos[1]) / @top_scene.camera.zoom
                
                @top_scene.trigger "MOUSE_MOVE", @top_scene.mpos

            Hal.on "RESIZE", (scale) =>
                for scene in @scenes
                    scene.trigger "RESIZE", scale

            Hal.on "SHOW_NEXT_SCENE", () =>
                @nextScene()

            Hal.on "SHOW_PREV_SCENE", () =>
                return

        nextScene: () ->
            ind = @scenes.indexOf(@top_scene)
            @top_scene.pause()
            @top_scene = @scenes[(ind+1) % @scenes.length]
            @top_scene.resume()

        enterFullScreen: (@fscale) ->
            Hal.trigger "ENTER_FULLSCREEN"
            for sc in @scenes
                sc.trigger "ENTER_FULLSCREEN", @fscale
                
        exitFullScreen: (@fscale) ->
            Hal.trigger "EXIT_FULLSCREEN"
            for sc in @scenes
                sc.trigger "EXIT_FULLSCREEN", @fscale

        addSceneOnTop: (sceneA, sceneB) ->
            i = @scenes.indexOf(sceneA) 
            return if i == -1
            depth = @scenes[i].depth
            zFrom = @scenes[i].g.canvas.style["z-index"]

            for scene in @findTopScenes(depth)
                z = scene.g.canvas.style["z-index"]
                scene.g.canvas.style["z-index"] = +z + 1
                scene.depth++

            sceneB.depth = depth + 1
            sceneB.g.canvas.style["z-index"] = +zFrom + 1
            @addScene(sceneB)

        getSceneByName: (name) ->
            for scene in @scenes
                if scene.name == name
                    return scene
            return null

        putOnTop: (scene) ->
            @sortScenes()
            if not @top_scene? || scene.depth == @max_depth
                return
            scene.depth = @max_depth + 1
            @pauseAll()
            scene.resume()
            @sortScenes()

        findTopScenes: (fromDepth) ->
            out = []
            for scene in @scenes
                if scene.depth > fromDepth
                    out.push(scene)
            return out

        findBelowScenes: (fromDepth) ->
            out = []
            for scene in @scenes
                if scene.depth < fromDepth
                    out.push(scene)
            return out

        addScene: (scene) ->
            if not (scene instanceof Scene)
                return null

            if not scene.name
                scene.name = "#scene" + "_" + scene.id

            if not scene.bounds
                scene.bounds = [
                    0,
                    0,
                    Hal.dom.renderspaceRect.width,
                    Hal.dom.renderspaceRect.height
                ]

            if not scene.depth
                scene.depth = @max_depth + 1

            scene.on "SCENE_PAUSED", (x) => return

            scene.on "SCENE_RESUMED", (x) => return

            scene.init()
            Hal.trigger "SCENE_ADDED_" + scene.name.toUpperCase(), scene
            
            @pauseAll()
            @scenes.unshift(scene)
            @sortScenes()

            log.debug "added scene: #{scene.name}"
            return scene
        
        removeScene: (scene) ->
            scene.removeAllTriggers()
            ind = @scenes.indexOf(scene)
            if ind isnt -1
                @scenes.splice(ind, 1)
                log.debug "removed scene #{scene.name}"
            else 
                log.debug "couldn't remove scene #{scene.name}"
                
        sortScenes: () ->
            @scenes.sort (el1,el2) ->
                return el1.depth > el2.depth
            @top_scene = @scenes[@scenes.length - 1]
            @max_depth = @top_scene.depth

        pauseAll: () ->
            log.debug "paused all"
            scene.pause() for scene in @scenes

        unpauseAll: () ->
            log.debug "resumed all"
            scene.resume() for scene in @scenes

    return SceneManager