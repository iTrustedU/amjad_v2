"use strict"

define ["jquery-ui"], 
($) ->
  class MapLoaderBar
    constructor: (@map, @clb) ->
      $prbar = $('.floating-bar')
      $prbartext = $("#floating-bar-text")
      @starttime = 0

      @maploadstartlist = 
        @map.on "MAP_LOADING_STARTED", (len) =>
          $prbartext.text("Loading map: #{@map.name}")
          log.debug "started"
          @starttime = performance.now()
          $prbar.show()

      @maploadedlist = 
        @map.on "MAP_LOADED", () =>
            log.debug "finished"
            log.debug "it took " + (performance.now() - @starttime) / 1000 + "s"
            $prbar.fadeOut()
            @clb() if @clb?
          

  return MapLoaderBar