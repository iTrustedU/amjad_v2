(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  require(["Halal", "../amjad/AmjadMap", "Scene", "entities/RectBoundedEntity", "entities/WalkableEntity", "../amjad/AmjadMapEditor"], function(Halal, AmjadMap, Scene, RectBoundedEntity, WalkableEntity, AmjadMapEditor) {
    var Scenica;
    Scenica = (function(_super) {
      __extends(Scenica, _super);

      function Scenica() {
        var n, x, y, _i;
        Scenica.__super__.constructor.call(this, "scenica", Hal.r.bounds, false);
        this.line = [[50, 50], [328, 312]];
        this.range = [50, 50];
        this.resetQuadSpace(this.bounds);
        for (n = _i = 0; _i < 50; n = ++_i) {
          x = Math.random() * this.bounds[2];
          y = Math.random() * this.bounds[3];
        }
      }

      Scenica.prototype.init = function() {
        var _this = this;
        Hal.on("LEFT_CLICK", function(pos) {
          var e, ents, t, t1, _i, _len;
          t = performance.now();
          ents = _this.quadspace.searchInRange([_this.mpos[0] - _this.range[0], _this.mpos[1] - _this.range[1], 2 * _this.range[0], 2 * _this.range[1]]);
          t1 = performance.now() - t;
          for (_i = 0, _len = ents.length; _i < _len; _i++) {
            e = ents[_i];
            e.trigger("LEFT_CLICK", _this.mpos);
          }
          console.log(t1);
          return console.log(ents.length);
        });
        Hal.on("MOUSE_DBL_CLICK", function(pos) {
          var en;
          if (!Hal.m.isPointInRect(pos, _this.bounds)) {
            return;
          }
          en = new WalkableEntity([pos[0], pos[1]], "test/warhorse");
          _this.addEntity(en);
          return _this.quadspace.insert(en);
        });
        return Hal.on("RIGHT_CLICK", function(pos) {
          return _this.camera.lerpTo(pos);
        });
      };

      Scenica.prototype.update = function(delta) {
        var en, _i, _len, _ref;
        Scenica.__super__.update.call(this, delta);
        _ref = this.ents;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          en = _ref[_i];
          en.update(delta);
        }
        this.g.ctx.strokeStyle = "blue";
        if (this.mpos != null) {
          this.g.ctx.strokeStyle = "blue";
          this.g.ctx.strokeRect(this.mpos[0] - this.range[0], this.mpos[1] - this.range[1], this.range[0] * 2, this.range[1] * 2);
          return this.g.ctx.stroke();
        }
      };

      return Scenica;

    })(Scene);
    return $(document).ready(function() {
      return require(["MapEditor"], function(MapEditor) {
        Hal.on("ENGINE_STARTED", function() {
          var scenica, scenica2;
          scenica = new AmjadMap({
            "name": "zone_map",
            "numrows": 50,
            "numcols": 50,
            "tiledim": {
              width: 128,
              height: 64
            }
          });
          scenica.pause();
          scenica2 = new AmjadMapEditor({
            "name": "amjad_editor",
            "numrows": 50,
            "numcols": 50,
            "tiledim": {
              width: 128,
              height: 64
            }
          });
          return Hal.scm.addScene(scenica2);
        });
      });
    });
  });

}).call(this);
