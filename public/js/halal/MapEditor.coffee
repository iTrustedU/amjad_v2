"use strict"

define ["jquery-ui", "FullscreenButton", "AssetLoaderBar", "../amjad/AmjadMapEditor", "MapLoaderBar"], 

($, FullscreenButton, AssetLoaderBar, AmjadMapEditor, MapLoaderBar) ->

  extract_int = /^(\d+)%$/
  
  socket              = io.connect('http://localhost:8080')
  $props_container    = $("#entity-properties-container")
  $ent_container      = $("#entity-container")
  $anim_container     = $("#animation-container")
  $sprites_container  = $("#sprites-container")
  $sheet_container    = $("#spritesheets-container")
  $all_containers     = $("ol[id$='-container']")
  $ents_container     = $('#list-container')
  $ent_save_btn       = $('#entity-save')
  $ent_new_btn        = $('#entity-new')
  $tile_list          = $('#tiles-list')
  $tile_group         = $('.tile-group')
  $pattern_group      = $('.pattern-group')
  $map_list           = $('#map-list')
  $pattern_list       = $('#pattern-list')
  $map_group          = $('.map-group')
  $map_list_box       = $('<select/>', {
      "size": 3,
      "class": '.map-list'
  })
  $map_save           = $('#map-save')
  $map_load           = $('#map-load')
  $map_new            = $("#map-new")

  $new_prop_btn       = $('#new-prop-btn')
  $new_list_btn       = $('#new-list-btn')
  amjad               = null
  amjad_tile          = null

  Hal.start()
  progressbar         = new AssetLoaderBar(() ->
    showNewMapDialog(false)
  )
  Hal.asm.loadViaSocketIO()

  Maps = {}
  TileLayers = 
    terrain: []
    decor: []
    buildings: []
    poi: []
    character: []
  Patterns = {}

  $.getJSON "assets/amjad/TilesList.json", (data) ->
    log.info data
    for k, tile of data
      TileLayers[tile.level][tile.name] = tile

  $.getJSON "assets/amjad/Patterns.json", (data) ->
    for k, pattern of data
      Patterns[k] = pattern

  #Hal.on "SCENE_ADDED_AMJAD_EDITOR", (sc) ->
  #    amjad = sc;

  Hal.on "NEW_PATTERN_CREATED", (pattern) ->
    log.debug "new pattern created"
    log.debug pattern
    out = []
    pivot = pattern["pivot"].serialize()
    out.push(pivot)
    for k, v of pattern
      if k is "pivot"
        continue
      pt = v.serialize()
      nums = k.split("_")
      pt.off = [+nums[0], +nums[1]]
      out.push(pt)

    showSavePatternDialog(JSON.stringify(out))

  Hal.asm.on "SPRITES_LOADED", (spr) ->
    log.debug "wtfasss"
    showFolders()

  socket.on "TILE_ADDED", (tile) ->
    log.info "tile loaded..."
    log.info tile
    TileLayers[tile.level][tile.name] = tile
    Hal.trigger "TILE_ADDED", tile
    log.info TileLayers
    $ents_container.empty()
    $("#entities-#{tile.level}").click()

  socket.on "PATTERN_ADDED", (data) ->
    Patterns[data.name] = JSON.parse(data.pattern)
    $pattern_list.click()

  socket.on "TILE_DELETED", (tile) ->
    delete TileLayers[tile.level][tile.name]
    $("#entities-#{tile.level}").click()

  $ent_save_btn.click () ->
    tile = saveCurrentTile()

    #if $ent_save_btn.text() is "Save [InMap]"
    #  amjad_tile.trigger "UPDATE_META", tile 

    log.debug tile
    jsonstr = JSON.stringify(tile)
    socket.emit "NEW_TILE", tile: jsonstr
    log.info tile

  $tile_list.click () ->
      $ents_container.empty()
      $map_group.hide()
      $tile_group.show()
      $ents_container.slideDown()

  $pattern_list.click () ->
    $ents_container.empty()  
    $map_group.hide()
    $tile_group.hide()
    $pattern_group.show()
    $ents_container.slideDown()
    showPatterns()

  $('#entities-terrain,#entities-decor,#entities-buildings,#entities-poi,#entities-character').click () ->
    $(@).parent().parent().find("a").removeClass("active")
    $(@).addClass("active")
    $tile_group.show()
    $ents_container.empty()
    log.info @
    id = $(@).attr('id')
    id = id.substr(id.indexOf('-') + 1)
    for k, tile of TileLayers[id]
      log.info tile
      $ents_container.append(createTileWrapper(tile, id))

  showPatterns = () ->
    for k, v of Patterns
      $ents_container.append(createPatternWrapper(k, v))

  createPatternWrapper = (name, pattern) ->
    $li = createLi(name, name)
    img = $(Hal.asm.getSprite("editor/pattern").img).clone()
    $title = $("<div/>")
    $title.addClass("selectable-title")
    $title.text(name)

    $li.append(img)
    $li.append($title)
    $li.contextPopup(
      "title": name
      "items": [
        {
          "label": "Remove pattern"
          "action": () ->
            socket.emit "DELETE_PATTERN", {name: name}
        }
      ]
    )
    $li.attr("id", name)
    $li.click do (name) ->
      return () ->
        amjad.trigger "PATTERN_SELECTED", pattern
    $li.fadeIn()
    return $li

  createTileWrapper = (tile, id) ->
    $li = createLi(tile.sprite, tile.name)

    img = $(Hal.asm.getSprite(tile.sprite).img).clone()

    $title = $("<div/>")
    $title.addClass("selectable-title")
    $title.text(tile.name)

    $li.append(img)
    $li.append($title)
    $li.contextPopup(
      "title": tile.name
      "items": [
        {
          "label": "Remove tile"
          "action": () ->
            socket.emit "DELETE_TILE", {name: tile.name, level: tile.level}
        }
      ]
    )
    $li.attr("id", tile.sprite)
    $li.click do (id, tile) ->
      return () ->
        amjad.trigger "TILELAYER_SELECTED", tile
        loadTileFromMeta({tile:tile, spr: $(@)})
    $li.fadeIn()
    return $li

  saveCurrentTile = () ->
    lvl = $ent_container.find("#level").find("select > option:selected").text()
    tile = 
      level: lvl
      sprite: $ent_container.data("sprite_name")
      name: lvl + "_" + $ent_container.find("#name").find("input").val()
      size: parseMiniGridSize()
      attr: parseAttributes()
      listeners: parseListeners()
    log.debug "tile size: #{tile.size}"
    return tile

  parseListeners = () ->
    out = {}
    $.each($props_container.data(), (k, v) ->
      out[k] = v
    )
    return out

  parseAttributes = () ->
    attr = {}
    key = ""
    $.each($props_container.children(), 
      (k, v) ->
        k = $(v).attr("id")
        if k isnt "inmap"
          attr[k] = $(v).find("input").val()
    )
    return attr

  parseMiniGridSize = () ->
    out = []
    $.each($ent_container.find(".minigrid").children(), 
      (k, v) ->
        out[k] = 0
        if $(v).hasClass "minigrid-active-cell"
          out[k] = 1
    )
    binaryString = out.toString().replace(/,/g,'') #"1#{out.toString().replace(/,/g,'')}"
    log.debug binaryString
    #num = parseInt(binaryString, 2)
    #log.debug num
    return binaryString

  drawFoldersAndSprites = (name) ->
    sprs = Hal.asm.getSpritesFromFolder(name + "/")
    infolders = Hal.asm.getSpriteFoldersFromFolder(name + "/")

    $sprites_container.empty()

    for k, folder of infolders
      log.debug "folder name: #{name + '/' + k}"
      li = createSpriteFolder(name + "/" + k)
      $sprites_container.append(li)

    for k, spr of sprs
      sprli = createSpriteWrapperWithCMenu(spr)
      $sprites_container.append(sprli)

  createSpriteFolder = (name) ->
    img = new Image()
    img.src = "img/folder.png"
    $li = createLi("folder_#{name}", name)
    $title = $("<div/>")
    $title.addClass("selectable-title")
    $title.text(name)

    $li.append(img)
    $li.append($title)

    log.debug "creating sprite folder: #{name}"
    $li.dblclick () ->

      drawFoldersAndSprites(name)      
      $back = $("#back-arrow")

      if $back.length > 0
        $back.remove()

      back_img = new Image()
      back_img.id = "back-arrow"
      back_img.src = "img/back.png"
      back_img.style = "cursor: pointer;"
      $back = $(back_img)
      $back.data("addr", name)

      $back.click () ->
        addr = $(@).data("addr")
        
        log.debug "addr prev is: #{addr}"
        ind = addr.lastIndexOf("/"); 
        
        if ind isnt -1
          addr = addr.substr(0, ind)
          $(@).data("addr", addr)
          drawFoldersAndSprites(addr)
        else
          showFolders()
          $(@).remove()
        
        log.debug "addr next is: #{addr}"

      $sprites_container.parent().prepend(back_img)

    return $li

  createKeyLabel = (key) ->
    $label = $("<label/>",
      "for": key
    )
    $label.text("#{key}:")
    return $label

  createKeyValWrapper = (label, val) ->
    $wrapper = $("<div/>",
      "class": "keyval-wrapper"
      "id": label.attr("for")
    )
    return $wrapper.append(label).append(val)

  createNewProperty = (key, val, box, type, regexValidator) ->
    $input = $("<input/>", 
      "type": "text"
      "name": key
      "id": key
      "class": "ui-widget-content ui-corner-all"
      "value": val
    )
    box.append(createKeyValWrapper(createKeyLabel(key), $input))

  createSpriteDroppable = (key, container, spr) ->
    $sprholder = $("<div/>",
      "id": "ent-sprite-droppable"
    )
    $sprholder.addClass("sprite-placeholder")
    $sprholder.addClass("ui-state-default")
    $sprholder.droppable(
      activeClass: "ui-state-default"
      hoverClass: "ui-state-hover"
      accept: ".sprite-box"
      addClasses: false
      drop: (event, ui) ->
        sprname = ui.draggable.attr('id')
        container.data("sprite_name", sprname)
        #$(this).empty()
        img = new Image()
        img.src = Hal.asm.getSprite(sprname).img.src
        $(this).find("img").replaceWith(img)
        $(this).find(".selectable-title").text(sprname)
        createGrid(sprname, "size", $ent_container, parseMiniGridSize())
        $(this).parent().parent().find(".keyval-wrapper#size:first").remove()
      deactivate: (ev, ui) ->
        $(this).addClass("ui-state-default")
        $(this).addClass("sprite-placeholder")
    )

    if spr?
      spr = spr.clone()
      spr.css(
        "margin": "0 0 0 0"
        "border": "0"
      )
      container.data("sprite_name", spr.attr("id"))
      $sprholder.empty()
      spr.appendTo($sprholder)
      spr.fadeIn()

    container.append(createKeyValWrapper(createKeyLabel(key), $sprholder))

  createGrid = (sprname, key, container, encodednum) ->
    toggleActiveCell = () ->
      $(this).toggleClass("minigrid-active-cell")

    spr = Hal.asm.getSprite(sprname)
    h = Math.pow(2, ~~(Math.log(spr.h-1)/Math.LN2) + 1)
    factor = 16
    size = 128
    w = Math.max(h, spr.h) * 2
    numrows = w / size
    numcols = w / size
    diagonal = (Math.sqrt(2*size*size) * numrows) / (size/factor)
    diff = diagonal - (numcols*factor)
    
    $wrapper = $("<div/>", 
      "width": (diagonal) + "px"
      "height": (diagonal / 2) + "px"
      "class" :"minigrid-wrapper"
    )

    $parent = $("<div/>",
      "class": "minigrid"
      "width": numcols * factor
      "height": numrows * factor
      "css":
        "left": (diff * 0.5 - 1) + "px"
        "top" : -(diff * 0.5 - (numrows*5) / 2 + (numrows / 2 + 1)) + "px"
    )

    k = 0
    bin = encodednum.split('')

    for i in [0...numrows]
      for j in [0...numcols]
        $cell = $("<div/>", 
          "css":
            "float": "left"
          "width": factor - 1
          "height": factor - 1
        )
        if +bin[k]
          $cell.addClass("minigrid-active-cell")
        k++
        $cell.appendTo($parent)
        $cell.click(toggleActiveCell)

    $parent.appendTo($wrapper)
    container.append(createKeyValWrapper(createKeyLabel(key), $wrapper))

  createNewLayerComboBox = (key, values, container, layer) ->
    $cbox = $("<select/>")
    for v in values
      opt = $("<option/>")
      opt.text(v).appendTo($cbox)
      if v == layer
        opt.attr("selected", "selected")
    container.append(createKeyValWrapper(createKeyLabel(key), $cbox))

  Hal.on "LOAD_TILE_INMAP", (data) ->
    amjad_tile = data.tile
    log.info data
    t = {tile: data.meta, spr: createSpriteWrapper(data.spr)}
    t.inmap = true
    loadTileFromMeta(t)

  loadTileFromMeta = (tilemeta) ->
    if tilemeta.inmap
      $ent_save_btn.text("Save [InMap]")
    else
      $ent_save_btn.text("Save")

    log.info tilemeta
    $props_container.empty()
    tile = tilemeta.tile
    for k, attr of tile.attr
      createNewProperty(k, attr, $props_container)
    if tile.listeners
      addTileListeners(tile.listeners)
    $ent_container.empty()

    for k, attr of tile
      type = typeof attr
      log.info attr
      if type isnt "object" and type isnt "function"
        if k is "level"
          createNewLayerComboBox(k, Object.keys(TileLayers), $ent_container, attr)
        else if k is "sprite"
          createSpriteDroppable(k, $ent_container, tilemeta.spr)
        else if k is "size"
          createGrid(tile.sprite, k, $ent_container, attr)
        else 
          if k is "name"
            ind = attr.indexOf("_")
            if ind isnt -1
              attr = attr.substr(ind+1)
          createNewProperty(k, attr, $ent_container)

  addTileListeners = (listarr) ->
    log.debug "adding listeners..."
    for k, list of listarr
      $props_container.data(k, list)

  createTileEntityFromSprite = (spr) ->
    tile = 
      name: ""
      level: "terrain"
      sprite: spr.folder + spr.name
      size: "1"
      attr: 
        description: "placeholder tile"
        type: "none"
    return tile

  showFolders = (folder) ->
    log.debug "showing folder: #{folder}"
    $sprites_container.empty()    
    if not folder?
      spr_folders = Hal.asm.getSpriteFolders()
      for folder in spr_folders
        li = createSpriteFolder(folder)
        $sprites_container.append(li)
    else 
      spr_folders = Hal.asm.getSpriteFoldersFromFolder(folder + "/")
      for k, folder of spr_folders
          log.debug "folder name: #{folder + '/' + k}"
          li = createSpriteFolder(folder + "/" + k)
          $sprites_container.append(li)  

  createLi = (name, title) ->
    $li = $("<li/>",
      "id" : name
      "class": "sprite-box"
      "css": 
        "overflow": "hidden"
      "title": title
    )
    return $li

  createSpriteWrapper = (spr) ->
    $li = createLi(spr.folder + spr.name, "#{spr.name}_#{spr.img.width}x#{spr.img.height}")
    $title = $("<div/>")
    $title.addClass("selectable-title")
    $title.text(spr.name)
    $li.append(spr.img)
    $li.append($title)
    return $li

  createSpriteWrapperWithCMenu = (spr) ->
    $li = createSpriteWrapper(spr)
    $li.contextPopup(
      "title": spr.name
      "items": [
        {
          "label": "Create tile"
          "action": () ->
            t = createTileEntityFromSprite(spr)
            loadTileFromMeta(
              "tile": t
              "spr": $li
            )
        }
      ]
    )

    $li.draggable({
        helper: "clone"
    });
    return $li
  
  socket.on "LOAD_MAPS", (data) ->
    allmaps = JSON.parse(data)
    for m in allmaps
      addMap(m)

  socket.on "MAP_SAVED", (name) ->
    addMap(name)

  addMap = (name) ->
    $mapn = $("<option/>")
    ind = name.indexOf(".json")
    if ind isnt -1
      name = name.substr(0, ind)
    $mapn.text(name)
    if Maps[name]?
      return
    Maps[name] = name
    $map_list_box.append($mapn)

  saveMap = (map, name) ->
    log.debug "saving map to name"
    socket.emit "NEW_MAP", {name: name, map: JSON.stringify(map)}

  $map_save.click () ->
    amjad.pause()
    $("#save-map-dialog").dialog(
      autoOpen: true
      height: 300
      width: 350
      modal: true
      closeOnEscape: true
      buttons:
        "OK": () ->
            name = $(this).find("input").val()
            $(this).dialog("close")
            amjad.trigger "REQUEST_MAP_SAVE", name
            amjad.resume()
      close: () ->
        amjad.resume()
      create: () ->
        $name = $(this).find("input")
        $name.val(amjad.name)
    )

  $map_new.click () ->
    showNewMapDialog(true)

  Hal.on "MAP_SAVED", (data) ->
    log.debug data
    saveMap(data.map, data.name)

  $map_list.click () ->
    $ents_container.empty()
    $tile_group.hide()
    $map_group.show()
    $ents_container.slideDown()
    $map_list_box.appendTo($ents_container)

  $map_load.click () ->
    nm = $map_list_box.find("option:selected").text()
    loadMap(nm)

  loadMap = (name) ->
    log.debug "loading map #{name}"
    #extract name from placeholder_dim1xdim2
    amjad.name = name.match(/(.*)_(\d+x\d+)/)[1]
    $.getJSON "assets/maps/" + name + ".json", (data) ->
      mapbar = null
      mapbar = new MapLoaderBar(amjad, () ->
      )
      amjad.trigger "REQUEST_MAP_LOAD", {map: data, name: name}
  
  ###
    Setup entity sandbox
  ###
  do () ->
    $new_prop_btn.click () ->
      amjad.pause()
      $("#keyval-form").dialog
        autoOpen: true
        height: 300
        width: 350
        modal: true
        closeOnEscape: true
        buttons:
          "OK": () ->
            key = $(@).find("#key").val()
            val = $(@).find("#value").val()
            createNewProperty(key, val, $props_container)
            $(@).dialog("close")
            amjad.resume()
        close: () ->
          amjad.resume()

    $new_list_btn.click () ->
      amjad.pause()
      $("#list-form").dialog
        autoOpen: true
        height: 500
        width: 550
        modal: true
        closeOnEscape: true   
        buttons:
          "OK": () ->
            list = $(@).find("option:selected").text()
            code = $(@).find("#code-area").val()
            $props_container.data(list, code)
            $(@).dialog("close")

        close: () ->
          amjad.resume()

        create: () ->
          $(@).find("#events").change () ->
            list = $(@).find("option:selected").text()
            log.debug "listener: " + list
            code = $props_container.data(list)
            $(@).parent().find("#code-area").val(code)

          $(@).find("#events").trigger("change")


  showNewMapDialog = (closeable) ->
    $("#new-map-form").dialog
      autoOpen: true
      height: 350
      width: 350
      modal: true
      closeOnEscape: closeable
      buttons:
        "OK": () ->
          name = $(@).find("#name").val()
          rows = $(@).find("#rows").val()
          cols = $(@).find("#cols").val()
          $selected = $(@).find("option:selected")

          log.debug name + "," + rows + "," + cols
          
          if amjad?
            Hal.scm.removeScene(amjad)

          amjad = new AmjadMapEditor({
            "name": name
            "numrows": rows
            "numcols": cols
            "tiledim": 
              width: 128
              height: 64
          });

          amjad.pause()

          amjad.on "MAP_LOADING_STARTED", () ->
            @pause()

          amjad.on "MAP_LOADED", () ->
            @resume()

          map = $selected.val()

          if map.length > 0
            loadMap(map)
          else
            amjad.resume()

          Hal.scm.addScene(amjad)

          $(@).dialog("close")
          Hal.start()

      open: () ->
        xbtn =  $(@).parent().children().children('.ui-dialog-titlebar-close')
        
        if closeable
          log.debug "show close button"
          xbtn.show()
        else
          xbtn.hide()

        maps = $(@).find("#maps")
        $name = $(@).find("#name")
        $rows = $(@).find("#rows")
        $cols = $(@).find("#cols")

        maps.change () ->
          $selected = $(@).find("option:selected")
          name = $selected.val()
          log.debug name
          if name.length is 0 or not name?
            $rows.val("")
            $name.val("")
            $cols.val("")
          else 
            size = name.match(/(\d+x\d+)/g)[0].split("x")
            $rows.val(size[0])
            $cols.val(size[1])
            $name.val(name)

        for k, v of Maps
          opt = $("<option/>")
          opt.attr("value", k)
          opt.text(k)
          maps.append opt

  showSavePatternDialog = (pattern) ->
    $("#save-pattern-form").dialog
      autoOpen: true
      height: 250
      width: 200
      modal: true
      closeOnEscape: true   
      buttons:
        "OK": () ->
          name = $(@).find("#pattern-name").val()
          socket.emit "NEW_PATTERN", {name: name, pattern: pattern}
          $(@).dialog("close")

      close: () ->
        amjad.resume()
