(function() {
  "use strict";
  define([], function() {
    return Hal.trigger("DOM_ADD", function(domlayer) {
      var div, right, width;
      if (!Hal.supports("FULLSCREEN")) {
        log.warn("fullscreen not supported on this device");
      }
      width = 50;
      right = 0;
      div = $("<div/>", {
        css: {
          "background-color": "rgb(94, 190, 255)",
          "opacity": "0.6",
          "text-align": "center",
          "font-family": "sans-serif",
          "font-size": "0.65em",
          "position": "absolute",
          "width": width + "px",
          "display": "block",
          "right": right + "px",
          "border-radius": "5px",
          "cursor": "pointer"
        }
      });
      div.text("Go fullscreen");
      div.on("mousemove", function(ev) {
        return ev.stopPropagation();
      });
      div.on("mouseup", function(ev) {
        return ev.stopPropagation();
      });
      div.on("click", function(ev) {
        Hal.trigger("REQUEST_FULLSCREEN");
        return ev.stopPropagation();
      });
      Hal.on("ENTER_FULLSCREEN", function() {
        div.hide();
        return $("#hud").toggle();
      });
      Hal.on("EXIT_FULLSCREEN", function() {
        div.show();
        return $("#hud").toggle();
      });
      return $(domlayer).append(div);
    });
  });

}).call(this);
