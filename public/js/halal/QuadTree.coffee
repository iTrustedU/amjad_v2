"use strict"

define () ->
    capacity = 1
    class QuadTree
        constructor: (@bounds) ->
            @pts = []
            @nw = null
            @sw = null
            @ne = null
            @se = null

        insert: (ent) ->
            # if not Hal.m.isPointInRect([ent.pos[0], ent.pos[1]], @bounds) and
            # not Hal.m.isPointInRect([ent.pos[0] + ent.bounds[2], ent.pos[1]], @bounds) and
            # not Hal.m.isPointInRect([ent.pos[0], ent.pos[1] + ent.bounds[3]], @bounds) and
            # not Hal.m.isPointInRect([ent.pos[0] + ent.bounds[2], ent.pos[1] + ent.bounds[3]], @bounds) 
                #alert("what")
            if not Hal.m.isPointInRect(ent.pos, @bounds) #ent.inBounds([@bounds[0]*0.5, @bounds[1]*0.5])
                return false

            if @pts.length < capacity
                ent.quadspace = @
                @pts.push(ent)
                return true

            if not @nw?
                @divide()

            if @nw.insert(ent)
                return true
            if @ne.insert(ent)
                return true
            if @sw.insert(ent)
                return true
            if @se.insert(ent)
                return true

            return false

        remove: (ent) ->
            ind = @pts.indexOf(ent)
            @pts.splice(ind, 1)

        searchInRange: (range) ->
            entsInRange = []
            if not Hal.m.rectIntersectsRect(range, @bounds)
                return entsInRange

            for p in @pts
                if p.rectIntersectsShape(range)
                    entsInRange.push(p)

            if not @nw?
                return entsInRange

            entsInRange = entsInRange.concat(@nw.searchInRange(range))
            entsInRange = entsInRange.concat(@ne.searchInRange(range))
            entsInRange = entsInRange.concat(@sw.searchInRange(range))
            entsInRange = entsInRange.concat(@se.searchInRange(range))

            return entsInRange

        divide: () ->
            w = @bounds[2] / 2
            h = @bounds[3] / 2

            @nw = new QuadTree([@bounds[0], @bounds[1], w, h])
            @ne = new QuadTree([@bounds[0] + w, @bounds[1], w, h])
            
            @sw = new QuadTree([@bounds[0], @bounds[1] + h, w, h])
            @se = new QuadTree([@bounds[0] + w, @bounds[1] + h, w, h])


    return QuadTree