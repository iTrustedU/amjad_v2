define () ->
    class Result
        constructor: (@url) ->
            @success_ = @fail_ = @always_ = Function()
            @success = (@success_) ->
            @fail = (@fail_) ->
            @always = (@always_) ->
                
    Ajax = {}

    Ajax.get = (url, callbacks...) ->
        result = new Result(document.domain + '/' + url)
        ajaxreq = new XMLHttpRequest()
        ajaxreq.open("GET", url)
        ajaxreq.send()

        ajaxreq.onreadystatechange = () ->
            if (ajaxreq.readyState == 4)
                type = ajaxreq.getResponseHeader("Content-Type")
                if (ajaxreq.status == 200)
                    data = ajaxreq.responseText
                    console.log type
                    console.log url
                    data = JSON.parse(data) if type == "application/json" and url.indexOf("json") is -1
                    result.success_(data) 
                    callbacks[0](data) if callbacks[0]
                else
                    result.fail_(url)
                    callbacks[1](data) if callbacks[1]
                result.always_(url || data)
                callbacks[2](data) if callbacks[2]


        return result


    Ajax.post = (url, data, callbacks...) ->
        result = new Result(document.domain + '/' + url)
        ajaxreq = new XMLHttpRequest()
        ajaxreq.open("POST", url);
        ajaxreq.setRequestHeader("Content-Type", "x-www-form-urlencoded")
        ajaxreq.send(data)

        ajaxreq.onreadystatechange = () ->
            if (ajaxreq.readyState == 4)
                type = ajaxreq.getResponseHeader("Content-Type")
                if (ajaxreq.status == 200)
                    data = ajaxreq.responseText
                    data = JSON.parse(data) if type == "application/json"
                    result.success_(data)
                    callbacks[0](data) if callbacks[0]
                else
                    result.fail_(url)
                    callbacks[1](data) if callbacks[1]
                result.always_(url || data)
                callbacks[2](data) if callbacks[2]
        
        return result

    return Ajax