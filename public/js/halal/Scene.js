(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher", "Renderer", "Camera", "QuadTree"], function(EventDispatcher, Renderer, Camera, QuadTree) {
    var Scene;
    Scene = (function(_super) {
      __extends(Scene, _super);

      function Scene(name, bounds, renderer) {
        this.name = name;
        this.bounds = bounds;
        Scene.__super__.constructor.call(this);
        this.paused = false;
        this.id = Hal.UID();
        this.in_focus = [false];
        this.mpos = [0, 0];
        this.depth = 0;
        this.x = this.bounds[0];
        this.y = this.bounds[1];
        this.ents = [];
        this.scale = 1;
        this.ent_cache = {};
        this.g = renderer || Hal.r;
        this.debug = false;
        this.initHandlers();
        this.addCamera();
      }

      Scene.prototype.resetQuadSpace = function(dim) {
        this.quadspace = null;
        this.quadspace = new QuadTree(dim);
        return this.quadspace.divide();
      };

      Scene.prototype.addCamera = function() {
        this.camera = new Camera(this.g.ctx, [0, 0], this.bounds[2], this.bounds[3], this);
        this.camera.enableDrag();
        this.camera.enableLerp();
        return this.camera.enableZoom();
      };

      Scene.prototype.resume = function() {
        this.paused = false;
        Hal.trigger("SCENE_RESUMED", this);
        return log.info("" + this.name + " resumed");
      };

      Scene.prototype.pause = function() {
        this.paused = true;
        Hal.trigger("SCENE_PAUSED", this);
        return log.info("" + this.name + " paused");
      };

      Scene.prototype.initHandlers = function() {
        var _this = this;
        this.on("ZOOM", function(scale) {
          _this.scale = scale;
          return log.info("zoom happened: " + scale);
        });
        this.on("RESIZE", function(scale) {
          return log.info("resize happened: " + scale);
        });
        this.on("ENTER_FULLSCREEN", function(scale) {
          _this.scale = scale[0];
          _this.bounds[2] = Hal.dom.area.width;
          _this.bounds[3] = Hal.dom.area.height;
          log.info(_this.name + " in fullscreen");
          log.info("scale: " + scale[0] + "," + scale[1]);
          return log.info(_this.bounds);
        });
        this.on("EXIT_FULLSCREEN", function(scale) {
          _this.scale = scale[0];
          _this.bounds[2] = Hal.dom.area.width;
          _this.bounds[3] = Hal.dom.area.height;
          log.info(_this.name + " exited fullscreen");
          log.info("scale: " + scale[0] + "," + scale[1]);
          return log.info(_this.bounds);
        });
        this.on("KEY_UP", function(key) {
          if (key.keyCode === Hal.Keys.LEFT) {
            return Hal.trigger("SHOW_PREV_SCENE");
          } else if (key.keyCode === Hal.Keys.RIGHT) {
            return Hal.trigger("SHOW_NEXT_SCENE");
          }
        });
        Hal.on("DEBUG_MODE", function(debug) {
          _this.debug = debug;
          return log.info("debug mode: " + _this.debug);
        });
        return this.on("ENTITY_MOVING", function(ent) {
          if (!Hal.m.isPointInRect(ent.pos, ent.quadspace.bounds)) {
            ent.quadspace.remove(ent);
            return _this.quadspace.insert(ent);
          }
        });
      };

      Scene.prototype.drawStat = function() {
        Hal.glass.ctx.fillText("Camera: " + this.camera.pos[0] + ", " + this.camera.pos[1], 0, 20);
        Hal.glass.ctx.fillText("Zoom: " + this.camera.zoom, 0, 30);
        Hal.glass.ctx.fillText("Mouse: " + this.mpos[0] + ", " + this.mpos[1], 0, 40);
        return Hal.glass.ctx.fillText("Num of entities: " + this.ents.length, 0, 50);
      };

      Scene.prototype.drawQuadTree = function(quadspace) {
        if (quadspace.nw != null) {
          this.drawQuadTree(quadspace.nw);
          this.g.ctx.strokeRect(quadspace.nw.bounds[0], quadspace.nw.bounds[1], quadspace.nw.bounds[2], quadspace.nw.bounds[3]);
        }
        if (quadspace.ne != null) {
          this.drawQuadTree(quadspace.ne);
          this.g.ctx.strokeRect(quadspace.ne.bounds[0], quadspace.ne.bounds[1], quadspace.ne.bounds[2], quadspace.ne.bounds[3]);
        }
        if (quadspace.sw != null) {
          this.drawQuadTree(quadspace.sw);
          this.g.ctx.strokeRect(quadspace.sw.bounds[0], quadspace.sw.bounds[1], quadspace.sw.bounds[2], quadspace.sw.bounds[3]);
        }
        if (quadspace.se != null) {
          this.drawQuadTree(quadspace.se);
          return this.g.ctx.strokeRect(quadspace.se.bounds[0], quadspace.se.bounds[1], quadspace.se.bounds[2], quadspace.se.bounds[3]);
        }
      };

      Scene.prototype.update = function(delta) {
        this.g.ctx.setTransform(this.camera.zoom, 0, 0, this.camera.zoom, this.camera.pos[0], this.camera.pos[1]);
        if (this.debug) {
          this.drawStat();
          this.g.ctx.save();
          this.g.ctx.strokeStyle = "blue";
          this.drawQuadTree(this.quadspace);
          this.g.ctx.stroke();
          return this.g.ctx.restore();
        }
      };

      Scene.prototype.inBounds = function(point) {
        return Hal.m.isPointInRect(point, this.bounds);
      };

      Scene.prototype.drawBounds = function() {
        return this.g.strokeRect(this.bounds);
      };

      Scene.prototype.addEntity = function(ent) {
        if (this.exists(ent.id)) {
          return;
        }
        ent.scene = this;
        ent.g = this.g;
        ent.pos[0] -= this.camera.pos[0] / this.camera.zoom;
        ent.pos[1] -= this.camera.pos[1] / this.camera.zoom;
        ent.init();
        this.ent_cache[ent.id] = true;
        this.ents.push(ent);
        return ent;
      };

      Scene.prototype.removeEntity = function(ent) {
        var ind;
        ind = this.ents.indexOf(ent);
        if (ind !== -1) {
          this.ent_cache[ent.id] = false;
          this.ents[ind] = null;
          return this.ents.splice(ind);
        }
      };

      Scene.prototype.exists = function(id) {
        return this.ent_cache[id];
      };

      Scene.prototype.move = function(x, y) {
        this.bounds[0] = x;
        this.bounds[1] = y;
        this.x = x;
        this.y = y;
        this.g.offsetX = x;
        return this.g.offsetY = y;
      };

      return Scene;

    })(EventDispatcher);
    return Scene;
  });

}).call(this);
