"use strict"

define [],

() ->

    Hal.trigger "DOM_ADD", (domlayer) ->
        if not Hal.supports("FULLSCREEN")
            log.warn "fullscreen not supported on this device"

        width = 50
        right = 0
        div = $("<div/>", {
            css: 
                "background-color": "rgb(94, 190, 255)",
                "opacity": "0.6",
                "text-align": "center",
                "font-family": "sans-serif",
                "font-size": "0.65em",
                "position": "absolute",
                "width": width + "px",
                "display": "block",
                "right": right + "px"
                "border-radius": "5px"
                "cursor": "pointer"
        })

        div.text("Go fullscreen")
        
        div.on "mousemove", (ev) ->
            ev.stopPropagation()

        div.on "mouseup", (ev) ->
            ev.stopPropagation()

        div.on "click", (ev) ->
            Hal.trigger "REQUEST_FULLSCREEN"
            ev.stopPropagation()

        Hal.on "ENTER_FULLSCREEN", () -> 
            div.hide()
            $("#hud").toggle()

        Hal.on "EXIT_FULLSCREEN", () -> 
            div.show()
            $("#hud").toggle()

        $(domlayer).append(div)
    