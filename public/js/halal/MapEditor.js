(function() {
  "use strict";
  define(["jquery-ui", "FullscreenButton", "AssetLoaderBar", "../amjad/AmjadMapEditor", "MapLoaderBar"], function($, FullscreenButton, AssetLoaderBar, AmjadMapEditor, MapLoaderBar) {
    var $all_containers, $anim_container, $ent_container, $ent_new_btn, $ent_save_btn, $ents_container, $map_group, $map_list, $map_list_box, $map_load, $map_new, $map_save, $new_list_btn, $new_prop_btn, $pattern_group, $pattern_list, $props_container, $sheet_container, $sprites_container, $tile_group, $tile_list, Maps, Patterns, TileLayers, addMap, addTileListeners, amjad, amjad_tile, createGrid, createKeyLabel, createKeyValWrapper, createLi, createNewLayerComboBox, createNewProperty, createPatternWrapper, createSpriteDroppable, createSpriteFolder, createSpriteWrapper, createSpriteWrapperWithCMenu, createTileEntityFromSprite, createTileWrapper, drawFoldersAndSprites, extract_int, loadMap, loadTileFromMeta, parseAttributes, parseListeners, parseMiniGridSize, progressbar, saveCurrentTile, saveMap, showFolders, showNewMapDialog, showPatterns, showSavePatternDialog, socket;
    extract_int = /^(\d+)%$/;
    socket = io.connect('http://localhost:8080');
    $props_container = $("#entity-properties-container");
    $ent_container = $("#entity-container");
    $anim_container = $("#animation-container");
    $sprites_container = $("#sprites-container");
    $sheet_container = $("#spritesheets-container");
    $all_containers = $("ol[id$='-container']");
    $ents_container = $('#list-container');
    $ent_save_btn = $('#entity-save');
    $ent_new_btn = $('#entity-new');
    $tile_list = $('#tiles-list');
    $tile_group = $('.tile-group');
    $pattern_group = $('.pattern-group');
    $map_list = $('#map-list');
    $pattern_list = $('#pattern-list');
    $map_group = $('.map-group');
    $map_list_box = $('<select/>', {
      "size": 3,
      "class": '.map-list'
    });
    $map_save = $('#map-save');
    $map_load = $('#map-load');
    $map_new = $("#map-new");
    $new_prop_btn = $('#new-prop-btn');
    $new_list_btn = $('#new-list-btn');
    amjad = null;
    amjad_tile = null;
    Hal.start();
    progressbar = new AssetLoaderBar(function() {
      return showNewMapDialog(false);
    });
    Hal.asm.loadViaSocketIO();
    Maps = {};
    TileLayers = {
      terrain: [],
      decor: [],
      buildings: [],
      poi: [],
      character: []
    };
    Patterns = {};
    $.getJSON("assets/amjad/TilesList.json", function(data) {
      var k, tile, _results;
      log.info(data);
      _results = [];
      for (k in data) {
        tile = data[k];
        _results.push(TileLayers[tile.level][tile.name] = tile);
      }
      return _results;
    });
    $.getJSON("assets/amjad/Patterns.json", function(data) {
      var k, pattern, _results;
      _results = [];
      for (k in data) {
        pattern = data[k];
        _results.push(Patterns[k] = pattern);
      }
      return _results;
    });
    Hal.on("NEW_PATTERN_CREATED", function(pattern) {
      var k, nums, out, pivot, pt, v;
      log.debug("new pattern created");
      log.debug(pattern);
      out = [];
      pivot = pattern["pivot"].serialize();
      out.push(pivot);
      for (k in pattern) {
        v = pattern[k];
        if (k === "pivot") {
          continue;
        }
        pt = v.serialize();
        nums = k.split("_");
        pt.off = [+nums[0], +nums[1]];
        out.push(pt);
      }
      return showSavePatternDialog(JSON.stringify(out));
    });
    Hal.asm.on("SPRITES_LOADED", function(spr) {
      log.debug("wtfasss");
      return showFolders();
    });
    socket.on("TILE_ADDED", function(tile) {
      log.info("tile loaded...");
      log.info(tile);
      TileLayers[tile.level][tile.name] = tile;
      Hal.trigger("TILE_ADDED", tile);
      log.info(TileLayers);
      $ents_container.empty();
      return $("#entities-" + tile.level).click();
    });
    socket.on("PATTERN_ADDED", function(data) {
      Patterns[data.name] = JSON.parse(data.pattern);
      return $pattern_list.click();
    });
    socket.on("TILE_DELETED", function(tile) {
      delete TileLayers[tile.level][tile.name];
      return $("#entities-" + tile.level).click();
    });
    $ent_save_btn.click(function() {
      var jsonstr, tile;
      tile = saveCurrentTile();
      log.debug(tile);
      jsonstr = JSON.stringify(tile);
      socket.emit("NEW_TILE", {
        tile: jsonstr
      });
      return log.info(tile);
    });
    $tile_list.click(function() {
      $ents_container.empty();
      $map_group.hide();
      $tile_group.show();
      return $ents_container.slideDown();
    });
    $pattern_list.click(function() {
      $ents_container.empty();
      $map_group.hide();
      $tile_group.hide();
      $pattern_group.show();
      $ents_container.slideDown();
      return showPatterns();
    });
    $('#entities-terrain,#entities-decor,#entities-buildings,#entities-poi,#entities-character').click(function() {
      var id, k, tile, _ref, _results;
      $(this).parent().parent().find("a").removeClass("active");
      $(this).addClass("active");
      $tile_group.show();
      $ents_container.empty();
      log.info(this);
      id = $(this).attr('id');
      id = id.substr(id.indexOf('-') + 1);
      _ref = TileLayers[id];
      _results = [];
      for (k in _ref) {
        tile = _ref[k];
        log.info(tile);
        _results.push($ents_container.append(createTileWrapper(tile, id)));
      }
      return _results;
    });
    showPatterns = function() {
      var k, v, _results;
      _results = [];
      for (k in Patterns) {
        v = Patterns[k];
        _results.push($ents_container.append(createPatternWrapper(k, v)));
      }
      return _results;
    };
    createPatternWrapper = function(name, pattern) {
      var $li, $title, img;
      $li = createLi(name, name);
      img = $(Hal.asm.getSprite("editor/pattern").img).clone();
      $title = $("<div/>");
      $title.addClass("selectable-title");
      $title.text(name);
      $li.append(img);
      $li.append($title);
      $li.contextPopup({
        "title": name,
        "items": [
          {
            "label": "Remove pattern",
            "action": function() {
              return socket.emit("DELETE_PATTERN", {
                name: name
              });
            }
          }
        ]
      });
      $li.attr("id", name);
      $li.click((function(name) {
        return function() {
          return amjad.trigger("PATTERN_SELECTED", pattern);
        };
      })(name));
      $li.fadeIn();
      return $li;
    };
    createTileWrapper = function(tile, id) {
      var $li, $title, img;
      $li = createLi(tile.sprite, tile.name);
      img = $(Hal.asm.getSprite(tile.sprite).img).clone();
      $title = $("<div/>");
      $title.addClass("selectable-title");
      $title.text(tile.name);
      $li.append(img);
      $li.append($title);
      $li.contextPopup({
        "title": tile.name,
        "items": [
          {
            "label": "Remove tile",
            "action": function() {
              return socket.emit("DELETE_TILE", {
                name: tile.name,
                level: tile.level
              });
            }
          }
        ]
      });
      $li.attr("id", tile.sprite);
      $li.click((function(id, tile) {
        return function() {
          amjad.trigger("TILELAYER_SELECTED", tile);
          return loadTileFromMeta({
            tile: tile,
            spr: $(this)
          });
        };
      })(id, tile));
      $li.fadeIn();
      return $li;
    };
    saveCurrentTile = function() {
      var lvl, tile;
      lvl = $ent_container.find("#level").find("select > option:selected").text();
      tile = {
        level: lvl,
        sprite: $ent_container.data("sprite_name"),
        name: lvl + "_" + $ent_container.find("#name").find("input").val(),
        size: parseMiniGridSize(),
        attr: parseAttributes(),
        listeners: parseListeners()
      };
      log.debug("tile size: " + tile.size);
      return tile;
    };
    parseListeners = function() {
      var out;
      out = {};
      $.each($props_container.data(), function(k, v) {
        return out[k] = v;
      });
      return out;
    };
    parseAttributes = function() {
      var attr, key;
      attr = {};
      key = "";
      $.each($props_container.children(), function(k, v) {
        k = $(v).attr("id");
        if (k !== "inmap") {
          return attr[k] = $(v).find("input").val();
        }
      });
      return attr;
    };
    parseMiniGridSize = function() {
      var binaryString, out;
      out = [];
      $.each($ent_container.find(".minigrid").children(), function(k, v) {
        out[k] = 0;
        if ($(v).hasClass("minigrid-active-cell")) {
          return out[k] = 1;
        }
      });
      binaryString = out.toString().replace(/,/g, '');
      log.debug(binaryString);
      return binaryString;
    };
    drawFoldersAndSprites = function(name) {
      var folder, infolders, k, li, spr, sprli, sprs, _results;
      sprs = Hal.asm.getSpritesFromFolder(name + "/");
      infolders = Hal.asm.getSpriteFoldersFromFolder(name + "/");
      $sprites_container.empty();
      for (k in infolders) {
        folder = infolders[k];
        log.debug("folder name: " + (name + '/' + k));
        li = createSpriteFolder(name + "/" + k);
        $sprites_container.append(li);
      }
      _results = [];
      for (k in sprs) {
        spr = sprs[k];
        sprli = createSpriteWrapperWithCMenu(spr);
        _results.push($sprites_container.append(sprli));
      }
      return _results;
    };
    createSpriteFolder = function(name) {
      var $li, $title, img;
      img = new Image();
      img.src = "img/folder.png";
      $li = createLi("folder_" + name, name);
      $title = $("<div/>");
      $title.addClass("selectable-title");
      $title.text(name);
      $li.append(img);
      $li.append($title);
      log.debug("creating sprite folder: " + name);
      $li.dblclick(function() {
        var $back, back_img;
        drawFoldersAndSprites(name);
        $back = $("#back-arrow");
        if ($back.length > 0) {
          $back.remove();
        }
        back_img = new Image();
        back_img.id = "back-arrow";
        back_img.src = "img/back.png";
        back_img.style = "cursor: pointer;";
        $back = $(back_img);
        $back.data("addr", name);
        $back.click(function() {
          var addr, ind;
          addr = $(this).data("addr");
          log.debug("addr prev is: " + addr);
          ind = addr.lastIndexOf("/");
          if (ind !== -1) {
            addr = addr.substr(0, ind);
            $(this).data("addr", addr);
            drawFoldersAndSprites(addr);
          } else {
            showFolders();
            $(this).remove();
          }
          return log.debug("addr next is: " + addr);
        });
        return $sprites_container.parent().prepend(back_img);
      });
      return $li;
    };
    createKeyLabel = function(key) {
      var $label;
      $label = $("<label/>", {
        "for": key
      });
      $label.text("" + key + ":");
      return $label;
    };
    createKeyValWrapper = function(label, val) {
      var $wrapper;
      $wrapper = $("<div/>", {
        "class": "keyval-wrapper",
        "id": label.attr("for")
      });
      return $wrapper.append(label).append(val);
    };
    createNewProperty = function(key, val, box, type, regexValidator) {
      var $input;
      $input = $("<input/>", {
        "type": "text",
        "name": key,
        "id": key,
        "class": "ui-widget-content ui-corner-all",
        "value": val
      });
      return box.append(createKeyValWrapper(createKeyLabel(key), $input));
    };
    createSpriteDroppable = function(key, container, spr) {
      var $sprholder;
      $sprholder = $("<div/>", {
        "id": "ent-sprite-droppable"
      });
      $sprholder.addClass("sprite-placeholder");
      $sprholder.addClass("ui-state-default");
      $sprholder.droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ".sprite-box",
        addClasses: false,
        drop: function(event, ui) {
          var img, sprname;
          sprname = ui.draggable.attr('id');
          container.data("sprite_name", sprname);
          img = new Image();
          img.src = Hal.asm.getSprite(sprname).img.src;
          $(this).find("img").replaceWith(img);
          $(this).find(".selectable-title").text(sprname);
          createGrid(sprname, "size", $ent_container, parseMiniGridSize());
          return $(this).parent().parent().find(".keyval-wrapper#size:first").remove();
        },
        deactivate: function(ev, ui) {
          $(this).addClass("ui-state-default");
          return $(this).addClass("sprite-placeholder");
        }
      });
      if (spr != null) {
        spr = spr.clone();
        spr.css({
          "margin": "0 0 0 0",
          "border": "0"
        });
        container.data("sprite_name", spr.attr("id"));
        $sprholder.empty();
        spr.appendTo($sprholder);
        spr.fadeIn();
      }
      return container.append(createKeyValWrapper(createKeyLabel(key), $sprholder));
    };
    createGrid = function(sprname, key, container, encodednum) {
      var $cell, $parent, $wrapper, bin, diagonal, diff, factor, h, i, j, k, numcols, numrows, size, spr, toggleActiveCell, w, _i, _j;
      toggleActiveCell = function() {
        return $(this).toggleClass("minigrid-active-cell");
      };
      spr = Hal.asm.getSprite(sprname);
      h = Math.pow(2, ~~(Math.log(spr.h - 1) / Math.LN2) + 1);
      factor = 16;
      size = 128;
      w = Math.max(h, spr.h) * 2;
      numrows = w / size;
      numcols = w / size;
      diagonal = (Math.sqrt(2 * size * size) * numrows) / (size / factor);
      diff = diagonal - (numcols * factor);
      $wrapper = $("<div/>", {
        "width": diagonal + "px",
        "height": (diagonal / 2) + "px",
        "class": "minigrid-wrapper"
      });
      $parent = $("<div/>", {
        "class": "minigrid",
        "width": numcols * factor,
        "height": numrows * factor,
        "css": {
          "left": (diff * 0.5 - 1) + "px",
          "top": -(diff * 0.5 - (numrows * 5) / 2 + (numrows / 2 + 1)) + "px"
        }
      });
      k = 0;
      bin = encodednum.split('');
      for (i = _i = 0; 0 <= numrows ? _i < numrows : _i > numrows; i = 0 <= numrows ? ++_i : --_i) {
        for (j = _j = 0; 0 <= numcols ? _j < numcols : _j > numcols; j = 0 <= numcols ? ++_j : --_j) {
          $cell = $("<div/>", {
            "css": {
              "float": "left"
            },
            "width": factor - 1,
            "height": factor - 1
          });
          if (+bin[k]) {
            $cell.addClass("minigrid-active-cell");
          }
          k++;
          $cell.appendTo($parent);
          $cell.click(toggleActiveCell);
        }
      }
      $parent.appendTo($wrapper);
      return container.append(createKeyValWrapper(createKeyLabel(key), $wrapper));
    };
    createNewLayerComboBox = function(key, values, container, layer) {
      var $cbox, opt, v, _i, _len;
      $cbox = $("<select/>");
      for (_i = 0, _len = values.length; _i < _len; _i++) {
        v = values[_i];
        opt = $("<option/>");
        opt.text(v).appendTo($cbox);
        if (v === layer) {
          opt.attr("selected", "selected");
        }
      }
      return container.append(createKeyValWrapper(createKeyLabel(key), $cbox));
    };
    Hal.on("LOAD_TILE_INMAP", function(data) {
      var t;
      amjad_tile = data.tile;
      log.info(data);
      t = {
        tile: data.meta,
        spr: createSpriteWrapper(data.spr)
      };
      t.inmap = true;
      return loadTileFromMeta(t);
    });
    loadTileFromMeta = function(tilemeta) {
      var attr, ind, k, tile, type, _ref, _results;
      if (tilemeta.inmap) {
        $ent_save_btn.text("Save [InMap]");
      } else {
        $ent_save_btn.text("Save");
      }
      log.info(tilemeta);
      $props_container.empty();
      tile = tilemeta.tile;
      _ref = tile.attr;
      for (k in _ref) {
        attr = _ref[k];
        createNewProperty(k, attr, $props_container);
      }
      if (tile.listeners) {
        addTileListeners(tile.listeners);
      }
      $ent_container.empty();
      _results = [];
      for (k in tile) {
        attr = tile[k];
        type = typeof attr;
        log.info(attr);
        if (type !== "object" && type !== "function") {
          if (k === "level") {
            _results.push(createNewLayerComboBox(k, Object.keys(TileLayers), $ent_container, attr));
          } else if (k === "sprite") {
            _results.push(createSpriteDroppable(k, $ent_container, tilemeta.spr));
          } else if (k === "size") {
            _results.push(createGrid(tile.sprite, k, $ent_container, attr));
          } else {
            if (k === "name") {
              ind = attr.indexOf("_");
              if (ind !== -1) {
                attr = attr.substr(ind + 1);
              }
            }
            _results.push(createNewProperty(k, attr, $ent_container));
          }
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };
    addTileListeners = function(listarr) {
      var k, list, _results;
      log.debug("adding listeners...");
      _results = [];
      for (k in listarr) {
        list = listarr[k];
        _results.push($props_container.data(k, list));
      }
      return _results;
    };
    createTileEntityFromSprite = function(spr) {
      var tile;
      tile = {
        name: "",
        level: "terrain",
        sprite: spr.folder + spr.name,
        size: "1",
        attr: {
          description: "placeholder tile",
          type: "none"
        }
      };
      return tile;
    };
    showFolders = function(folder) {
      var k, li, spr_folders, _i, _len, _results, _results1;
      log.debug("showing folder: " + folder);
      $sprites_container.empty();
      if (folder == null) {
        spr_folders = Hal.asm.getSpriteFolders();
        _results = [];
        for (_i = 0, _len = spr_folders.length; _i < _len; _i++) {
          folder = spr_folders[_i];
          li = createSpriteFolder(folder);
          _results.push($sprites_container.append(li));
        }
        return _results;
      } else {
        spr_folders = Hal.asm.getSpriteFoldersFromFolder(folder + "/");
        _results1 = [];
        for (k in spr_folders) {
          folder = spr_folders[k];
          log.debug("folder name: " + (folder + '/' + k));
          li = createSpriteFolder(folder + "/" + k);
          _results1.push($sprites_container.append(li));
        }
        return _results1;
      }
    };
    createLi = function(name, title) {
      var $li;
      $li = $("<li/>", {
        "id": name,
        "class": "sprite-box",
        "css": {
          "overflow": "hidden"
        },
        "title": title
      });
      return $li;
    };
    createSpriteWrapper = function(spr) {
      var $li, $title;
      $li = createLi(spr.folder + spr.name, "" + spr.name + "_" + spr.img.width + "x" + spr.img.height);
      $title = $("<div/>");
      $title.addClass("selectable-title");
      $title.text(spr.name);
      $li.append(spr.img);
      $li.append($title);
      return $li;
    };
    createSpriteWrapperWithCMenu = function(spr) {
      var $li;
      $li = createSpriteWrapper(spr);
      $li.contextPopup({
        "title": spr.name,
        "items": [
          {
            "label": "Create tile",
            "action": function() {
              var t;
              t = createTileEntityFromSprite(spr);
              return loadTileFromMeta({
                "tile": t,
                "spr": $li
              });
            }
          }
        ]
      });
      $li.draggable({
        helper: "clone"
      });
      return $li;
    };
    socket.on("LOAD_MAPS", function(data) {
      var allmaps, m, _i, _len, _results;
      allmaps = JSON.parse(data);
      _results = [];
      for (_i = 0, _len = allmaps.length; _i < _len; _i++) {
        m = allmaps[_i];
        _results.push(addMap(m));
      }
      return _results;
    });
    socket.on("MAP_SAVED", function(name) {
      return addMap(name);
    });
    addMap = function(name) {
      var $mapn, ind;
      $mapn = $("<option/>");
      ind = name.indexOf(".json");
      if (ind !== -1) {
        name = name.substr(0, ind);
      }
      $mapn.text(name);
      if (Maps[name] != null) {
        return;
      }
      Maps[name] = name;
      return $map_list_box.append($mapn);
    };
    saveMap = function(map, name) {
      log.debug("saving map to name");
      return socket.emit("NEW_MAP", {
        name: name,
        map: JSON.stringify(map)
      });
    };
    $map_save.click(function() {
      amjad.pause();
      return $("#save-map-dialog").dialog({
        autoOpen: true,
        height: 300,
        width: 350,
        modal: true,
        closeOnEscape: true,
        buttons: {
          "OK": function() {
            var name;
            name = $(this).find("input").val();
            $(this).dialog("close");
            amjad.trigger("REQUEST_MAP_SAVE", name);
            return amjad.resume();
          }
        },
        close: function() {
          return amjad.resume();
        },
        create: function() {
          var $name;
          $name = $(this).find("input");
          return $name.val(amjad.name);
        }
      });
    });
    $map_new.click(function() {
      return showNewMapDialog(true);
    });
    Hal.on("MAP_SAVED", function(data) {
      log.debug(data);
      return saveMap(data.map, data.name);
    });
    $map_list.click(function() {
      $ents_container.empty();
      $tile_group.hide();
      $map_group.show();
      $ents_container.slideDown();
      return $map_list_box.appendTo($ents_container);
    });
    $map_load.click(function() {
      var nm;
      nm = $map_list_box.find("option:selected").text();
      return loadMap(nm);
    });
    loadMap = function(name) {
      log.debug("loading map " + name);
      amjad.name = name.match(/(.*)_(\d+x\d+)/)[1];
      return $.getJSON("assets/maps/" + name + ".json", function(data) {
        var mapbar;
        mapbar = null;
        mapbar = new MapLoaderBar(amjad, function() {});
        return amjad.trigger("REQUEST_MAP_LOAD", {
          map: data,
          name: name
        });
      });
    };
    /*
      Setup entity sandbox
    */

    (function() {
      $new_prop_btn.click(function() {
        amjad.pause();
        return $("#keyval-form").dialog({
          autoOpen: true,
          height: 300,
          width: 350,
          modal: true,
          closeOnEscape: true,
          buttons: {
            "OK": function() {
              var key, val;
              key = $(this).find("#key").val();
              val = $(this).find("#value").val();
              createNewProperty(key, val, $props_container);
              $(this).dialog("close");
              return amjad.resume();
            }
          },
          close: function() {
            return amjad.resume();
          }
        });
      });
      return $new_list_btn.click(function() {
        amjad.pause();
        return $("#list-form").dialog({
          autoOpen: true,
          height: 500,
          width: 550,
          modal: true,
          closeOnEscape: true,
          buttons: {
            "OK": function() {
              var code, list;
              list = $(this).find("option:selected").text();
              code = $(this).find("#code-area").val();
              $props_container.data(list, code);
              return $(this).dialog("close");
            }
          },
          close: function() {
            return amjad.resume();
          },
          create: function() {
            $(this).find("#events").change(function() {
              var code, list;
              list = $(this).find("option:selected").text();
              log.debug("listener: " + list);
              code = $props_container.data(list);
              return $(this).parent().find("#code-area").val(code);
            });
            return $(this).find("#events").trigger("change");
          }
        });
      });
    })();
    showNewMapDialog = function(closeable) {
      return $("#new-map-form").dialog({
        autoOpen: true,
        height: 350,
        width: 350,
        modal: true,
        closeOnEscape: closeable,
        buttons: {
          "OK": function() {
            var $selected, cols, map, name, rows;
            name = $(this).find("#name").val();
            rows = $(this).find("#rows").val();
            cols = $(this).find("#cols").val();
            $selected = $(this).find("option:selected");
            log.debug(name + "," + rows + "," + cols);
            if (amjad != null) {
              Hal.scm.removeScene(amjad);
            }
            amjad = new AmjadMapEditor({
              "name": name,
              "numrows": rows,
              "numcols": cols,
              "tiledim": {
                width: 128,
                height: 64
              }
            });
            amjad.pause();
            amjad.on("MAP_LOADING_STARTED", function() {
              return this.pause();
            });
            amjad.on("MAP_LOADED", function() {
              return this.resume();
            });
            map = $selected.val();
            if (map.length > 0) {
              loadMap(map);
            } else {
              amjad.resume();
            }
            Hal.scm.addScene(amjad);
            $(this).dialog("close");
            return Hal.start();
          }
        },
        open: function() {
          var $cols, $name, $rows, k, maps, opt, v, xbtn, _results;
          xbtn = $(this).parent().children().children('.ui-dialog-titlebar-close');
          if (closeable) {
            log.debug("show close button");
            xbtn.show();
          } else {
            xbtn.hide();
          }
          maps = $(this).find("#maps");
          $name = $(this).find("#name");
          $rows = $(this).find("#rows");
          $cols = $(this).find("#cols");
          maps.change(function() {
            var $selected, name, size;
            $selected = $(this).find("option:selected");
            name = $selected.val();
            log.debug(name);
            if (name.length === 0 || (name == null)) {
              $rows.val("");
              $name.val("");
              return $cols.val("");
            } else {
              size = name.match(/(\d+x\d+)/g)[0].split("x");
              $rows.val(size[0]);
              $cols.val(size[1]);
              return $name.val(name);
            }
          });
          _results = [];
          for (k in Maps) {
            v = Maps[k];
            opt = $("<option/>");
            opt.attr("value", k);
            opt.text(k);
            _results.push(maps.append(opt));
          }
          return _results;
        }
      });
    };
    return showSavePatternDialog = function(pattern) {
      return $("#save-pattern-form").dialog({
        autoOpen: true,
        height: 250,
        width: 200,
        modal: true,
        closeOnEscape: true,
        buttons: {
          "OK": function() {
            var name;
            name = $(this).find("#pattern-name").val();
            socket.emit("NEW_PATTERN", {
              name: name,
              pattern: pattern
            });
            return $(this).dialog("close");
          }
        },
        close: function() {
          return amjad.resume();
        }
      });
    };
  });

}).call(this);
