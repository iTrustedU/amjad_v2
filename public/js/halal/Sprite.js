(function() {
  "use strict";
  define(function() {
    var Sprite;
    Sprite = (function() {
      function Sprite(img, name, x, y, w, h) {
        var spl;
        this.img = img;
        this.name = name;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        spl = this.img.src.match(/\/assets\/sprites\/(.*\/)(.*)\.png/);
        this.name = spl[2];
        this.folder = spl[1];
      }

      return Sprite;

    })();
    return Sprite;
  });

}).call(this);
