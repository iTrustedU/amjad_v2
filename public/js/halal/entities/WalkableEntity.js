(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["../entities/RectBoundedEntity", "Vec2"], function(RectBoundedEntity, Vec2) {
    var Character;
    Character = (function(_super) {
      __extends(Character, _super);

      function Character(pos, sprite) {
        Character.__super__.constructor.call(this, pos, sprite);
        this.mass = 1;
        this.gravity = Vec2.fromValues(0, 0);
        this.velocity = Vec2.fromValues(0.0001, 0.0001);
        this.max_vel = 2;
        this.target = this.pos;
        Vec2.sub(this.velocity, this.target, this.pos);
        this.steering_f = Vec2.fromValues(0, 0);
        this.max_steering_f = 0.3;
        this.max_speed = 0.4;
        this.states = {
          SELECTED: 0,
          MOVING: 1,
          DEFAULT: 2
        };
        this.cur_state = this.states.DEFAULT;
      }

      Character.prototype.move = function(target) {
        var _this = this;
        if (!this.move_to) {
          return this.move_to = Hal.on("ENTER_FRAME", function(delta) {
            if ((Math.abs(_this.pos[0] - target[0]) < 1) && (Math.abs(_this.pos[1] - target[1]) < 1)) {
              Hal.removeTrigger("ENTER_FRAME", _this.move_to);
              return _this.move_to = null;
            } else {
              Vec2.lerp(_this.pos, _this.pos.slice(), target, delta / 1000);
              return _this.scene.trigger("ENTITY_MOVING", _this);
            }
          });
        }
      };

      Character.prototype.update = function(delta) {
        return Character.__super__.update.call(this, delta);
      };

      return Character;

    })(RectBoundedEntity);
    return Character;
  });

}).call(this);
