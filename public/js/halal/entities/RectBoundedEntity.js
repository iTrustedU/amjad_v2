(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Entity", "BBoxAlgos"], function(Entity, BBoxAlgos) {
    var RectBoundedEntity;
    RectBoundedEntity = (function(_super) {
      __extends(RectBoundedEntity, _super);

      function RectBoundedEntity(pos, spr) {
        this.sprite = Hal.asm.getSprite(spr);
        this.tint = false;
        this.inBounds = BBoxAlgos.rectBoundCheck;
        this.rectIntersectsShape = BBoxAlgos.rectIntersectsRect;
        RectBoundedEntity.__super__.constructor.call(this, pos, BBoxAlgos.rectBBoxFromSprite(this.sprite));
      }

      RectBoundedEntity.prototype.init = function() {
        var _this = this;
        return this.on("LEFT_CLICK", function(pos) {
          if (_this.inBounds(pos)) {
            _this.tint = true;
            return _this.tint_r = Hal.asm.tint(_this.sprite, "green");
          }
        });
      };

      RectBoundedEntity.prototype.update = function() {
        if (this.tint) {
          this.g.ctx.drawImage(this.tint_r, this.pos[0], this.pos[1]);
        } else {
          this.g.ctx.drawImage(this.sprite.img, this.pos[0], this.pos[1]);
        }
        if (this.scene.debug) {
          this.g.ctx.strokeRect(this.pos[0], this.pos[1], this.bounds[2], this.bounds[3]);
          return this.g.ctx.stroke();
        }
      };

      return RectBoundedEntity;

    })(Entity);
    return RectBoundedEntity;
  });

}).call(this);
