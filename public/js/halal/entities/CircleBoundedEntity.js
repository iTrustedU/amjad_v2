(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Entity", "BBoxAlgos"], function(Entity, BBoxAlgos) {
    var CircleBoundedEntity;
    CircleBoundedEntity = (function(_super) {
      __extends(CircleBoundedEntity, _super);

      function CircleBoundedEntity(pos, spr) {
        this.sprite = Hal.asm.getSprite(spr);
        this.tint = false;
        this.inBounds = BBoxAlgos.circularBoundCheck;
        this.rectIntersectsShape = BBoxAlgos.rectIntersectsCircle;
        CircleBoundedEntity.__super__.constructor.call(this, pos, BBoxAlgos.circularBBoxFromSprite(this.sprite));
      }

      CircleBoundedEntity.prototype.init = function() {
        var _this = this;
        return this.on("LEFT_CLICK", function(pos) {
          if (_this.inBounds(pos)) {
            _this.tint = true;
            return _this.tint_r = Hal.asm.tint(_this.sprite, "green");
          }
        });
      };

      CircleBoundedEntity.prototype.update = function() {
        if (this.tint) {
          this.g.ctx.drawImage(this.tint_r, this.pos[0] - this.sprite.w * 0.5, this.pos[1] - this.sprite.h * 0.5);
        } else {
          this.g.ctx.drawImage(this.sprite.img, this.pos[0] - this.sprite.w * 0.5, this.pos[1] - this.sprite.h * 0.5);
        }
        if (this.scene.debug) {
          this.g.ctx.beginPath();
          this.g.ctx.arc(this.pos[0], this.pos[1], this.bounds[0], 0, Math.PI * 2);
          this.g.ctx.closePath();
          return this.g.ctx.stroke();
        }
      };

      return CircleBoundedEntity;

    })(Entity);
    return CircleBoundedEntity;
  });

}).call(this);
