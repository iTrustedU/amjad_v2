"use strict"

define ["Entity", "BBoxAlgos"],

(Entity, BBoxAlgos) ->

    class CircleBoundedEntity extends Entity
        constructor: (pos, spr) ->
            @sprite = Hal.asm.getSprite(spr)
            @tint = false
            @inBounds = BBoxAlgos.circularBoundCheck
            @rectIntersectsShape = BBoxAlgos.rectIntersectsCircle

            super(pos, BBoxAlgos.circularBBoxFromSprite(@sprite))
            
        init: () ->
            @on "LEFT_CLICK", (pos) =>
                if @inBounds(pos)
                    @tint = true
                    @tint_r = Hal.asm.tint(@sprite, "green")

        update: () ->
            if @tint
                @g.ctx.drawImage(@tint_r, @pos[0] - @sprite.w * 0.5, @pos[1] - @sprite.h * 0.5)
            else
                @g.ctx.drawImage(@sprite.img, @pos[0] - @sprite.w * 0.5, @pos[1] - @sprite.h * 0.5)

            if @scene.debug
                @g.ctx.beginPath()
                @g.ctx.arc(@pos[0], @pos[1], @bounds[0], 0, Math.PI*2)
                @g.ctx.closePath()
                @g.ctx.stroke()

    return CircleBoundedEntity