"use strict"

define ["Entity", "BBoxAlgos"],

(Entity, BBoxAlgos) ->

    class RectBoundedEntity extends Entity
        constructor: (pos, spr) ->
            @sprite = Hal.asm.getSprite(spr)  
            @tint = false
            @inBounds = BBoxAlgos.rectBoundCheck
            @rectIntersectsShape = BBoxAlgos.rectIntersectsRect

            super(pos, BBoxAlgos.rectBBoxFromSprite(@sprite))

        init: () ->
            @on "LEFT_CLICK", (pos) =>
                if @inBounds(pos)
                    @tint = true
                    @tint_r = Hal.asm.tint(@sprite, "green")

        update: () ->
            if @tint
                @g.ctx.drawImage(@tint_r, @pos[0], @pos[1])
            else
                @g.ctx.drawImage(@sprite.img, @pos[0], @pos[1])

            if @scene.debug
                @g.ctx.strokeRect(@pos[0], @pos[1], @bounds[2], @bounds[3])
                @g.ctx.stroke()

    return RectBoundedEntity