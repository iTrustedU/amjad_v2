(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["Entity", "BBoxAlgos"], function(Entity, BBoxAlgos) {
    var SpriteEntity;
    SpriteEntity = (function(_super) {
      __extends(SpriteEntity, _super);

      function SpriteEntity(pos, spr) {
        this.sprite = Hal.asm.getSprite(spr);
        this.tint = false;
        SpriteEntity.__super__.constructor.call(this, pos, BBoxAlgos.rectBBoxFromSprite(this.sprite));
      }

      SpriteEntity.prototype.update = function(delta) {
        if (this.tint) {
          return this.g.ctx.drawImage(this.tint_r, this.pos[0], this.pos[1]);
        } else {
          return this.g.ctx.drawImage(this.sprite.img, this.pos[0], this.pos[1]);
        }
      };

      return SpriteEntity;

    })(Entity);
    return SpriteEntity;
  });

}).call(this);
