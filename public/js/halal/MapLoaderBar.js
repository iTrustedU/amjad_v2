(function() {
  "use strict";
  define(["jquery-ui"], function($) {
    var MapLoaderBar;
    MapLoaderBar = (function() {
      function MapLoaderBar(map, clb) {
        var $prbar, $prbartext,
          _this = this;
        this.map = map;
        this.clb = clb;
        $prbar = $('.floating-bar');
        $prbartext = $("#floating-bar-text");
        this.starttime = 0;
        this.maploadstartlist = this.map.on("MAP_LOADING_STARTED", function(len) {
          $prbartext.text("Loading map: " + _this.map.name);
          log.debug("started");
          _this.starttime = performance.now();
          return $prbar.show();
        });
        this.maploadedlist = this.map.on("MAP_LOADED", function() {
          log.debug("finished");
          log.debug("it took " + (performance.now() - _this.starttime) / 1000 + "s");
          $prbar.fadeOut();
          if (_this.clb != null) {
            return _this.clb();
          }
        });
      }

      return MapLoaderBar;

    })();
    return MapLoaderBar;
  });

}).call(this);
