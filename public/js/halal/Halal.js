(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["AssetManager", "EventDispatcher", "SceneManager", "DOMManager", "Renderer", "DOMEventManager", "MathUtil", "ImgUtils"], function(AssetManager, EventDispatcher, SceneManager, DOMManager, Renderer, DOMEventManager, MathUtil, ImgUtils) {
    var Halal, cur_fps_time, cur_time, delta, engineLoop, fps, fps_counter, fps_trigger_time, last_frame_id;
    window.requestAnimFrame = (function() {
      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
        return window.setTimeout(callback, 1);
      };
    })();
    if (window.performance == null) {
      window.performance = Date;
    }
    cur_time = 0;
    delta = 0;
    fps_trigger_time = 1000;
    cur_fps_time = 0;
    fps_counter = 0;
    fps = 0;
    last_frame_id = 0;
    Halal = (function(_super) {
      __extends(Halal, _super);

      function Halal() {
        Halal.__super__.constructor.call(this);
        this.uid = 0;
        this.paused = true;
        this.debug_mode = false;
        this.pressed_keys = [];
        Halal.prototype.Keys = {
          SHIFT: 16,
          G: 71,
          D: 68,
          W: 87,
          C: 67,
          I: 73,
          M: 77,
          ONE: 49,
          TWO: 50,
          THREE: 51,
          FOUR: 52,
          DELETE: 46,
          LEFT: 37,
          RIGHT: 39,
          UP: 38,
          DOWN: 40,
          F: 70
        };
      }

      return Halal;

    })(EventDispatcher);
    Halal.prototype.supports = function(feature) {
      return this.trigger("SUPPORTS_" + feature);
    };
    Halal.prototype.init = function() {
      cur_time = performance.now();
      delta = 0;
      fps_trigger_time = 1000;
      cur_fps_time = 0;
      fps_counter = 0;
      fps = 0;
      return last_frame_id = 0;
    };
    Halal.prototype.start = function() {
      this.init();
      this.paused = false;
      engineLoop();
      return this.trigger("ENGINE_STARTED");
    };
    Halal.prototype.drawInfo = function() {
      return this.glass.ctx.fillText("FPS: " + fps, 0, 10);
    };
    engineLoop = function() {
      var sc, _i, _len, _ref;
      delta = performance.now() - cur_time;
      cur_fps_time += delta;
      Hal.trigger("ENTER_FRAME", delta);
      _ref = Hal.scm.scenes;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        sc = _ref[_i];
        if (!sc.paused) {
          sc.update(delta);
        }
      }
      if (Hal.debug_mode) {
        Hal.drawInfo();
      }
      if (cur_fps_time >= fps_trigger_time) {
        fps = fps_counter;
        cur_fps_time = 0;
        fps_counter = 0;
        Hal.trigger("FPS_UPDATE", fps);
      }
      Hal.trigger("ENTER_POST_FRAME");
      last_frame_id = requestAnimFrame(engineLoop);
      cur_time = performance.now();
      return fps_counter++;
    };
    Halal.prototype.UID = function() {
      return ++this.uid;
    };
    window.Hal = new Halal();
    Hal.asm = new AssetManager();
    Hal.dom = new DOMManager();
    Hal.r = new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], null, 0);
    Hal.glass = new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], null, 11);
    Hal.scm = new SceneManager();
    Hal.evm = new DOMEventManager();
    Hal.m = MathUtil;
    Hal.im = new ImgUtils();
    return Hal;
  });

}).call(this);
