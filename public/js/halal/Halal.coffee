"use strict"

define ["AssetManager", "EventDispatcher", "SceneManager", "DOMManager", "Renderer", "DOMEventManager", "MathUtil", "ImgUtils"],

(AssetManager, EventDispatcher, SceneManager, DOMManager, Renderer, DOMEventManager, MathUtil, ImgUtils) ->

    window.requestAnimFrame = do () ->
        return  window.requestAnimationFrame       or
                window.webkitRequestAnimationFrame or
                window.mozRequestAnimationFrame    or
                (callback) ->
                    window.setTimeout(callback, 1)

    if not window.performance?
        window.performance = Date

    cur_time = 0
    delta = 0
    fps_trigger_time = 1000
    cur_fps_time = 0
    fps_counter = 0
    fps = 0
    last_frame_id = 0

    class Halal extends EventDispatcher
        constructor: () ->
            super()
            @uid = 0
            @paused = true
            @debug_mode = false
            @pressed_keys = []
            Halal::Keys = 
                SHIFT: 16
                G: 71
                D: 68
                W: 87
                C: 67
                I: 73
                M: 77
                ONE: 49
                TWO: 50
                THREE: 51
                FOUR: 52
                DELETE: 46
                LEFT: 37
                RIGHT: 39
                UP: 38
                DOWN: 40
                F: 70

    Halal::supports = (feature) ->
        @trigger "SUPPORTS_#{feature}"

    # Halal::isKeyPressed = (key) ->
    #     return @pressed_keys[Halal::Keys[key.toUpperCase()]]

    Halal::init = () ->
        # Hal.on "KEY_DOWN", (ev) ->
        #     @pressed_keys[ev.keyCode] = true

        # Hal.on "KEY_UP", (ev) ->
        #     @pressed_keys[ev.keyCode] = false

        cur_time = performance.now()
        delta = 0
        fps_trigger_time = 1000
        cur_fps_time = 0
        fps_counter = 0
        fps = 0
        last_frame_id = 0

        #@on "KEY_UP", (key) =>
            #if key.keyCode is Hal.Keys.I
            #    @debug_mode = not @debug_mode
            #    Hal.trigger "DEBUG_MODE", @debug_mode

    Halal::start = () ->
        @init()
        @paused = false
        engineLoop()
        @trigger "ENGINE_STARTED"

    Halal::drawInfo = () ->
        @glass.ctx.fillText("FPS: #{fps}", 0, 10)

    engineLoop = () ->
        delta = performance.now() - cur_time
        cur_fps_time += delta

        Hal.trigger "ENTER_FRAME", delta

        for sc in Hal.scm.scenes
            sc.update(delta) if not sc.paused
            
        if Hal.debug_mode
            Hal.drawInfo()

        if cur_fps_time >= fps_trigger_time
            fps = fps_counter
            cur_fps_time = 0
            fps_counter = 0
            Hal.trigger "FPS_UPDATE", fps

        Hal.trigger "ENTER_POST_FRAME"

        last_frame_id = requestAnimFrame(engineLoop)
        cur_time = performance.now()
        fps_counter++

    Halal::UID = () ->
        return ++@uid

    window.Hal = new Halal()
    Hal.asm = new AssetManager()
    Hal.dom = new DOMManager()
    Hal.r = new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], null, 0)
    Hal.glass = new Renderer([0, 0, Hal.dom.area.width, Hal.dom.area.height], null, 11)
    Hal.scm = new SceneManager()
    Hal.evm = new DOMEventManager()
    Hal.m = MathUtil
    Hal.im = new ImgUtils()
    return Hal