(function() {
  "use strict";
  define(["Vec2"], function(Vec2) {
    var Camera;
    Camera = (function() {
      function Camera(ctx, pos, w, h, scene) {
        var _this = this;
        this.ctx = ctx;
        this.pos = pos;
        this.w = w;
        this.h = h;
        this.scene = scene;
        this.dragging = false;
        this.start_drag_point = [0, 0];
        this.camera_prev_pos = [this.pos[0], this.pos[1]];
        this.zoom = 1;
        this.zoom_step = 0.1;
        this.camera_speed = 3.8;
        this.bounds = [0, 0, this.w, this.h];
        this.center_point = [this.scene.bounds[2] / 2, this.scene.bounds[3] / 2];
        this.lerp_to = this.center_point.slice();
        this.view_frustum = [0, 0, this.scene.bounds[2], this.scene.bounds[3]];
        this.scene.on("ENTER_FULLSCREEN", function(scale) {
          _this.zoom = scale[0];
          log.debug("zoom factor: " + _this.zoom);
          _this.bounds = [0, 0, _this.w * _this.zoom, _this.h * _this.zoom];
          _this.center_point = [_this.scene.bounds[2] / 2, _this.scene.bounds[3] / 2];
          return log.info(_this.center_point);
        });
        this.scene.on("EXIT_FULLSCREEN", function(scale) {
          _this.zoom = scale[0];
          _this.bounds = [0, 0, _this.w * _this.zoom, _this.h * _this.zoom];
          return _this.center_point = [_this.scene.bounds[2] / 2, _this.scene.bounds[3] / 2];
        });
      }

      Camera.prototype.enableDrag = function() {
        var _this = this;
        this.drag_started = Hal.on("DRAG_STARTED", function(pos) {
          if (_this.scene.paused) {
            return;
          }
          _this.dragging = true;
          _this.start_drag_point = pos.slice();
          return _this.camera_prev_pos = _this.pos.slice();
        });
        this.drag_ended = Hal.on("DRAG_ENDED", function(pos) {
          return _this.dragging = false;
        });
        return this.drag = Hal.on("MOUSE_MOVE", function(pos) {
          if (_this.scene.paused) {
            return;
          }
          if (_this.dragging) {
            _this.pos[0] = _this.camera_prev_pos[0] + (pos[0] - _this.start_drag_point[0]);
            _this.pos[1] = _this.camera_prev_pos[1] + (pos[1] - _this.start_drag_point[1]);
            return _this.scene.trigger("CAMERA_MOVED", _this.pos);
          }
        });
      };

      Camera.prototype.isVisible = function(ent) {
        return Hal.m.rectIntersectsRect([ent.pos[0] * this.zoom + this.pos[0], ent.pos[1] * this.zoom + this.pos[1], ent.bounds[2], ent.bounds[3]], this.bounds);
      };

      Camera.prototype.enableZoom = function() {
        var _this = this;
        return this.zoom_trig = Hal.on("SCROLL", function(ev) {
          if (_this.scene.paused) {
            return;
          }
          if (_this.scene.paused) {
            return;
          }
          if (ev.down) {
            _this.zoom -= _this.zoom_step;
          } else {
            _this.zoom += _this.zoom_step;
          }
          _this.scene.trigger("CAMERA_MOVED", _this.pos);
          return _this.scene.trigger("ZOOM", _this.zoom);
        });
      };

      Camera.prototype.setViewFrustum = function(bnds) {
        this.view_frustum[0] = -bnds[0];
        this.view_frustum[1] = -bnds[1];
        this.view_frustum[2] = -bnds[2];
        this.view_frustum[3] = -bnds[3];
        log.debug("view frustum setted");
        return log.debug(this.view_frustum);
      };

      Camera.prototype.enableArrowKeys = function() {
        var _this = this;
        return this.arrkeys = Hal.on("KEY_DOWN", function(ev) {
          if (ev.keyCode === Hal.Keys.LEFT) {
            return _this.lerpTo([_this.center_point[0] - _this.camera_speed * 50, _this.center_point[1]]);
          } else if (ev.keyCode === Hal.Keys.RIGHT) {
            return _this.lerpTo([_this.center_point[0] + _this.camera_speed * 50, _this.center_point[1]]);
          } else if (ev.keyCode === Hal.Keys.UP) {
            return _this.lerpTo([_this.center_point[0], _this.center_point[1] - _this.camera_speed * 50]);
          } else if (ev.keyCode === Hal.Keys.DOWN) {
            return _this.lerpTo([_this.center_point[0], _this.center_point[1] + _this.camera_speed * 50]);
          }
        });
      };

      Camera.prototype.disableArrowKeys = function() {
        return Hal.removeTrigger("KEY_DOWN", this.arrkeys);
      };

      Camera.prototype.enableLerp = function() {
        return this.lerpTo = function(pos) {
          var lx, ly,
            _this = this;
          if (this.scene.paused) {
            return;
          }
          lx = this.center_point[0] - pos[0] + this.pos[0];
          ly = this.center_point[1] - pos[1] + this.pos[1];
          if (lx > this.view_frustum[0] * this.zoom) {
            lx = this.view_frustum[0] * this.zoom;
          }
          if ((lx - this.bounds[2]) < this.view_frustum[2] * this.zoom) {
            lx = this.view_frustum[2] * this.zoom + this.bounds[2];
          }
          if (ly > this.view_frustum[1] * this.zoom) {
            ly = this.view_frustum[1] * this.zoom;
          }
          if (ly - this.bounds[3] < this.view_frustum[3] * this.zoom) {
            ly = this.view_frustum[3] * this.zoom + this.bounds[3];
          }
          this.lerp_to[0] = lx;
          this.lerp_to[1] = ly;
          if (!this.lerp_anim) {
            return this.lerp_anim = Hal.on("ENTER_FRAME", function(delta) {
              if ((~~Math.abs(_this.pos[0] - _this.lerp_to[0]) + ~~Math.abs(-_this.pos[1] + _this.lerp_to[1])) < 2) {
                Hal.removeTrigger("ENTER_FRAME", _this.lerp_anim);
                return _this.lerp_anim = null;
              } else {
                Vec2.lerp(_this.pos, _this.pos.slice(), _this.lerp_to, delta / 1000 * _this.camera_speed);
                return _this.scene.trigger("CAMERA_MOVED", _this.pos);
              }
            });
          }
        };
      };

      Camera.prototype.lerpTo = function() {};

      Camera.prototype.disableLerp = function() {
        return this.lerpTo = function() {};
      };

      Camera.prototype.disableZoom = function() {
        return Hal.removeTrigger("SCROLL", this.zoom_trig);
      };

      Camera.prototype.disableDrag = function() {
        Hal.removeTrigger("DRAG_STARTED", this.drag_started);
        Hal.removeTrigger("DRAG_ENDED", this.drag_ended);
        return Hal.removeTrigger("MOUSE_MOVE", this.drag);
      };

      return Camera;

    })();
    return Camera;
  });

}).call(this);
