(function() {
  "use strict";
  define(["Scene"], function(Scene) {
    var SceneManager;
    SceneManager = (function() {
      function SceneManager() {
        var _this = this;
        this.scenes = [];
        this.max_depth = 0;
        this.top_scene = null;
        this.fscale = [1, 1];
        Hal.on("ENGINE_PAUSED", function() {
          return _this.pauseAll();
        });
        Hal.on("ENGINE_RESUMED", function() {
          return _this.unpauseAll();
        });
        Hal.on("MOUSE_MOVE", function(pos) {
          var high, scene, _i, _len, _ref;
          pos[0] = pos[0] / _this.fscale[0];
          pos[1] = pos[1] / _this.fscale[1];
          _ref = _this.scenes;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            scene = _ref[_i];
            scene.in_focus[0] = false;
            if (!scene.paused && scene.inBounds(pos)) {
              high = scene;
            }
          }
          if (high == null) {
            return;
          }
          _this.top_scene = high;
          _this.top_scene.in_focus[0] = true;
          _this.top_scene.mpos[0] = ((pos[0] - _this.top_scene.x) - _this.top_scene.camera.pos[0]) / _this.top_scene.camera.zoom;
          _this.top_scene.mpos[1] = ((pos[1] - _this.top_scene.y) - _this.top_scene.camera.pos[1]) / _this.top_scene.camera.zoom;
          return _this.top_scene.trigger("MOUSE_MOVE", _this.top_scene.mpos);
        });
        Hal.on("RESIZE", function(scale) {
          var scene, _i, _len, _ref, _results;
          _ref = _this.scenes;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            scene = _ref[_i];
            _results.push(scene.trigger("RESIZE", scale));
          }
          return _results;
        });
        Hal.on("SHOW_NEXT_SCENE", function() {
          return _this.nextScene();
        });
        Hal.on("SHOW_PREV_SCENE", function() {});
      }

      SceneManager.prototype.nextScene = function() {
        var ind;
        ind = this.scenes.indexOf(this.top_scene);
        this.top_scene.pause();
        this.top_scene = this.scenes[(ind + 1) % this.scenes.length];
        return this.top_scene.resume();
      };

      SceneManager.prototype.enterFullScreen = function(fscale) {
        var sc, _i, _len, _ref, _results;
        this.fscale = fscale;
        Hal.trigger("ENTER_FULLSCREEN");
        _ref = this.scenes;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          sc = _ref[_i];
          _results.push(sc.trigger("ENTER_FULLSCREEN", this.fscale));
        }
        return _results;
      };

      SceneManager.prototype.exitFullScreen = function(fscale) {
        var sc, _i, _len, _ref, _results;
        this.fscale = fscale;
        Hal.trigger("EXIT_FULLSCREEN");
        _ref = this.scenes;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          sc = _ref[_i];
          _results.push(sc.trigger("EXIT_FULLSCREEN", this.fscale));
        }
        return _results;
      };

      SceneManager.prototype.addSceneOnTop = function(sceneA, sceneB) {
        var depth, i, scene, z, zFrom, _i, _len, _ref;
        i = this.scenes.indexOf(sceneA);
        if (i === -1) {
          return;
        }
        depth = this.scenes[i].depth;
        zFrom = this.scenes[i].g.canvas.style["z-index"];
        _ref = this.findTopScenes(depth);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          scene = _ref[_i];
          z = scene.g.canvas.style["z-index"];
          scene.g.canvas.style["z-index"] = +z + 1;
          scene.depth++;
        }
        sceneB.depth = depth + 1;
        sceneB.g.canvas.style["z-index"] = +zFrom + 1;
        return this.addScene(sceneB);
      };

      SceneManager.prototype.getSceneByName = function(name) {
        var scene, _i, _len, _ref;
        _ref = this.scenes;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          scene = _ref[_i];
          if (scene.name === name) {
            return scene;
          }
        }
        return null;
      };

      SceneManager.prototype.putOnTop = function(scene) {
        this.sortScenes();
        if ((this.top_scene == null) || scene.depth === this.max_depth) {
          return;
        }
        scene.depth = this.max_depth + 1;
        this.pauseAll();
        scene.resume();
        return this.sortScenes();
      };

      SceneManager.prototype.findTopScenes = function(fromDepth) {
        var out, scene, _i, _len, _ref;
        out = [];
        _ref = this.scenes;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          scene = _ref[_i];
          if (scene.depth > fromDepth) {
            out.push(scene);
          }
        }
        return out;
      };

      SceneManager.prototype.findBelowScenes = function(fromDepth) {
        var out, scene, _i, _len, _ref;
        out = [];
        _ref = this.scenes;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          scene = _ref[_i];
          if (scene.depth < fromDepth) {
            out.push(scene);
          }
        }
        return out;
      };

      SceneManager.prototype.addScene = function(scene) {
        var _this = this;
        if (!(scene instanceof Scene)) {
          return null;
        }
        if (!scene.name) {
          scene.name = "#scene" + "_" + scene.id;
        }
        if (!scene.bounds) {
          scene.bounds = [0, 0, Hal.dom.renderspaceRect.width, Hal.dom.renderspaceRect.height];
        }
        if (!scene.depth) {
          scene.depth = this.max_depth + 1;
        }
        scene.on("SCENE_PAUSED", function(x) {});
        scene.on("SCENE_RESUMED", function(x) {});
        scene.init();
        Hal.trigger("SCENE_ADDED_" + scene.name.toUpperCase(), scene);
        this.pauseAll();
        this.scenes.unshift(scene);
        this.sortScenes();
        log.debug("added scene: " + scene.name);
        return scene;
      };

      SceneManager.prototype.removeScene = function(scene) {
        var ind;
        scene.removeAllTriggers();
        ind = this.scenes.indexOf(scene);
        if (ind !== -1) {
          this.scenes.splice(ind, 1);
          return log.debug("removed scene " + scene.name);
        } else {
          return log.debug("couldn't remove scene " + scene.name);
        }
      };

      SceneManager.prototype.sortScenes = function() {
        this.scenes.sort(function(el1, el2) {
          return el1.depth > el2.depth;
        });
        this.top_scene = this.scenes[this.scenes.length - 1];
        return this.max_depth = this.top_scene.depth;
      };

      SceneManager.prototype.pauseAll = function() {
        var scene, _i, _len, _ref, _results;
        log.debug("paused all");
        _ref = this.scenes;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          scene = _ref[_i];
          _results.push(scene.pause());
        }
        return _results;
      };

      SceneManager.prototype.unpauseAll = function() {
        var scene, _i, _len, _ref, _results;
        log.debug("resumed all");
        _ref = this.scenes;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          scene = _ref[_i];
          _results.push(scene.resume());
        }
        return _results;
      };

      return SceneManager;

    })();
    return SceneManager;
  });

}).call(this);
