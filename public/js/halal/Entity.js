(function() {
  "use strict";
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(["EventDispatcher"], function(EventDispatcher) {
    var Entity;
    Entity = (function(_super) {
      __extends(Entity, _super);

      function Entity(pos, bounds) {
        this.pos = pos;
        this.bounds = bounds;
        this.scene = null;
        this.g = null;
        this.in_focus = false;
        this.id = Hal.UID();
        this.quadspace = null;
        Entity.__super__.constructor.call(this);
      }

      Entity.prototype.init = function() {};

      return Entity;

    })(EventDispatcher);
    Entity.prototype.inBounds = function(point) {
      return Hal.m.isPointInRect(point, this.bounds);
    };
    return Entity;
  });

}).call(this);
