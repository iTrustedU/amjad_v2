"use strict"

define ["EventDispatcher", "Renderer", "Camera", "QuadTree"], 

(EventDispatcher, Renderer, Camera, QuadTree) ->
    class Scene extends EventDispatcher
        constructor: (@name, @bounds, renderer) ->
            super()

            @paused     = false
            @id         = Hal.UID()
            @in_focus   = [false]
            @mpos       = [0, 0]
            @depth      = 0
            @x          = @bounds[0]
            @y          = @bounds[1]
            @ents       = []
            @scale      = 1
            @ent_cache  = {}

            @g = renderer || Hal.r
            @debug = false
            @initHandlers() 
            @addCamera()

        resetQuadSpace: (dim) ->
            @quadspace = null
            @quadspace = new QuadTree(dim)
            @quadspace.divide()

        addCamera: () ->
            @camera = new Camera(@g.ctx, [0, 0], @bounds[2], @bounds[3], @)
            @camera.enableDrag()
            @camera.enableLerp()
            @camera.enableZoom()

        resume: () ->
            @paused = false
            Hal.trigger "SCENE_RESUMED", @
            log.info "#{@name} resumed"

        pause: () ->
            @paused = true
            Hal.trigger "SCENE_PAUSED", @
            log.info "#{@name} paused"

        initHandlers: () ->
            @on "ZOOM", (scale) => 
                @scale = scale
                log.info "zoom happened: " + scale

            @on "RESIZE", (scale) =>
                log.info "resize happened: " + scale

            @on "ENTER_FULLSCREEN", (scale) =>
                @scale = scale[0]
                @bounds[2] = Hal.dom.area.width
                @bounds[3] = Hal.dom.area.height

                log.info (@.name + " in fullscreen")
                log.info ("scale: " + scale[0] + "," + scale[1])
                log.info @bounds

            @on "EXIT_FULLSCREEN", (scale) =>
                @scale = scale[0]
                @bounds[2] = Hal.dom.area.width
                @bounds[3] = Hal.dom.area.height

                log.info (@.name + " exited fullscreen")
                log.info ("scale: " + scale[0] + "," + scale[1])
                log.info @bounds

            @on "KEY_UP", (key) ->
                if key.keyCode == Hal.Keys.LEFT
                    Hal.trigger "SHOW_PREV_SCENE"
                else if key.keyCode == Hal.Keys.RIGHT
                    Hal.trigger "SHOW_NEXT_SCENE"

            Hal.on "DEBUG_MODE", (debug) =>
                @debug = debug
                log.info "debug mode: #{@debug}"

            @on "ENTITY_MOVING", (ent) =>
                if not Hal.m.isPointInRect(ent.pos, ent.quadspace.bounds)
                    ent.quadspace.remove(ent)
                    @quadspace.insert(ent)

        drawStat: () ->
            Hal.glass.ctx.fillText("Camera: #{@camera.pos[0]}, #{@camera.pos[1]}", 0, 20)
            Hal.glass.ctx.fillText("Zoom: #{@camera.zoom}", 0, 30)
            Hal.glass.ctx.fillText("Mouse: #{@mpos[0]}, #{@mpos[1]}", 0, 40)
            Hal.glass.ctx.fillText("Num of entities: #{@ents.length}", 0, 50)
        
        drawQuadTree: (quadspace) ->
            if quadspace.nw?
                @drawQuadTree(quadspace.nw)
                @g.ctx.strokeRect(quadspace.nw.bounds[0], quadspace.nw.bounds[1], quadspace.nw.bounds[2], quadspace.nw.bounds[3])
            if quadspace.ne?
                @drawQuadTree(quadspace.ne)
                @g.ctx.strokeRect(quadspace.ne.bounds[0], quadspace.ne.bounds[1], quadspace.ne.bounds[2], quadspace.ne.bounds[3])
            if quadspace.sw?
                @drawQuadTree(quadspace.sw)
                @g.ctx.strokeRect(quadspace.sw.bounds[0], quadspace.sw.bounds[1], quadspace.sw.bounds[2], quadspace.sw.bounds[3])
            if quadspace.se?
                @drawQuadTree(quadspace.se)
                @g.ctx.strokeRect(quadspace.se.bounds[0], quadspace.se.bounds[1], quadspace.se.bounds[2], quadspace.se.bounds[3])

        update: (delta) ->
            #ovo mogu izbaciti u kameru
            @g.ctx.setTransform(@camera.zoom, 0, 0, @camera.zoom, @camera.pos[0], @camera.pos[1])
            if @debug
                @drawStat()
                @g.ctx.save()
                @g.ctx.strokeStyle = "blue"
                @drawQuadTree(@quadspace)
                @g.ctx.stroke()
                @g.ctx.restore()

        inBounds: (point) ->
            return Hal.m.isPointInRect(point, @bounds)

        drawBounds: () ->
            @g.strokeRect(@bounds)

        addEntity: (ent) ->
            if @exists(ent.id)
                return
            ent.scene = @
            ent.g = @g
            ent.pos[0] -= (@camera.pos[0] / @camera.zoom)
            ent.pos[1] -= (@camera.pos[1] / @camera.zoom)
            ent.init()
            @ent_cache[ent.id] = true
            @ents.push(ent)
            return ent

        removeEntity: (ent) ->
            ind = @ents.indexOf(ent)
            if ind != -1
                @ent_cache[ent.id] = false
                @ents[ind] = null
                @ents.splice(ind)

        exists: (id) ->
            return @ent_cache[id]

        move: (x, y) ->
            @bounds[0] = x
            @bounds[1] = y
            @x = x
            @y = y
            @g.offsetX = x
            @g.offsetY = y

    return Scene