(function() {
  "use strict";
  define(["jquery-ui"], function($) {
    var AssetLoaderBar;
    AssetLoaderBar = (function() {
      function AssetLoaderBar(clb) {
        var $prbar, $prbartext, $viewport,
          _this = this;
        this.clb = clb;
        $prbar = $('.floating-bar');
        $prbartext = $("#floating-bar-text");
        $viewport = $('#viewport');
        this.starttime = 0;
        this.total = 0;
        this.loaded = 0;
        this.spritesloadstartlist = Hal.asm.on("SPRITES_LOADING", function(len) {
          _this.total += len;
          _this.starttime = performance.now();
          return $prbar.show();
        });
        this.spritesloadedlist = Hal.asm.on("SPRITES_LOADED", function() {
          log.debug("finished loading sprites");
          log.debug("it took " + (performance.now() - _this.starttime) / 1000 + "s");
          $prbar.fadeOut();
          return $viewport.animate({
            opacity: "+=1"
          }, 2000, function() {
            $viewport.css({
              'opacity': '255'
            });
            $viewport.css({
              'background': 'transparent'
            });
            if (_this.clb != null) {
              _this.clb();
            }
            Hal.asm.removeTrigger("SPRITES_LOADED", _this.spritesloadedlist);
            Hal.asm.removeTrigger("SPRITE_LOADED", _this.spriteloaded);
            return Hal.asm.removeTrigger("SPRITES_LOADING", _this.spritesloadstartlist);
          });
        });
        this.spriteloaded = Hal.asm.on("SPRITE_LOADED", function(spr) {
          _this.loaded++;
          return $prbartext.text("Loading sprite: " + spr);
        });
      }

      return AssetLoaderBar;

    })();
    return AssetLoaderBar;
  });

}).call(this);
