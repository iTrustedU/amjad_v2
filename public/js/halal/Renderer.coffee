"use strict"

define [],

() ->
    
    class Renderer 
        constructor: (@bounds, canvas, z) ->
            if canvas? 
                @canvas     = canvas
            else 
                @canvas     = Hal.dom.createCanvasLayer(z)
                @bounds[0]  = 0
                @bounds[1]  = 0
                Hal.dom.addCanvas(@canvas, @bounds[0], @bounds[1], true)
            
            @ctx = @canvas.getContext("2d")

    Renderer::resize = (w, h) ->
        @canvas.width = w
        @canvas.height = h
        @prev_bounds = @bounds.slice()
        @bounds[2] = w
        @bounds[3] = h
    
    # Renderer::drawSprite = (sprite, x, y, dx = 0, dy = 0) ->
    #     @ctx.drawImage(sprite.img, sprite.x, sprite.y, sprite.w-2, sprite.h-2, ~~(x+@bounds[0]-dx || 0), ~~(y+@bounds[1]-dy+@camera[1] || 0), sprite.w+dx, sprite.h+dy)

    # #style izbaciti, jako lose utice ako se ovako radi
    # Renderer::drawPolygon = (points) ->
    #     @ctx.beginPath()
    #     @ctx.moveTo(points[0][0] + @bounds[0] + @camera[0], points[0][1] + @bounds[1] + @camera[1])
    #     for p in points[1..]
    #         @ctx.lineTo(p[0] + @bounds[0] + @camera[0] , p[1] + @bounds[1] + @camera[1])
    #     @ctx.closePath()
    #     @ctx.stroke()

    # Renderer::fillPolygon = (points, color) ->
    #     @ctx.save()
    #     @ctx.beginPath()
    #     @ctx.fillStyle = color
    #     @ctx.moveTo(points[0][0] + @bounds[0] + @camera[0], points[0][1] + @bounds[1] + @camera[1])
    #     for p in points[1..]
    #         @ctx.lineTo(p[0] + @bounds[0] + @camera[0] , p[1] + @bounds[1] + @camera[1])
    #     @ctx.closePath()
    #     @ctx.fill()
    #     @ctx.restore()

    # Renderer::resize = (w, h) ->
    #     @canvas.width = w
    #     @canvas.height = h
    #     @prev_bounds = @bounds.slice()
    #     @bounds[2] = w
    #     @bounds[3] = h

    # Renderer::drawPoint = (p) ->
    #     @ctx.fillRect(p[0] + @bounds[0], p[1] + @bounds[1] , 2, 2)
    #     @ctx.fill()

    # Renderer::drawPoints = (pts) ->
    #     @drawPoint(p) for p in pts

    # Renderer::drawLine = (a, b) ->
    #     @ctx.beginPath()
    #     @ctx.moveTo(a[0], a[1])
    #     @ctx.lineTo(b[0], b[1])
    #     @ctx.closePath()
    #     @ctx.stroke()

    # Renderer::fillRect = (rect) ->
    #     @ctx.fillRect(rect[0]  + @bounds[0] + @camera[0], rect[1] + @bounds[1] + @camera[1], rect[2], rect[3])
    #     @ctx.fill()

    # Renderer::strokeRect = (rect) ->
    #     @ctx.strokeRect(rect[0]  + @bounds[0] + @camera[0], rect[1] + @bounds[1] + @camera[1], rect[2], rect[3])
    #     @ctx.stroke()

    # Renderer::clearRect = (rect) ->
    #     @ctx.clearRect(@camera[0] + @bounds[0] + rect[0], @camera[1] + @bounds[1] + rect[1], rect[2], rect[3])

    # Renderer::clear = () ->
    #     @ctx.clearRect(@bounds[0], @bounds[1], @bounds[2], @bounds[3])


    return Renderer