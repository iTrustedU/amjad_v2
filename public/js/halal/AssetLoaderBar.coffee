"use strict"

define ["jquery-ui"], 
($) ->
  class AssetLoaderBar
    constructor: (@clb) ->
      $prbar = $('.floating-bar')
      $prbartext = $("#floating-bar-text")
      $viewport = $('#viewport')
      @starttime = 0
      @total = 0
      @loaded = 0

      @spritesloadstartlist = 
        Hal.asm.on "SPRITES_LOADING", (len) =>
          @total += len
          @starttime = performance.now()
          $prbar.show()

      @spritesloadedlist = 
        Hal.asm.on "SPRITES_LOADED", () =>
            log.debug "finished loading sprites"
            log.debug "it took " + (performance.now() - @starttime) / 1000 + "s"
            $prbar.fadeOut()
            $viewport.animate({
                opacity: "+=1"
            }, 2000, () => 
                $viewport.css({'opacity': '255'});
                $viewport.css({'background': 'transparent'})
                @clb() if @clb?
                Hal.asm.removeTrigger "SPRITES_LOADED", @spritesloadedlist
                Hal.asm.removeTrigger "SPRITE_LOADED", @spriteloaded
                Hal.asm.removeTrigger "SPRITES_LOADING", @spritesloadstartlist
            )

      @spriteloaded =
        Hal.asm.on "SPRITE_LOADED", (spr) =>
          @loaded++
          $prbartext.text("Loading sprite: #{spr}")

      # @sprloading = 
      #   Hal.asm.on "SPRITES_LOADING", (len) =>  
      #     $prbarcnt.show()
      #     log.debug "length is #{len}"

      #     @sprloaded = 
      #       Hal.asm.on "SPRITE_LOADED", () =>
      #         cnt++
      #         do (cnt) ->
      #             $prbar.animate({
      #                     width: ~~((cnt/len) * 100)
      #                 }, {
      #                     duration: 1,
      #                     step: () -> $prval.text((~~(((cnt)/len)*100))+"%")
      #                 }
      #             )

      # @sprsloaded = 
      #   Hal.asm.on "SPRITES_LOADED", () =>
      #     $prbar.animate({
      #             width: "100%"
      #         }, 100, 
      #         () => 
      #             $prval.text("100%")
      #             $viewport.animate({
      #                 opacity: "+=1"
      #             }, 2000, () => 
      #                 $viewport.css({'opacity': '255'});
      #                 $('#domlayer').css({'background': 'transparent'})
      #             )
      #             $prbarcnt.animate({
      #                     opacity: "-=1"
      #                 }, 500, () => 
      #                     $prbarcnt.hide()
      #                     @clb()
      #                     Hal.asm.removeTrigger "SPRITES_LOADED", @sprsloaded
      #                     Hal.asm.removeTrigger "SPRITE_LOADED", @sprloaded
      #                     Hal.asm.removeTrigger "SPRITES_LOADING", @sprloading
      #             )
      #     )

  return AssetLoaderBar