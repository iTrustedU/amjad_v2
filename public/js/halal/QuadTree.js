(function() {
  "use strict";
  define(function() {
    var QuadTree, capacity;
    capacity = 1;
    QuadTree = (function() {
      function QuadTree(bounds) {
        this.bounds = bounds;
        this.pts = [];
        this.nw = null;
        this.sw = null;
        this.ne = null;
        this.se = null;
      }

      QuadTree.prototype.insert = function(ent) {
        if (!Hal.m.isPointInRect(ent.pos, this.bounds)) {
          return false;
        }
        if (this.pts.length < capacity) {
          ent.quadspace = this;
          this.pts.push(ent);
          return true;
        }
        if (this.nw == null) {
          this.divide();
        }
        if (this.nw.insert(ent)) {
          return true;
        }
        if (this.ne.insert(ent)) {
          return true;
        }
        if (this.sw.insert(ent)) {
          return true;
        }
        if (this.se.insert(ent)) {
          return true;
        }
        return false;
      };

      QuadTree.prototype.remove = function(ent) {
        var ind;
        ind = this.pts.indexOf(ent);
        return this.pts.splice(ind, 1);
      };

      QuadTree.prototype.searchInRange = function(range) {
        var entsInRange, p, _i, _len, _ref;
        entsInRange = [];
        if (!Hal.m.rectIntersectsRect(range, this.bounds)) {
          return entsInRange;
        }
        _ref = this.pts;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          p = _ref[_i];
          if (p.rectIntersectsShape(range)) {
            entsInRange.push(p);
          }
        }
        if (this.nw == null) {
          return entsInRange;
        }
        entsInRange = entsInRange.concat(this.nw.searchInRange(range));
        entsInRange = entsInRange.concat(this.ne.searchInRange(range));
        entsInRange = entsInRange.concat(this.sw.searchInRange(range));
        entsInRange = entsInRange.concat(this.se.searchInRange(range));
        return entsInRange;
      };

      QuadTree.prototype.divide = function() {
        var h, w;
        w = this.bounds[2] / 2;
        h = this.bounds[3] / 2;
        this.nw = new QuadTree([this.bounds[0], this.bounds[1], w, h]);
        this.ne = new QuadTree([this.bounds[0] + w, this.bounds[1], w, h]);
        this.sw = new QuadTree([this.bounds[0], this.bounds[1] + h, w, h]);
        return this.se = new QuadTree([this.bounds[0] + w, this.bounds[1] + h, w, h]);
      };

      return QuadTree;

    })();
    return QuadTree;
  });

}).call(this);
